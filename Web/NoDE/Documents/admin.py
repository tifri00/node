from django.contrib import admin

# Register your models here.
from Documents.models import DocumentCollection, Document, ExtractedInfo, ExtractedInfoConflictingMaxValue, \
    ExtractedInfoAltValue, ExtractedInfoSourceSentence, ExtractedInfoDetailSentence

admin.site.register(DocumentCollection)
admin.site.register(Document)
admin.site.register(ExtractedInfo)
admin.site.register(ExtractedInfoConflictingMaxValue)
admin.site.register(ExtractedInfoAltValue)
admin.site.register(ExtractedInfoSourceSentence)
admin.site.register(ExtractedInfoDetailSentence)
