import django_tables2 as tables


class DocumentCollectionTable(tables.Table):
    name = tables.Column(verbose_name="Name")
    description = tables.Column(verbose_name="Beschreibung")

    class Meta:
        attrs = {"class": "table-alternating-rowcols", "id": "main-file-table"}
        row_attrs = {"onClick": lambda record: "window.open('/documents/collection-details?collection=" + str(record.pk) + "', '_self')"}

    def render_description(self, value):
        result = value[:50]

        if len(value) > 50:
            result += "..."

        return result

