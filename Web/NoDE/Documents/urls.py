from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name="index"),
    path('collection-edit', views.editCollection, name="collection-edit"),
    path('collection-details', views.collectionDetails, name="collection-details"),
    path('collection-delete', views.deleteCollection, name="collection-delete"),
    path('collection-check-validity', views.collectionCheckValidity, name="collection-check-validity"),
    path('document-edit', views.editDocument, name="document-edit"),
    path('document-delete', views.deleteDocument, name="document-delete"),
    path('download', views.downloadDocument, name="document-download"),
    path('info-details-ajax', views.extractedInfoPopupContent, name="info-details-ajax"),
    path('update-info-value', views.updateInfoValue, name="update-info-value"),
    path('delete-info', views.deleteInfo, name="delete-info"),
    path('add-info', views.addInfo, name="add-info"),
    path('add-info-popup-ajax', views.addInfoPopupContent, name="add-info-popup-ajax"),
]