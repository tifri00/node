from collections import Counter

import stanza
import re

stanza.download(lang='de', processors='tokenize,pos,ner')
# stanza.download(lang="de", package=None, processors={"mwt":"gsd"})
nlp = stanza.Pipeline('de', processors='tokenize,pos,ner', download_method=None)

def mergeSentences(s1, s2):
    for word in s2.words:
        s1.words.append(word)
    s1.text += f" {s2.text}"

    return s1

def getSentenceMap(text):
    result = {}

    doc = nlp(text)

    mergedSentences = []
    i = 0
    while i < len(doc.sentences):
        sentence = doc.sentences[i]
        if i < len(doc.sentences) - 1 and (sentence.text.endswith("Nr.") or sentence.text.endswith("Abs.") or sentence.text.endswith("S.") or sentence.text.endswith(":")):
            sentence = mergeSentences(sentence, doc.sentences[i+1])
            i += 1

        mergedSentences.append(sentence)
        i += 1

    for sentence in mergedSentences:
        trueCaseWords = []

        for i, word in enumerate(sentence.words):
            if i == len(sentence.words) - 1 and word.pos == "PUNCT":
                continue
            elif word.pos == "PUNCT" and word.text == "-":
                continue
            elif re.match(r"(0?[1-9]|[1-3]\d)\.(0?[1-9]|1[0-2])\.\d\d\d\d", word.text):
                word.text = word.text.replace(".", " . ")
            elif word.pos == "NUM":
                word.text = word.text.replace(".", "")

            if word.pos in ['NOUN', 'PROPN']:
                if word.text[0].isupper(): # words like "BGB" should just be taken as they are (already capitalized)
                    trueCaseWords.append(word.text)
                else:
                    trueCaseWords.append(word.text.capitalize())
            elif word.pos == "PUNCT" and word.text == ".":
                trueCaseWords[len(trueCaseWords)-1] += "."
            else:
                trueCaseWords.append(word.text.lower())

        resultSentence = " ".join(trueCaseWords)

        processedEntities = []
        for ne in doc.entities:
            if ne.text in processedEntities or ne.text in ["HRB"]:
                continue

            genderInfo = []
            for word in ne.words:
                genderInfo.extend(list(map(lambda f: f[1], filter(lambda f: f[0] == "Gender",
                                                                  map(lambda f: f.split("="), word.feats.split("|"))))))

            gender = "Neut" if len(genderInfo) == 0 else Counter(genderInfo).most_common(1)[0][0]

            processedEntities.append(ne.text)
            name = ne.text.replace(" ", "&+")
            resultSentence = re.sub(r"(?<!\{)\b" + re.escape(ne.text) + r"\b(?![\w\s]*[\}])", f"NoDENamedEntity{gender}{{ {name} }}", resultSentence) # resultSentence.replace(ne.text, f"NoDENamedEntity{gender}{{ {name} }}")

        result[resultSentence] = (sentence.id, sentence.text)


    return result

def extractPlainText(file):
    result = ""
    type = file.content_type

    if type == "text/plain":
        for chunk in file.chunks():
            result += chunk.decode("utf-8")

    return result
