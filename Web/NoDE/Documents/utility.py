from constants import INFO_KEYS
import re

onmouseover = "highlightSentence('doc{{ document.id }}sen{{ info.info.sourceSentence.id }}', true)"
onmouseout = "highlightSentence('doc{{ document.id }}sen{{ info.info.sourceSentence.id }}', false)"


def getHumanReadableSentence(sentence):
    humanReadableSentence = sentence

    nes = re.findall("NoDENamedEntity{ .+? }", humanReadableSentence)
    for ne in nes:
        name = ne[len("NoDENamedEntity") + 2:len(ne) - 2].replace("&+", " ")
        humanReadableSentence = humanReadableSentence.replace(ne, name)

    return humanReadableSentence


def keyword(text):
    return "<span class='extracted-info-keyword'>%s</span>" % text


def hoverable(text, sentence):
    if sentence is None:
        return "<span>%s</span>" % text

    return """<span 
    onmouseover=\"highlightSentence('sen%s', true)\" 
    onmouseout=\"highlightSentence('sen%s', false)\">%s
    </span>""" % (sentence.id, sentence.id, text)


def getExtractedInfoRenderData(info):
    key = info.key
    value = info.value
    maxAlt = list(map(lambda x: (x.value, x.sourceSentence), info.extractedinfoconflictingmaxvalue_set.all()))
    alt = list(map(lambda x: (x.value, x.sourceSentence), info.extractedinfoaltvalue_set.all()))
    docId = info.document.id

    text = ""
    if info.conditionSentence:
        text = keyword("Unter bestimmten Bedingungen: ")

    rawText = INFO_KEYS[key]["shortInfoText"](value, hoverable, keyword, maxAlt, alt, info.sourceSentence)
    text += INFO_KEYS[key]["longInfoText"](value, hoverable, keyword, maxAlt, alt, info.sourceSentence)

    return {"doc": docId, "text": text, "rawText": rawText, "info": info}


def getInputAndLabelByInfoKey(key, info):
    label = INFO_KEYS[key]["newInfoInputLabel"] if info is None else INFO_KEYS[key]["editInfoInputLabel"]

    inputType = INFO_KEYS[key]["inputType"]

    inputCode = ""
    if inputType == "text":
        inputCode = "<input id='newValue' name='newValue' type='text' value=''>" if info is None else f"<input id='correctedValue' name='correctedValue' type='text' value='{info.value}'>"
    elif inputType == "checkbox":
        inputCode = "<input id='newValue' name='newValue' type='checkbox'>" \
            if info is None \
            else f"<input id='correctedValue' name='correctedValue' type='checkbox' {'checked' if info.value == 'True' else ''}>"
    elif inputType == "select":
        nl = "\n"
        inputCode = f"""<select id='newValue' name='newValue'>
            {nl.join(list(map(lambda k: f"<option value='{k}'>{INFO_KEYS[key]['valueMap'][k]}</option>", INFO_KEYS[key]["valueMap"].keys())))}
        </select>""" if info is None else f"""<select id='correctedValue' name='correctedValue'>
            {nl.join(list(map(lambda k: f"<option value='{k}' {'selected' if info.value == k else ''}>{INFO_KEYS[key]['valueMap'][k]}</option>", INFO_KEYS[key]["valueMap"].keys())))}
        </select>"""

    return label, inputCode


def getSuccessValueFromReasonerOutput(result, key):
    try:
        index = result.index(key)

        if index < 2:
            return False

        return result[index-2] == "✓"
    except ValueError:
        return False
