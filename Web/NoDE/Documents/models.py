from django.db import models


class DocumentCollection(models.Model):
    name = models.CharField(max_length=100)
    description = models.CharField(max_length=512, default="")
    created_at = models.DateTimeField(auto_now=True)
    modified_at = models.DateTimeField(auto_now=True)


class Document(models.Model):
    name = models.CharField(max_length=100)
    documentCollection = models.ForeignKey(DocumentCollection, on_delete=models.CASCADE)
    data = models.FileField()
    plainText = models.TextField()
    fileType = models.CharField(max_length=64)
    created_at = models.DateTimeField(auto_now=True)
    modified_at = models.DateTimeField(auto_now=True)


class ExtractedInfoSourceSentence(models.Model):
    sentence = models.TextField()
    document = models.ForeignKey(Document, on_delete=models.CASCADE)
    pos = models.IntegerField()


class ExtractedInfoDetailSentence(models.Model):
    sentence = models.TextField()


class ExtractedInfo(models.Model):
    key = models.CharField(max_length=128)
    value = models.CharField(max_length=512)
    confidence = models.FloatField()
    sourceSentence = models.ForeignKey(ExtractedInfoSourceSentence, on_delete=models.CASCADE, null=True)
    conditionSentence = models.ForeignKey(ExtractedInfoDetailSentence, on_delete=models.SET_NULL, null=True)
    document = models.ForeignKey(Document, on_delete=models.CASCADE)


class ExtractedInfoConflictingMaxValue(models.Model):
    value = models.CharField(max_length=512)
    confidence = models.FloatField()
    sourceSentence = models.ForeignKey(ExtractedInfoSourceSentence, on_delete=models.CASCADE)
    conditionSentence = models.ForeignKey(ExtractedInfoDetailSentence, on_delete=models.SET_NULL, null=True)
    extractedInfo = models.ForeignKey(ExtractedInfo, on_delete=models.CASCADE)


class ExtractedInfoAltValue(models.Model):
    value = models.CharField(max_length=512)
    confidence = models.FloatField()
    sourceSentence = models.ForeignKey(ExtractedInfoSourceSentence, on_delete=models.CASCADE)
    conditionSentence = models.ForeignKey(ExtractedInfoDetailSentence, on_delete=models.SET_NULL, null=True)
    extractedInfo = models.ForeignKey(ExtractedInfo, on_delete=models.CASCADE)
