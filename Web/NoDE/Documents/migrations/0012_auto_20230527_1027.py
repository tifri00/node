# Generated by Django 3.2.16 on 2023-05-27 10:27

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('Documents', '0011_extractedinfosourcesentence_pos'),
    ]

    operations = [
        migrations.AddField(
            model_name='extractedinfoaltvalue',
            name='sourceSentence',
            field=models.TextField(default=''),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='extractedinfoconflictingmaxvalue',
            name='sourceSentence',
            field=models.TextField(default=''),
            preserve_default=False,
        ),
    ]
