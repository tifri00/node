import datetime
import os

from constants import INFO_KEYS
from django.core.exceptions import ObjectDoesNotExist
from django.db.models import Q
from django.http import HttpResponseBadRequest, HttpResponseNotAllowed, Http404, HttpResponse
from django.shortcuts import render, redirect
from extractor import extractFromSentenceList
from django.template.defaulttags import register
from Documents.models import DocumentCollection, Document, ExtractedInfo, ExtractedInfoConflictingMaxValue, \
    ExtractedInfoAltValue, ExtractedInfoSourceSentence, ExtractedInfoDetailSentence
from Documents.plainTextExtractor import extractPlainText, getSentenceMap
from Documents.tables import DocumentCollectionTable
from Documents.utility import getExtractedInfoRenderData, getInputAndLabelByInfoKey, getHumanReadableSentence, \
    getSuccessValueFromReasonerOutput
from NoDE import settings
from login import login_utils
import json
import subprocess


@register.filter
def get_item(dictionary, key):
    return dictionary.get(key)


def index(request):
    if login_utils.login_required(request):
        return login_utils.redirect_login(request)

    if request.method == "POST":
        return handleSearch(request)

    documentCollectionTable = DocumentCollectionTable(DocumentCollection.objects.all())

    return render(request, 'index.html', {'documentCollectionTable': documentCollectionTable})

def handleSearch(request):
    query = request.POST.get("q")

    documentCollectionTable = DocumentCollectionTable(DocumentCollection.objects.filter(Q(name__contains=query) | Q(description__contains=query)))

    return render(request, 'index.html', {'query': query, 'searchResults': documentCollectionTable})


def editCollection(request):
    if login_utils.login_required(request):
        return login_utils.redirect_login(request)

    if request.method == "POST":
        collectionId = request.POST.get("id")
        name = request.POST.get("name")
        description = request.POST.get("description")

        collection = None

        if collectionId:
            try:
                collection = DocumentCollection.objects.get(id=collectionId)
            except ObjectDoesNotExist:
                pass
        else:
            collectionId = -1

        errors = []
        if not name:
            errors.append("Bitte geben Sie einen Namen ein")

        if len(errors) > 0:
            collection = DocumentCollection(name=name, description=description)
            return render(request, 'collection-edit.html', {"errors": errors, "collection": collection, "collectionId": collectionId})

        if not collection:
            collection = DocumentCollection(name=name, description=description, created_at=datetime.date.today())
        else:
            collection.name = name
            collection.description=description

        collection.modified_at = datetime.date.today()

        collection.save()

        return redirect('/documents/collection-details?collection=%s' % collection.id)

    collectionId = request.GET.get("collection")

    context = {
        "collection": None,
        "collectionId": -1,
    }

    if collectionId:
        try:
            context["collection"] = DocumentCollection.objects.get(id=collectionId)
            context["collectionId"] = int(collectionId)
        except ObjectDoesNotExist:
            return HttpResponseBadRequest("The requested document collection does not exist")

    return render(request, 'collection-edit.html', context)


def collectionDetails(request):
    if login_utils.login_required(request):
        return login_utils.redirect_login(request)

    if not request.method == "GET":
        return HttpResponseNotAllowed(["GET"])

    collectionId = request.GET.get("collection")

    if not collectionId:
        return HttpResponseBadRequest("Missing required GET parameter 'collection'")

    extractedDocumentInfos = []

    try:
        collection = DocumentCollection.objects.get(id=collectionId)
        for document in collection.document_set.all():
            for extractedInfo in document.extractedinfo_set.all():
                renderData = getExtractedInfoRenderData(extractedInfo)
                if renderData:
                    extractedDocumentInfos.append(renderData)
    except ObjectDoesNotExist:
        return HttpResponseBadRequest("The requested document collection does not exist")

    orderedSentences = {}
    sentenceInfoMap = {}
    for document in collection.document_set.all():
        sentences = ExtractedInfoSourceSentence.objects.filter(document=document).all()
        orderedSentences[document.id] = sentences
        for sentence in sentences:
            sentenceInfoMap[sentence.id] = list(map(lambda i: i.id, sentence.extractedinfo_set.all()))
            sentenceInfoMap[sentence.id].extend(list(map(lambda i: i.extractedInfo.id, sentence.extractedinfoconflictingmaxvalue_set.all())))
            sentenceInfoMap[sentence.id].extend(list(map(lambda i: i.extractedInfo.id, sentence.extractedinfoaltvalue_set.all())))

    validityCheckDone = request.GET.get("validityCheckDone")
    validityCheckResults = {}

    if validityCheckDone is not None:
        registrationValid = request.GET.get("registrationValid")
        resolutionValid = request.GET.get("resolutionValid")
        resolutionFormallyValid = request.GET.get("resolutionFormallyValid")
        resolutionValidWithMeeting = request.GET.get("resolutionValidWithMeeting")
        resolutionValidWithoutMeeting = request.GET.get("resolutionValidWithoutMeeting")
        resolutionWithMajority = request.GET.get("resolutionWithMajority")
        resolutionWritingUnanimous = request.GET.get("resolutionWritingUnanimous")
        resolutionVotingWithMajority = request.GET.get("resolutionVotingWithMajority")
        deedValid = request.GET.get("deedValid")
        reassuranceValid = request.GET.get("reassuranceValid")
        rightOfRepresentation = request.GET.get("rightOfRepresentation")
        allCEOsPresent = request.GET.get("allCEOsPresent")
        CEOwithConcreteSoleRepresentation = request.GET.get("CEOwithConcreteSoleRepresentation")
        CEOwithGeneralSoleRepresentation = request.GET.get("CEOwithGeneralSoleRepresentation")
        CEOwithConcreteOverallRepresentation = request.GET.get("CEOwithConcreteOverallRepresentation")
        CEOwithGeneralOverallRepresentation = request.GET.get("CEOwithGeneralOverallRepresentation")

        validityCheckResults = {
            "registrationValid": "validationSuccess" if registrationValid == "True" else "validationError",
            "resolutionValid": "validationSuccess" if resolutionValid == "True" else "validationError",
            "resolutionFormallyValid": "validationSuccess" if resolutionFormallyValid == "True" else "validationError",
            "resolutionValidWithMeeting": "validationSuccess" if resolutionValidWithMeeting == "True" else "validationError",
            "resolutionValidWithoutMeeting": "validationSuccess" if resolutionValidWithoutMeeting == "True" else "validationError",
            "resolutionWithMajority": "validationSuccess" if resolutionWithMajority == "True" else "validationError",
            "resolutionWritingUnanimous": "validationSuccess" if resolutionWritingUnanimous == "True" else "validationError",
            "resolutionVotingWithMajority": "validationSuccess" if resolutionVotingWithMajority == "True" else "validationError",
            "deedValid": "validationSuccess" if deedValid == "True" else "validationError",
            "reassuranceValid": "validationSuccess" if reassuranceValid == "True" else "validationError",
            "rightOfRepresentation": "validationSuccess" if rightOfRepresentation == "True" else "validationError",
            "allCEOsPresent": "validationSuccess" if allCEOsPresent == "True" else "validationError",
            "CEOwithConcreteSoleRepresentation": "validationSuccess" if CEOwithConcreteSoleRepresentation == "True" else "validationError",
            "CEOwithGeneralSoleRepresentation": "validationSuccess" if CEOwithGeneralSoleRepresentation == "True" else "validationError",
            "CEOwithConcreteOverallRepresentation": "validationSuccess" if CEOwithConcreteOverallRepresentation == "True" else "validationError",
            "CEOwithGeneralOverallRepresentation": "validationSuccess" if CEOwithGeneralOverallRepresentation == "True" else "validationError",
        }


    return render(request, 'collection-details.html',
                  {'collection': collection, 'extractedDocumentInfos': extractedDocumentInfos,
                   'docsWithInfos': list(set(map(lambda i: i["doc"], extractedDocumentInfos))),
                   'orderedSentences': orderedSentences, 'sentenceInfoMap': sentenceInfoMap,
                   'validityCheckDone': validityCheckDone, 'validityCheckResults': validityCheckResults})


def collectionCheckValidity(request):
    if login_utils.login_required(request):
        return login_utils.redirect_login(request)

    if not request.method == "GET":
        return HttpResponseNotAllowed(["GET"])

    collectionId = request.GET.get("collection")

    if not collectionId:
        return HttpResponseBadRequest("Missing required GET parameter 'collection'")

    try:
        collection = DocumentCollection.objects.get(id=collectionId)

        gesellschafterCount = request.GET.get("gesellschafterCount")
        gesellschafterData = []
        for i in range(int(gesellschafterCount)):
            name = request.GET.get(f"gesellschafterName{i}")
            stimmrechte = request.GET.get(f"stimmrechte{i}")
            gesellschafterData.append({
                "name": name,
                "stimmrechte": 0 if stimmrechte == "" else int(stimmrechte),
            })

        CEOCount = request.GET.get("CEOCount")
        CEOData = []
        for i in range(int(CEOCount)):
            name = request.GET.get(f"CEOName{i}")
            vertretungsregelung = request.GET.get(f"CEOvertretungsregelung{i}")
            befreit181 = request.GET.get(f"CEO181waiver{i}") == "on"
            CEOData.append({
                "name": name,
                "vertretungsregelung": vertretungsregelung,
                "befreiung181": befreit181,
            })

        vertretungsregelung = request.GET.get("vertretungsregelung")
        mehrheitsanforderung = request.GET.get("mehrheitsanforderung")
        newCEOName = request.GET.get("newCEOName")
        newCEOVertretungsregelung = request.GET.get("newCEOVertretungsregelung")
        newCEO181Waiver = request.GET.get("newCEO181Waiver") == "on"
        versammlungStattgefunden = request.GET.get("versammlungStattgefunden") == "on"
        versammlungsform = request.GET.get("versammlungsform")

        remoteEinverstandenCount = request.GET.get("remoteEinverstandenCount")
        remoteEinverstanden = []
        for i in range(int(remoteEinverstandenCount)):
            remoteEinverstanden.append(request.GET.get(f"remoteEinverstanden{i}"))

        jaAnteil = request.GET.get("jaAnteil")
        versicherungUnterzeichnet = request.GET.get("versicherungUnterzeichnet") == "on"
        urkundeForm = request.GET.get("urkundeForm")

        anmeldendeGFCount = request.GET.get("anmeldendeGFCount")
        anmeldendeGF = []
        for i in range(int(anmeldendeGFCount)):
            anmeldendeGF.append(request.GET.get(f"anmeldendeGF{i}"))

        caseData = {
            "gesellschaft": {
                "gesellschafter": gesellschafterData,
                "geschaeftsfuehrer": CEOData,
                "vertretungsregelung": vertretungsregelung,
                "mehrheitsanforderung": 0 if mehrheitsanforderung == "" else int(mehrheitsanforderung) / 100,
            },
            "beschluss": {
                "neuer_geschaeftsfuehrer": {
                    "name": newCEOName,
                    "vertretungsregelung": newCEOVertretungsregelung,
                    "befreiung181": newCEO181Waiver,
                },
                "versammlung": {
                    "stattgefunden": versammlungStattgefunden,
                    "form": versammlungsform,
                    "fernmuendliche_versammlung_einverstanden": remoteEinverstanden,
                },
                "abstimmungsergebnis": {
                    "ja_anteil": 0 if jaAnteil == "" else int(jaAnteil) / 100,
                },
                "schriftliche_zustimmung": None,
                "schriftliche_stimmabgabe_einverstanden": [],
            },
            "anmeldung": {
                "versicherung_unterzeichnet": versicherungUnterzeichnet,
                "urkunde_form": urkundeForm,
                "anmeldende_geschaeftsfuehrer": anmeldendeGF,
            },
        }
        jsonCaseData = json.dumps(caseData)

        prover = subprocess.Popen(["./main.py", jsonCaseData],
                                  cwd="/Users/timfriedrich/Documents/Masterarbeit.nosync/Visual_Prover",
                                  stdout=subprocess.PIPE,
                                  stderr=subprocess.PIPE)

        out, err = prover.communicate()
        errcode = prover.returncode

        if errcode == 0:
            result = out.decode("utf-8")

            registrationValid = getSuccessValueFromReasonerOutput(result, "Anmeldung gültig?")
            resolutionValid = getSuccessValueFromReasonerOutput(result, "Beschluss gültig?")
            resolutionFormallyValid = getSuccessValueFromReasonerOutput(result, "Beschluss formell gültig?")
            resolutionValidWithMeeting = getSuccessValueFromReasonerOutput(result, "Beschluss mit Versammlung gültig?")
            resolutionValidWithoutMeeting = getSuccessValueFromReasonerOutput(result, "Beschluss ohne Versammlung gültig?")
            resolutionWithMajority = getSuccessValueFromReasonerOutput(result, "Beschluss mit Mehrheit ergangen?")
            resolutionWritingUnanimous = getSuccessValueFromReasonerOutput(result, "Beschluss schriftlich einstimmig?")
            resolutionVotingWithMajority = getSuccessValueFromReasonerOutput(result, "Beschluss in Abstimmung mit Mehrheit ergangen?")
            deedValid = getSuccessValueFromReasonerOutput(result, "Urkunde gültig?")
            reassuranceValid = getSuccessValueFromReasonerOutput(result, "Versicherung gültig?")
            rightOfRepresentation = getSuccessValueFromReasonerOutput(result, "Vertretungsberechtigung gegeben?")
            allCEOsPresent = getSuccessValueFromReasonerOutput(result, "Alle Geschäftsführer anwesend?")
            CEOwithConcreteSoleRepresentation = getSuccessValueFromReasonerOutput(result, "Geschäftsführer mit konkreter Alleinvertretung?")
            CEOwithGeneralSoleRepresentation = getSuccessValueFromReasonerOutput(result, "Geschäftsführer mit allgemeiner Alleinvertretung?")
            CEOwithConcreteOverallRepresentation = getSuccessValueFromReasonerOutput(result, "Geschäftsführer mit konkreter echter Gesamtvertretung?")
            CEOwithGeneralOverallRepresentation = getSuccessValueFromReasonerOutput(result, "Geschäftsführer mit allgemeiner echter Gesamtvertretung?")
            print()
        else:
            print("some error occured") # TODO: handle
    except ObjectDoesNotExist:
        return HttpResponseBadRequest("The requested document collection does not exist")


    return redirect(f"/documents/collection-details?collection={collection.id}&validityCheckDone=true"
                    f"&registrationValid={registrationValid}&resolutionValid={resolutionValid}"
                    f"&resolutionFormallyValid={resolutionFormallyValid}"
                    f"&resolutionValidWithMeeting={resolutionValidWithMeeting}"
                    f"&resolutionValidWithoutMeeting={resolutionValidWithoutMeeting}"
                    f"&resolutionWithMajority={resolutionWithMajority}"
                    f"&resolutionWritingUnanimous={resolutionWritingUnanimous}"
                    f"&resolutionVotingWithMajority={resolutionVotingWithMajority}"
                    f"&deedValid={deedValid}"
                    f"&reassuranceValid={reassuranceValid}"
                    f"&rightOfRepresentation={rightOfRepresentation}"
                    f"&allCEOsPresent={allCEOsPresent}"
                    f"&CEOwithConcreteSoleRepresentation={CEOwithConcreteSoleRepresentation}"
                    f"&CEOwithGeneralSoleRepresentation={CEOwithGeneralSoleRepresentation}"
                    f"&CEOwithConcreteOverallRepresentation={CEOwithConcreteOverallRepresentation}"
                    f"&CEOwithGeneralOverallRepresentation={CEOwithGeneralOverallRepresentation}")


def deleteCollection(request):
    if request.method != "POST":
        return redirect("/")

    collectionId = request.POST.get("collectionId")

    collection = DocumentCollection.objects.get(id=collectionId)
    collection.delete()

    return redirect("/")


def editDocument(request):
    if login_utils.login_required(request):
        return login_utils.redirect_login(request)

    if request.method == "POST":
        documentId = request.POST.get("id")
        collectionId = request.POST.get("collectionId")
        name = request.POST.get("name")
        plainText = request.POST.get("plainText")
        data = request.FILES.get("data")

        document = None

        if documentId:
            try:
                document = Document.objects.get(id=documentId)
            except ObjectDoesNotExist:
                pass
        else:
            documentId = -1

        errors = []
        if not name:
            errors.append("Bitte geben Sie einen Namen ein")

        if len(errors) > 0:
            document = Document(name=name, plainText="")
            return render(request, 'document-edit.html', {"errors": errors, "document": document, "documentId": documentId, "collectionId": collectionId})

        if not document:
            document = Document(name=name, plainText="", fileType="", created_at=datetime.date.today())
        else:
            document.name = name
            document.plainText = plainText

        document.modified_at = datetime.date.today()
        document.documentCollection_id = collectionId

        if data:
            if document.data:
                os.remove(os.path.join(settings.MEDIA_ROOT, document.data.name))

            document.data = data
            document.fileType = data.content_type
            document.plainText = extractPlainText(data)
            document.save()

        ExtractedInfoSourceSentence.objects.filter(document=document).delete()
        sentenceMap = getSentenceMap(document.plainText)
        for gfSentence in sentenceMap:
            entity = ExtractedInfoSourceSentence(sentence=getHumanReadableSentence(sentenceMap[gfSentence][1]), pos=sentenceMap[gfSentence][0], document=document)
            entity.save()
            sentenceMap[gfSentence] = entity
        extractedInformation = extractFromSentenceList(list(sentenceMap.keys()))
        ExtractedInfo.objects.filter(document=document).delete()
        for key in extractedInformation:
            infoElems = []
            if INFO_KEYS[key]["multiple"]:
                infoElems.extend(extractedInformation[key])
            else:
                infoElems.append(extractedInformation[key])

            for infoElem in infoElems:
                conditions = infoElem["conditions"]
                condition = None
                if conditions and len(conditions) > 0:
                    condition = ExtractedInfoDetailSentence(sentence=getHumanReadableSentence(conditions[0]))
                    condition.save()

                extractedInfo = ExtractedInfo(
                    key=key,
                    value=infoElem["val"],
                    confidence=infoElem["conf"],
                    sourceSentence=sentenceMap[infoElem["sourceSentence"]],
                    conditionSentence=condition,
                    document=document,
                )
                extractedInfo.save()
                for confMaxVal in infoElem["conflictingMax"]:
                    conditions = confMaxVal[3]
                    condition = None
                    if conditions and len(conditions) > 0:
                        condition = ExtractedInfoDetailSentence(sentence=getHumanReadableSentence(conditions[0]))
                        condition.save()

                    ExtractedInfoConflictingMaxValue(value=confMaxVal[0], extractedInfo=extractedInfo, sourceSentence=sentenceMap[confMaxVal[1]], confidence=confMaxVal[2], conditionSentence=condition).save()
                for altVal in infoElem["alt"]:
                    conditions = altVal[3]
                    condition = None
                    if conditions and len(conditions) > 0:
                        condition = ExtractedInfoDetailSentence(sentence=getHumanReadableSentence(conditions[0]))
                        condition.save()

                    ExtractedInfoAltValue(value=altVal[0], extractedInfo=extractedInfo, sourceSentence=sentenceMap[altVal[1]], confidence=altVal[2], conditionSentence=condition).save()

        document.save()

        return redirect('/documents/collection-details?collection=%s' % collectionId)

    documentId = request.GET.get("document")
    collectionId = request.GET.get("collection")

    context = {
        "document": None,
        "documentId": -1,
        "collectionId": int(collectionId),
    }

    if documentId:
        try:
            context["document"] = Document.objects.get(id=documentId)
            context["documentId"] = int(documentId)
        except ObjectDoesNotExist:
            return HttpResponseBadRequest("The requested document does not exist")

    return render(request, 'document-edit.html', context)


def deleteDocument(request):
    if request.method != "POST":
        return redirect("/")

    documentId = request.POST.get("documentId")

    document = Document.objects.get(id=documentId)

    collectionId = document.documentCollection_id

    document.data.delete()
    document.delete()

    return redirect("/documents/collection-details?collection=" + str(collectionId))


def downloadDocument(request):
    if request.method == "GET":
        documentId = request.GET.get("document")

        if not documentId:
            return HttpResponseBadRequest("Missing required query parameter document")

        try:
            document = Document.objects.get(id=documentId)
        except ObjectDoesNotExist:
            return Http404("Document not found")

        document_path = os.path.join(settings.MEDIA_ROOT, document.data.name)
        if os.path.exists(document_path):
            with open(document_path, 'rb') as fh:
                response = HttpResponse(fh.read(), content_type=document.fileType)
                response['Content-Disposition'] = 'inline; filename=' + os.path.basename(document_path)
                return response

        return Http404("Document not found")

    return HttpResponseNotAllowed(['GET'])


def extractedInfoPopupContent(request):
    if request.method != "GET":
        return HttpResponseNotAllowed([request.method])

    infoId = request.GET.get("infoId")
    info = ExtractedInfo.objects.get(id=infoId)

    if info is None:
        return Http404("info entity not found")

    collectionId = request.GET.get("collectionId")

    renderData = getExtractedInfoRenderData(info)

    valueMap = {}
    for maxVal in info.extractedinfoconflictingmaxvalue_set.all():
        valueMap[maxVal.value] = maxVal.value
    for altVal in info.extractedinfoaltvalue_set.all():
        valueMap[altVal.value] = altVal.value

    valueMap.update(INFO_KEYS[info.key]["valueMap"])

    (correctionLabelText, correctionInput) = getInputAndLabelByInfoKey(info.key, info)

    sortedAltValues = sorted(info.extractedinfoaltvalue_set.all(), key=lambda x: x.confidence, reverse=True)

    return render(request, 'extracted-info-popup-content.html',
                  {
                      "info": info,
                      "collectionId": collectionId,
                      "renderData": renderData,
                      "correctionLabelText": correctionLabelText,
                      "correctionInput": correctionInput,
                      "sortedAltValues": sortedAltValues,
                      "valueMap": valueMap,
                  })


def updateInfoValue(request):
    if request.method != "POST":
        return HttpResponseNotAllowed([request.method])

    postData = json.loads(request.body.decode("utf-8"))
    correctedValue = postData.get("correctedValue")
    infoId = postData.get("infoId")
    info = ExtractedInfo.objects.get(id=infoId)

    if info is None:
        return Http404("info entity not found")

    info.value = correctedValue
    info.confidence = 1.0
    for maxVal in info.extractedinfoconflictingmaxvalue_set.all():
        maxVal.delete()

    for altVal in info.extractedinfoaltvalue_set.all():
        altVal.delete()

    if info.conditionSentence:
        info.conditionSentence.delete()
        info.conditionSentence = None

    info.sourceSentence = None
    info.save()

    return HttpResponse(status=200)


def deleteInfo(request):
    if request.method != "POST":
        return HttpResponseNotAllowed([request.method])

    infoId = request.POST.get("infoId")
    info = ExtractedInfo.objects.get(id=infoId)

    if info is None:
        return Http404("info entity not found")

    info.delete()

    collectionId = request.POST.get("collectionId")

    return redirect('/documents/collection-details?collection=%s' % collectionId)


def addInfo(request):
    if request.method != "POST":
        return HttpResponseNotAllowed([request.method])

    postData = json.loads(request.body.decode("utf-8"))
    key = postData.get("key")
    value = postData.get("value")

    documentId = postData.get("documentId")
    document = Document.objects.get(id=documentId)
    if document is None:
        return Http404("document not found")

    info = ExtractedInfo(key=key, value=value, confidence=1.0, sourceSentence=None, conditionSentence=None, document=document)
    info.save()

    return HttpResponse(status=200)

def addInfoPopupContent(request):
    if request.method != "GET":
        return HttpResponseNotAllowed([request.method])

    documentId = request.GET.get("documentId")
    document = Document.objects.get(id=documentId)

    if document is None:
        return Http404("document not found")

    freeKeys = list(filter(lambda k: k not in list(map(lambda i: i.key, document.extractedinfo_set.all())) or INFO_KEYS[k]["multiple"], INFO_KEYS.keys()))

    keyLabelMap = {}
    keyInputMap = {}
    for key in freeKeys:
        (inputLabel, input) = getInputAndLabelByInfoKey(key, None)
        keyLabelMap[key] = inputLabel
        keyInputMap[key] = input

    return render(request, 'add-info-popup-content.html', {
        "selectedKey": None if len(freeKeys) == 0 else freeKeys[0],
        "keyLabelMap": keyLabelMap,
        "keyInputMap": keyInputMap,
        "keyOptions": {key: INFO_KEYS[key]["name"] for key in freeKeys},
        "documentId": documentId,
    })
