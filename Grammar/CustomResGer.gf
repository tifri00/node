resource CustomResGer = CatGer ** open (R = ResGer), (P = Prelude), (Coord = Coordination), (AG = AdverbGer), (IG = IrregGer), (PX = ParamX) in {

    oper
        insertAdvLeft = overload {
            insertAdvLeft : Str -> R.Verb -> R.Verb = \adv,v -> v ** { s = table { vform => adv ++ v.s ! vform } } ; -- dirty hack to add adverbs to the left of verbs. In German, VP's a1 field which would usually be used in those cases is ignored, we would have to reimplement half of the RGL to make this work
            
            insertAdvLeft : Str -> A -> A = \adv,adj -> adj ** { s = \\d,a => adv ++ adj.s ! d ! a } ;
        } ;
        
        insertAdvLeftInVPNN : Str -> VP -> VP = \adv,vp -> vp ** { nn = \\a => let vpnn = vp.nn ! a in
                        <vpnn.p1, vpnn.p2, vpnn.p3, adv ++ vpnn.p4, vpnn.p5, vpnn.p6> } ;
                        
        insertAdvLeftInVPNNP2 : Str -> VP -> VP = \adv,vp -> vp ** { nn = \\a => let vpnn = vp.nn ! a in
                        <vpnn.p1, adv ++ vpnn.p2, vpnn.p3, vpnn.p4, vpnn.p5, vpnn.p6> } ;
                        
        insertAdvLeftInVPNNP3 : Str -> VP -> VP = \adv,vp -> vp ** { nn = \\a => let vpnn = vp.nn ! a in
                        <vpnn.p1, vpnn.p2, adv ++ vpnn.p3, vpnn.p4, vpnn.p5, vpnn.p6> } ;
        
        insertAdvRight : Str -> A -> A = \adv,adj -> adj ** { s = \\d,a => adj.s ! d ! a ++ adv } ;
        
        insertAdvRightInVP : Str -> VP -> VP = \adv,vp -> vp ** { s = vp.s ** { s = table { vform => vp.s.s ! vform ++ adv } } } ;
        
        SentAPWithComma : AP -> SC -> AP = \ap, sc -> ap ** {
          isPre = P.False ;
          ext = ap.ext ++ Coord.comma ++ sc.s
        } ;
        
        ComplSlashColonObj : VPSlash -> NP -> VP = \vps,np -> let vp = case vps.objCtrl of { P.True => R.objAgr np vps ; _  => vps }
               ** { c2 = vps.c2 ; objCtrl = vps.objCtrl } ;
            in R.insertExtrapos (":" ++ (R.appPrepNP vps.c2 np)) vp ;
            
        ComplSlashAsFollowsColonObj : VPSlash -> NP -> VP = \vps,np -> let vp = case vps.objCtrl of { P.True => R.objAgr np vps ; _  => vps }
               ** { c2 = vps.c2 ; objCtrl = vps.objCtrl } ;
            in R.insertExtrapos ("wie folt :" ++ (R.appPrepNP vps.c2 np)) vp ;
        
        insertSalute : N -> CN -> CN = \s,ne -> ne ** { s = \\adjf => \\num,c => (s.s ! num ! c) ++ (ne.s ! adjf ! num ! c) } ;
        
        CNConj : CN -> Conj -> CN -> CN = \c1,conj,c2 -> c1 ** { s = \\adjf => \\num,c => conj.s1 ++ (c1.s ! adjf ! num ! c) ++ conj.s2 ++ (c2.s ! adjf ! num ! c) } ;
        
        PossNPGeneralPl : CN -> NP -> CN = \cn,np -> cn ** {
            s = \\a,n,c => cn.s ! a ! n ! c ++ np.s ! (R.NPC R.Gen) ++ R.bigNP np 
        } ;
        
        ForceStrongAdj : AP -> CN -> CN = \ap,cn -> 
          let 
            g = cn.g 
          in cn ** {
            s = \\a,n,c => 
                   P.preOrPost ap.isPre
                     (ap.c.p1 ++ ap.c.p2 ++ ap.s ! R.agrAdj g R.Strong n c ++ ap.ext)
                     (cn.s ! a ! n ! c) ;
            g = g
        } ;
        
        PassVS : VS -> VP = \v -> R.insertObj (\\_ => v.s ! R.VPastPart R.APred) (R.predV R.werdenPass) ;
        
        PassV2V : V2V -> VP = \v -> R.insertObj (\\_ => v.s ! R.VPastPart R.APred) (R.predV R.werdenPass) ;
        
        PassV3 : V3 -> VP = \v -> R.insertObj (\\_ => v.s ! R.VPastPart R.APred) (R.predV R.werdenPass) ;
        
        AdjToAdv : A -> Adv = \a -> AG.PositAdvAdj a ;
        
        PrepNP : Prep -> NP -> Adv = \p,np -> AG.PrepNP p np ;
        
        adjConj : A -> Conj -> A -> A = \a1,c,a2 -> a1 ** { s = \\d,af => c.s1 ++ (a1.s ! d ! af) ++ c.s2 ++ (a2.s ! d ! af) } ;
        
        adjCombineComma : A -> A -> A = \a1,a2 -> a1 ** { s = \\d,af => (a1.s ! d ! af) ++ Coord.comma ++ (a2.s ! d ! af) } ;
        
        EURO : Str = "€" | "EUR" ;
        EURO2 : Str = "Euro" ;
        
        appendSCToVP : VP -> SC -> VP = \vp,sc -> vp ** { ext = vp.ext ++ sc.s } ;
        
        appendVInfZuFormToVP : VP -> V -> VP = \vp,v -> vp ** { ext = vp.ext ++ (v.s ! R.VInf P.True) } ;
        
        appendVSInfZuFormToVP : VP -> VS -> VP = \vp,v -> vp ** { ext = vp.ext ++ (v.s ! R.VInf P.True) } ;
        
        sind_und_waren_V : V = 
            lin V { s = table {
                R.VInf P.False => "sein und gewesen sein" ;
                R.VInf P.True => "zu sein und gewesen zu sein" ;
                R.VFin b vf => case vf of {
                    R.VPresInd PX.Sg PX.P1 => "bin und war" ;
                    R.VPresInd PX.Sg PX.P2 => "bist und warst" ;
                    R.VPresInd PX.Sg PX.P3 => "ist und war" ;
                    R.VPresInd PX.Pl PX.P1 => "sind und waren" ;
                    R.VPresInd PX.Pl PX.P2 => "seid und wart" ;
                    R.VPresInd PX.Pl PX.P3 => "sind und waren" ;
                    R.VPresSubj PX.Sg PX.P1 => "sei und wäre" ;
                    R.VPresSubj PX.Sg PX.P2 => "seist und wärest" ;
                    R.VPresSubj PX.Sg PX.P3 => "sei und wäre" ;
                    R.VPresSubj PX.Pl PX.P1 => "seien und wären" ;
                    R.VPresSubj PX.Pl PX.P2 => "seiet und wäret" ;
                    R.VPresSubj PX.Pl PX.P3 => "seien und wären" ;
                    _ => "sind und waren"
                } ;
                R.VImper PX.Sg => "sei und sei gewesen" ;
                R.VImper PX.Pl => "seid und seid gewesen" ;
                R.VPresPart a => "sind und waren" ;
                R.VPastPart a => "sind und waren"
              } ;
              prefix = [] ;
              particle = [] ;
              aux = R.VSein ;
              vtype = R.VAct 
            } ;
        
        UseCompBleiben : Comp -> VP = \comp -> R.insertExtrapos comp.ext (R.insertObj comp.s (R.predV IG.bleiben_V)) ;
        UseCompSindUndWaren : Comp -> VP = \comp -> R.insertExtrapos comp.ext (R.insertObj comp.s (R.predV sind_und_waren_V)) ;
        
        PresPartAP : VP -> AP = \vp ->
          let a = R.agrP3 R.Sg in lin AP {
            s = \\af => (vp.nn ! a).p1 ++ (vp.nn ! a).p2 ++ (vp.nn ! a).p3 ++ vp.a2 ++ vp.adj
                        ++ vp.inf.inpl.p2 ++ (vp.inf.extr ! a) ++ vp.s.s ! R.VPresPart af ;
            isPre = P.True ;
            c = <[],[]> ;
            ext = vp.ext
          } ;
          
        SSubjSInv : S -> Subj -> S -> S = \a,s,b -> lin S {s = \\o => a.s ! o ++ "," ++ s.s ++ b.s ! R.Inv} ;
        
        insertIndirObjIntoV3 : V3 -> NP -> V3 = \v,i -> v ** { s = \\vf => v.c3.s ++ (i.s ! v.c3.c) ++ v.c3.s2 ++ (v.s ! vf) } ;
        
        prefixVPastPartSpace : Str -> R.Verb -> R.Verb = \ein,verb ->
            let
              vs = verb.s ;
              geben = vs ! R.VInf P.False ;
              einb : P.Bool -> Str -> Str = \b,geb -> 
                P.if_then_Str b (ein + geb) geb ;
            in
            {s = table {
              R.VInf P.False => ein + geben ;
              R.VInf P.True  => 
                P.if_then_Str (P.isNil ein) ("zu" ++ geben) (ein + "zu" + geben) ;
              R.VFin b vf => einb b (vs ! R.VFin b vf) ;
              R.VImper n    => vs ! R.VImper n ;
              R.VPresPart a => ein + (R.regA (geben + "d")).s ! R.Posit ! a ;
              R.VPastPart a => ein ++ vs ! R.VPastPart a
              } ;
             prefix = ein ;
             particle = verb.particle ;
             aux = verb.aux ;
             vtype = verb.vtype
             } ;
         
         verbConj = overload {
            verbConj : V -> Conj -> V -> V = \v1,conj,v2 -> v1 ** {s = \\vf => conj.s1 ++ (v1.s ! vf) ++ conj.s2 ++ (v2.s ! vf) } ;
            verbConj : V2 -> Conj -> V2 -> V2 = \v1,conj,v2 -> v1 ** {s = \\vf => conj.s1 ++ (v1.s ! vf) ++ conj.s2 ++ (v2.s ! vf) } ;
         } ;
}