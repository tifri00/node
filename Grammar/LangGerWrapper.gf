{- --# -path=.:present -}

concrete LangGerWrapper of LangWrapper = LangWithDictGer - [hier_Adv, dort_Adv, abhalten_V, PositAdvAdj, PrepNP, in_A, befugt_A, berechtigt_A, firma_N, unternehmen_N, gesellschaft_N, InflectionPrep, InLanguage, how_far_QCl, monthAdv, monthYearAdv, weekdayHabitualAdv, weekdayLastAdv, weekdayNextAdv, weekdayPunctualAdv, yearAdv, in_Prep, an_Prep, von_Prep, werden_V, ConjAdv, alle_Adv, have_V2, mindestens_Adv, ein_Adv, so_Adv, vorschrift_N, herr_N, efficacy_N, weder_Adv, saemtlich_A, an_Adv, tag_N, da_Adv, erloeschen_V, SubjS] ** open SyntaxGer, (CG = ConstructorsGer), Prelude, ParamX, ParadigmsGer, (T = TenseGer), (R = ResGer), (M = MorphoGer), (Coord = Coordination), (CRG = CustomResGer), (IG = IrregGer), (RG = RelativeGer), (XG = ExtendGer), (EXG = ExtraGer) in {

    lincat
        CStatement = S ;
        CFact = Cl ;
        CAction = VP ;
        CInstance = NP ;
        CConcept = CN ;
        CLawSection = CN ;
        CNamedEntity = CN ;
        CDate = CN ;
        CLaw = N ;
        CMonth = { s : Str } ;
        CPercentageCard = Card ;
        CAdvP = Adv ;
        CSC = SC ;

    lin
        -- ========== Building statements ========== --
        
        -- positive statements
        StatementPos t ant f = mkS t ant positivePol f ;
                        
        -- negated statements
        StatementNeg t ant f = mkS t ant negativePol f ;
        
        -- statements prefixed by an adverb, e.g., "schnell rennt er"
        StatementAdv adv s = mkS adv s ;
                    
        -- indicates that the condition for s is cond, i.e., "... if ..." 
        Condition s cond = SSubjS s if_Subj cond ;
        
        -- indicates that the condition for s is cond, i.e., "if ... then ..." 
        Condition2 s cond = mkS if_then_Conj s cond ;
        
        -- indicates that the condition for s is cond, i.e., "wenn ..., ..." 
        Condition3 s cond = ExtAdvS (SubjS if_Subj cond) s ;
        
        -- indicates that the condition for s is cond, e.g., "ist er Geschäftsführer, so ist er berechtigt, ..." 
        Condition4 s cond = lin S { s = \\o => (cond.s ! R.Inv) ++ Coord.comma ++ "so" ++ s.s ! R.Inv } ;
        
        Condition5 s cond = SSubjS s when_Subj cond ;
        Condition6 s cond = SSubjS s soweit_Subj cond ;
        Condition7 s cond = ExtAdvS (SubjS when_Subj cond) s ;
        Condition8 s cond = ExtAdvS (SubjS soweit_Subj cond) s ;
        Condition9 s cond = lin S { s = \\o => (cond.s ! R.Inv) ++ Coord.comma ++ s.s ! R.Inv } ;
        
        -- indicates that the reason for s is r, i.e., "... because ..."
        Reason s r = SSubjS s because_Subj r ;
        Reason2 s r = CRG.SSubjSInv r somit_Subj s ;
        Reason3 s r = CRG.SSubjSInv r wonach_Subj s ;
        
        -- indicates a contradiction of the form "s even though c"
        Contradiction s c = SSubjS s although_Subj c ;
        
        -- connects two sentences via and
        AndS s1 s2 = mkS and_Conj s1 s2 ;
        AndS2 s1 s2 = mkS sowie_Conj s1 s2 ;
        
        -- connects two sentences via or
        OrS s1 s2 = mkS or_Conj s1 s2 ;
        
        
        -- ========== Building facts ==========
        
        -- combines an instance and an action to a fact
        Fact i a = mkCl i a ;
        
        FactAllowedTo a = mkCl (mkSC a) (mkVP its_allowed_to_A) ;
        FactAllowedTo2 a = R.mkClause ((mkSC a).s ++ Coord.comma) (R.agrP3 R.Sg) (mkVP its_allowed_to_A) ;
        FactAllowedTo3 a = ImpersCl (mkVP (mkAP (mkAP its_allowed_to_A) (mkSC a))) ;
        FactAllowedTo4 a = ImpersCl (mkVP (CRG.SentAPWithComma (mkAP its_allowed_to_A) (mkSC a)));
        
        -- e.g., "es wird angemeldet" or just "wird angemeldet" as in "zur Eintragung ins Handelsregister wird angemeldet, dass ..."
        FactImpers = ImpersCl ;
        
        FactObjFocV2 s v o = lin Cl { s = \\m,t,a,p => table {
                    R.Main => (EXG.FocObj o (mkClSlash s (mkVPSlash v))).s ! m ! t ! a ! p ;
                    ord => (Fact s (Action2 v o)).s ! m ! t ! a ! p ! ord
                }
            } ;
        
        NoSubjFact = R.mkClause "" (R.Ag R.Masc Sg P1) ;
        NoSubjFact2 = R.mkClause "" (R.Ag R.Masc Sg P2) ;
        NoSubjFact3 = R.mkClause "" (R.Ag R.Masc Sg P3) ;
        NoSubjFact4 = R.mkClause "" (R.Ag R.Masc Pl P1) ;
        NoSubjFact5 = R.mkClause "" (R.Ag R.Masc Pl P2) ;
        NoSubjFact6 = R.mkClause "" (R.Ag R.Masc Pl P3) ;
        NoSubjFact7 = R.mkClause "" (R.Ag R.Fem Sg P1) ;
        NoSubjFact8 = R.mkClause "" (R.Ag R.Fem Sg P2) ;
        NoSubjFact9 = R.mkClause "" (R.Ag R.Fem Sg P3) ;
        NoSubjFact10 = R.mkClause "" (R.Ag R.Fem Pl P1) ;
        NoSubjFact11 = R.mkClause "" (R.Ag R.Fem Pl P2) ;
        NoSubjFact12 = R.mkClause "" (R.Ag R.Fem Pl P3) ;
        NoSubjFact13 = R.mkClause "" (R.Ag R.Neutr Sg P1) ;
        NoSubjFact14 = R.mkClause "" (R.Ag R.Neutr Sg P2) ;
        NoSubjFact15 = R.mkClause "" (R.Ag R.Neutr Sg P3) ;
        NoSubjFact16 = R.mkClause "" (R.Ag R.Neutr Pl P1) ;
        NoSubjFact17 = R.mkClause "" (R.Ag R.Neutr Pl P2) ;
        NoSubjFact18 = R.mkClause "" (R.Ag R.Neutr Pl P3) ;
        
        FactImpersV2Colon a i = ImpersCl (R.insertExtrapos (":" ++ i.s ! R.NPC R.Nom) (a ** {c2 = R.PrepNom ; objCtrl = False})) ;
        FactImpersV2Colon2 a i = R.mkClause "" (R.agrP3 R.Sg) (R.insertExtrapos (":" ++ i.s ! R.NPC R.Nom) (a ** {c2 = R.PrepNom ; objCtrl = False})) ;
        FactImpersV2Colon3 a i = R.mkClause "" (R.agrP3 R.Pl) (R.insertExtrapos (":" ++ i.s ! R.NPC R.Nom) (a ** {c2 = R.PrepNom ; objCtrl = False})) ;
        
        -- indicates the existence of i, e.g., "there is a man"
        Exists i = mkCl i ;
        
        -- e.g., "er ist berechtigt, ... (Befreiung von den Beschränkungen des § 181 BGB)"
        Annotation f i = f ** { s = \\m,t,a,p,o => (f.s ! m ! t ! a ! p ! o) ++ "(" ++ (i.s ! R.NPC R.Nom) ++ ")" } ;
        
        
        -- ========== Building actions ========== --
        
        -- indicates an action, e.g., "sleep"
        Action v = mkVP v;
        
        ActionAnd v1 v2 = mkVP (CRG.verbConj v1 and_Conj v2);
        
        -- indicates a two-parameter action, e.g., "he sees him"
        Action2 v o = mkVP v o ;
        
        -- e.g., "die Dokumente einholen und entgegennehmen"
        Action2And v1 v2 o = mkVP (CRG.verbConj v1 and_Conj v2) o ;
        
        -- e.g., "er vergewissert sich"
        ActionReflexive v = reflexiveVP v ;
        
        -- indicates a two-parameter action where the object is introduced after a colon, e.g., "er meldet an : Max Mustermann"
        Action2Colon v o = CRG.ComplSlashColonObj (SlashV2a v) o ;
        Action2Colon2 v o = CRG.ComplSlashAsFollowsColonObj (SlashV2a v) o ;
        
        -- indicates a three-parameter action, e.g., "he gives it to him"
        Action3 v o1 o2 = mkVP v o1 o2 ;
        
        -- e.g., "er erlaubt , zu kündigen"
        ActionVV v a = mkVP v a ;
        
        -- e.g., "ich ermächtige ihn, mich zu vertreten"
        ActionV2V v i a = ComplSlash (SlashV2V v a) i ;
        
        -- indicates a sentence-complement action, e.g., "know that he saw me"
        ActionSC v s = mkVP v s ;
        
        -- indicates a sentence-complement action where the sentence complement is introduced after a colon, e.g., "er meldet an: der Satzungssitz der Gesellschaft befindet sich in Nürnberg"
        ActionSCColon v s = R.insertExtrapos (":" ++ s.s ! R.Main) (R.predV v) ;
        
        -- sentences ending with ", was folgt:", e.g., "er meldet an, was folgt: ..."
        ActionSCWhatFollows v s = R.insertExtrapos (Coord.comma ++ "was folgt" ++ ":" ++ s.s ! R.Main) (R.predV v) ;
        
        -- e.g., "er meldet folgendes an: ..."
        ActionSCWhatFollows2 v s = R.insertExtrapos (":" ++ s.s ! R.Main) (R.insertObjNP (MassNP (UseN folgendes_N)) (R.noPreposition R.Acc) ((R.predV v ** {c2 = R.PrepNom ; objCtrl = False}))) ;
        
        -- e.g., "er meldet an was folgt: ..." (without comma)
        ActionSCWhatFollows3 v s = R.insertExtrapos ("was folgt" ++ ":" ++ s.s ! R.Main) (R.predV v) ;
        
        -- indicates a passive action, e.g., "to be seen"
        ActionPass v = passiveVP v;
        
        -- e.g., "ich wurde durch den Notar belehrt"
        ActionPassActor v i = Mod2 (ActionPass v) (DurchAdv i) ;
        
        -- e.g. "ich wurde von dem Notar belehrt"
        ActionPassActor2 v i = Mod2 (ActionPass v) (CRG.PrepNP LangGerWrapper.von_Prep i) ;
        
        -- e.g., "er wurde ermächtigt , zu ..."
        ActionPassV2V v sc = R.insertExtrapos (Coord.comma ++ sc.s) (CRG.PassV2V v) ;
        
        -- e.g., "wird angemeldet, dass ..."
        ActionPassSC v s = R.insertExtrapos (Coord.comma ++ R.conjThat ++ s.s ! R.Sub) ((CRG.PassVS v) ** {c2 = R.PrepNom ; objCtrl = False}) ;
        
        -- e.g., "es wird angemeldet: ..."
        ActionPassSCColon v s = R.insertExtrapos (":" ++ s.s ! R.Main) ((CRG.PassVS v) ** {c2 = R.PrepNom ; objCtrl = False}) ;
        
        -- passive sentences ending with ", was folgt:", e.g., "es wird angemeldet, was folgt: ..."
        ActionPassSCWhatFollows v s = R.insertExtrapos (Coord.comma ++ "was folgt" ++ ":" ++ s.s ! R.Main) ((CRG.PassVS v) ** {c2 = R.PrepNom ; objCtrl = False}) ;
        
        -- e.g., "es wurde ihm gewährt"
        ActionPassV3 v i = CRG.PassV3 (CRG.insertIndirObjIntoV3 v i) ;
        
        -- indicates a property action, e.g., "be beautiful", also "bleibt unberührt"
        PropAction a = mkVP a ;
        PropAction3 a = CRG.UseCompBleiben (CompAP (PositA a)) ;
        PropAction5 a = CRG.UseCompSindUndWaren (CompAP (PositA a)) ;
        
        -- e.g., "es ist mir untersagt"
        PropAction2 a i = mkVP a i ;
        PropAction4 a i = CRG.UseCompBleiben (CompAP (ComplA2 a i)) ;
        PropAction6 a i = CRG.UseCompSindUndWaren (CompAP (ComplA2 a i)) ;
        
        -- e.g., "sie ist wohnhaft : Berlin"
        PropActionColon a i =  R.insertExtrapos (":" ++ i.s ! a.c2.c) (mkVP (lin A { s = a.s })) ;
        
        -- e.g., "schnell und langsam"
        PropActionAnd a1 a2 = AdvActionAnd (CRG.AdjToAdv a1) (CRG.AdjToAdv a2) ;
        PropActionOr a1 a2 = AdvActionOr (CRG.AdjToAdv a1) (CRG.AdjToAdv a2) ;
        
        -- indicates a comparative property action, e.g., "be more beautiful than him"
        PropCompareAction a n = mkVP a n ;
        
        -- indicates an adverbial action, e.g., "be there"
        AdvAction adv = mkVP adv ;
        
        -- e.g., "er ist berechtigt und befreit von ..."
        AdvActionAnd adv1 adv2 = AdvAction (AdvAnd adv1 adv2) ;
        AdvActionOr adv1 adv2 = AdvAction (AdvOr adv1 adv2) ;
        
        -- indicates the action of being an i, e.g., "to be a lawyer"
        Is i = mkVP i ;
        
        -- indicates a the action of owning i, e.g., "have a car"
        Own i = mkVP have_V2 i ;
        
        LocationAction l = mkVP (CRG.PrepNP LangGerWrapper.in_Prep l); -- would like to use LocationAdv here but it's not possible computationally (at least with my devices)
        
        -- indicates a modification of an action, e.g., "run fast"
        Mod a adv = mkVP a adv ;
        Mod2 a adv = CRG.insertAdvLeftInVPNN adv.s a ;
        Mod3 a adv = CRG.insertAdvRightInVP adv.s a ;
        Mod4 a adv = CRG.insertAdvLeftInVPNNP2 adv.s a ;
        Mod5 a adv = CRG.insertAdvLeftInVPNNP3 adv.s a ;
        ModA a adj = mkVP a (CRG.AdjToAdv adj) ;
        ModA2 a adj = CRG.insertAdvLeftInVPNN (CRG.AdjToAdv adj).s a ;
        ModA3 a adj = CRG.insertAdvRightInVP (CRG.AdjToAdv adj).s a ;
        ModA4 a adj = CRG.insertAdvLeftInVPNNP2 (CRG.AdjToAdv adj).s a ;
        ModA5 a adj = CRG.insertAdvLeftInVPNNP3 (CRG.AdjToAdv adj).s a ;
         
        -- indicates that an action is possible, e.g., "can see you"
        Possibility a = mkVP can8know_VV a ;
        Possibility2 a = mkVP can_VV a ;
        
        -- indicates that an action is obligatory, e.g., "must work"
        Obligation a = mkVP must_VV a ;
        
        -- e.g., "er ist verpflichtet, zu ..."
        ObligationSC sc = mkVP (mkAP (mkAP obliged_to_A) sc) ;
        ObligationSC2 sc = mkVP (CRG.SentAPWithComma (mkAP obliged_to_A) sc) ;
        ObligationSC3 sc = CRG.appendSCToVP (mkVP IG.haben_V) sc ;
        
        -- e.g., "es ist zu gewähren"
        ObligationPass v = CRG.appendVInfZuFormToVP (mkVP IG.sein_V) v ;
        
        -- e.g., "es ist anzumelden , dass ..."
        ObligationPassSC v sc = R.insertExtrapos (Coord.comma ++ R.conjThat ++ sc.s ! R.Sub) ((CRG.appendVSInfZuFormToVP (mkVP IG.sein_V) v) ** {c2 = R.PrepNom ; objCtrl = False}) ;
        
        -- indicates that an action is allowed, e.g., "may go"
        Permission a = mkVP may_VV a ;
        Permission2 a = mkVP can_VV a ;
        
        -- e.g., "er ist berechtigt, zu ..."
        PermissionSC sc = mkVP (CRG.SentAPWithComma (mkAP allowed_to_A) sc) ;
        PermissionSC2 sc = mkVP (CRG.SentAPWithComma (mkAP allowed_to_2_A) sc) ;
        
        -- indicates that an action is being desired, e.g., "want to go"
        Desire a = mkVP want_VV a ;
        
        
        -- ========== Building instances ========== --
        
        -- creates instances from concepts with an optional determiner or quantifier and from pronouns
        Instance = mkNP the_Det ;
        Instance2 = mkNP thePl_Det ;
        Instance3 = mkNP a_Det ;
        Instance4 = mkNP someSg_Det ;
        Instance5 = mkNP somePl_Det ;
        Instance6 = mkNP this_Quant ;
        Instance7 = mkNP that_Quant ;
        Instance8 = mkNP he_Pron ;
        Instance9 = mkNP i_Pron ;
        Instance10 = mkNP it_Pron ;
        Instance11 = mkNP she_Pron ;
        Instance12 = mkNP they_Pron ;
        Instance13 = mkNP we_Pron ;
        Instance14 = mkNP youSg_Pron ;
        Instance15 = mkNP youPl_Pron ;
        Instance16 = mkNP youPol_Pron ;
        Instance17 = mkNP ;
        Instance18 = DetCN (DetQuant IndefArt NumPl) ;
        InstanceOwnerPronSg = mkNP dessen_Pron ;
        InstanceOwnerPronPl = mkNP deren_Pron ;
        
        -- e.g., "every man"
        InstanceEvery = mkNP every_Det ;
        InstanceEvery2 = mkNP all_Det ;
        InstanceEvery3 = mkNP saemtlich_Det ;
        
        -- e.g., "few people"
        InstanceFew = mkNP few_Det ;
        
        -- e.g., "many people"/"much money"
        InstanceMany = mkNP many_Det ;
        InstanceMany2 = mkNP much_Det ;
        
        -- e.g., "no woman"
        InstanceNone = mkNP no_Quant ;
        
        -- creates instances from city names, person names, etc.
        InstancePN = UsePN ;
        
        -- creates instances from pronouns
        InstancePron = UsePron ;
        
        -- creates instances from nouns that are quantified with a number (as digits), e.g., "7 huge mountains"
        InstanceDigits cn n = lin NP {
          s = \\c => n.s ++ R.usePrepC c (\k -> cn.s ! R.Strong ! Pl ! k) ++ cn.adv ;
          a = R.agrgP3 cn.g Pl ;
          w = R.WLight ;
          rc = cn.rc ! Pl ;
          ext = cn.ext
        } ;
        
        -- creates instances from nouns that are quantified with a number, e.g., "seven huge mountains"
        -- InstanceNumeral c n = mkNP n c ; -- removed in favor of InstanceCard (can be called with NumNumeral : Numeral -> Card)
        
        -- creates instances form nouns that are quantified with a cardinality, e.g., "almost ten people"
        InstanceCard c n = mkNP n c ;
        
        -- e.g., "the man and the woman"
        And i1 i2 = mkNP and_Conj i1 i2 ;
        And2 i1 i2 = mkNP sowie_Conj i1 i2 ;
        And3 i1 i2 = mkNP both7and_DConj i1 i2 ;
        And4 i1 i2 = mkNP comma_Conj i1 i2 ;
        And5 i1 i2 = mkNP bzw_Conj i1 i2 ;
        And6 i1 i2 = mkNP slash_Conj i1 i2 ;
        
        -- e.g., "the man or the woman"
        Or i1 i2 = mkNP or_Conj i1 i2 ;
        
        -- e.g., "Max Mustermann, geboren in Nürnberg"
        CommaDescription i a = i ** { s = \\pc => i.s ! pc ++ Coord.comma ++ a.s ! R.Posit ! R.APred ++ Coord.comma } ;
        CommaDescription2 i a = i ** { s = \\pc => i.s ! pc ++ Coord.comma ++ a.s ! R.Posit ! R.APred } ;
        
        -- the hometown is sometimes written without any further description, e.g., "Max Mustermann , Musterstadt" where Musterstadt is the hometown
        CommaDescriptionHometownMasc i t = i ** { s = \\pc => i.s ! pc ++ Coord.comma ++ "NoDENamedEntityMasc{" ++ t.s ++ "}" ++ Coord.comma } ;
        CommaDescriptionHometownMasc2 i t = i ** { s = \\pc => i.s ! pc ++ Coord.comma ++ "NoDENamedEntityMasc{" ++ t.s ++ "}" } ;
        CommaDescriptionHometownFem i t = i ** { s = \\pc => i.s ! pc ++ Coord.comma ++ "NoDENamedEntityFem{" ++ t.s ++ "}" ++ Coord.comma } ;
        CommaDescriptionHometownFem2 i t = i ** { s = \\pc => i.s ! pc ++ Coord.comma ++ "NoDENamedEntityFem{" ++ t.s ++ "}" } ;
        CommaDescriptionHometownNeut i t = i ** { s = \\pc => i.s ! pc ++ Coord.comma ++ "NoDENamedEntityNeut{" ++ t.s ++ "}" ++ Coord.comma } ;
        CommaDescriptionHometownNeut2 i t = i ** { s = \\pc => i.s ! pc ++ Coord.comma ++ "NoDENamedEntityNeut{" ++ t.s ++ "}" } ;
        
        -- the hometown is sometimes written just using "wohnhaft" without preposition "in", e.g., "Max Mustermann , wohnhaft Musterstadt" where Musterstadt is the hometown
        CommaDescriptionHometown2 i t = i ** { s = \\pc => i.s ! pc ++ Coord.comma ++ "wohnhaft" ++ (t.s ! (R.NPC R.Dat)) ++ Coord.comma } ;
        CommaDescriptionHometown3 i t = i ** { s = \\pc => i.s ! pc ++ Coord.comma ++ (t.s ! (R.NPC R.Dat)) } ;
        CommaDescriptionHometown4 i t = i ** { s = \\pc => i.s ! pc ++ Coord.comma ++ "wohnhaft :" ++ (t.s ! (R.NPC R.Dat)) ++ Coord.comma } ;
        
        -- the date of birth is sometimes written just using "geboren" without preposition "an", e.g., "Max Mustermann , geboren 01 . 01 . 2000"
        CommaDescriptionBirthday i b = i ** { s = \\pc => i.s ! pc ++ Coord.comma ++ "geboren" ++ (b.s ! R.Weak ! Sg ! R.Dat) } ;
        CommaDescriptionBirthday2 i b = i ** { s = \\pc => i.s ! pc ++ Coord.comma ++ "geboren" ++ (b.s ! R.Weak ! Sg ! R.Dat) ++ Coord.comma } ;
        CommaDescriptionBirthday3 i b = i ** { s = \\pc => i.s ! pc ++ Coord.comma ++ "geb." ++ (b.s ! R.Weak ! Sg ! R.Dat) } ;
        CommaDescriptionBirthday4 i b = i ** { s = \\pc => i.s ! pc ++ Coord.comma ++ "geb." ++ (b.s ! R.Weak ! Sg ! R.Dat) ++ Coord.comma } ;
        
        -- indicates that i's location is loc
        Location i loc = AdvNP i (CRG.PrepNP above_Prep loc) ;
        Location2 i loc = AdvNP i (CRG.PrepNP behind_Prep loc) ;
        Location3 i loc = AdvNP i (CRG.PrepNP between_Prep loc) ;
        Location4 i loc = AdvNP i (CRG.PrepNP in8front_Prep loc) ;
        Location5 i loc = AdvNP i (CRG.PrepNP LangGerWrapper.in_Prep loc) ;
        Location6 i loc = AdvNP i (CRG.PrepNP on_Prep loc) ;
        Location7 i loc = AdvNP i (CRG.PrepNP under_Prep loc) ;
        
        -- e.g., "die Eintragung in das Handelsregister"
        Target i t = AdvNP i (TargetAdv t) ;
        Target2 i t = AdvNP i (TargetAdv2 t) ;
        Target3 i t = AdvNP i (TargetAdv3 t) ;
        
        -- e.g., "die Vertretungsberechtigung gegenüber ..."
        Referring i r = AdvNP i (ReferringAdv r) ;
        Referring2 i r = AdvNP i (ReferringAdv2 r) ;
        Referring3 i r = AdvNP i (ReferringAdv3 r) ;
        Referring4 i r = AdvNP i (ReferringAdv4 r) ;
        Referring5 i r = AdvNP i (ReferringAdv5 r) ;
        Referring6 i r = AdvNP i (ReferringAdv6 r) ;
        
        -- e.g., "der Mann aus Nürnberg"
        Origin i orig = AdvNP i (OriginAdv orig) ;
        
        -- e.g., "Eintragung nach § ..."
        AccordingTo i t = AdvNP i (AccordingToAdv t) ;
        AccordingTo2 i t = AdvNP i (AccordingToAdv2 t) ;
        AccordingTo3 i t = AdvNP i (AccordingToAdv3 t) ;
        AccordingTo4 i t = AdvNP i (AccordingToAdv4 t) ;
        AccordingTo5 i t = AdvNP i (AccordingToAdv5 t) ;
        AccordingTo6 i t = AdvNP i (AccordingToAdv6 t) ;
        
        -- e.g., "Einwilligungsvorbehalt ( § 1903 BGB )"
        AccordingToLawSection i l = AdvNP i (AccordingToLawSectionAdv l) ;
        
        -- e.g., "der neue Geschäftsführer neben der aktuellen Geschäftsführerin"
        Besides i t = AdvNP i (BesidesAdv t) ;
        
        
        -- creates an instance that represents a percentage of the given instance, e.g., "75% of the people"
        InstancePercentage i pc = AdvNP {
            s = \\c => (pc.s ! R.Neutr ! R.Gen) ;
            a = R.agrgP3 neuter pc.n ;
            w = R.WLight ;
            rc, ext = []
        } (CRG.PrepNP genPrep i) ;
        
        
        -- ========== Building concepts ========== --
        
        -- builds concepts from nouns
        Concept n = mkCN n ;
        
        -- builds concepts from CLawSection
        ConceptLawSection l = l ;
        
        -- builds concepts from CNamedEntity
        ConceptNamedEntity ne = ne ;
        
        -- builds concepts from CDate
        ConceptDate d = d ;
        
        -- concept with relative clause
        ConceptRel c a = mkCN c (mkRS (mkRCl RG.IdRP a)) ;
        
        -- and/or of two concepts, e.g., "Männer und Frauen ..."
        ConceptAnd c1 c2 = CRG.CNConj c1 and_Conj c2 ;
        ConceptAnd2 c1 c2 = CRG.CNConj c1 sowie_Conj c2 ;
        ConceptOr c1 c2 = CRG.CNConj c1 or_Conj c2 ;
        
        -- build concepts from named entities with salutation, e.g., "Herr Maximus Mustermann"
        NEConceptWithSaluteMasc s n = CRG.insertSalute s (NEConceptMasc n) ;
        NEConceptWithSaluteFem s n = CRG.insertSalute s (NEConceptFem n) ;
        NEConceptWithSaluteNeut s n = CRG.insertSalute s (NEConceptNeut n) ;
          
        -- build concepts from named entities
        NEConceptMasc n = lin CN {
            s = \\_ => \\_,_ => "NoDENamedEntityMasc{" ++ n.s ++ "}";
            g = masculine ;
            rc = \\_ => [] ;
            ext,adv = [] 
          } ;
        NEConceptFem n = lin CN {
            s = \\_ => \\_,_ => "NoDENamedEntityFem{" ++ n.s ++ "}";
            g = feminine ;
            rc = \\_ => [] ;
            ext,adv = [] 
          } ;
        NEConceptNeut n = lin CN {
            s = \\_ => \\_,_ => "NoDENamedEntityNeut{" ++ n.s ++ "}";
            g = neuter ;
            rc = \\_ => [] ;
            ext,adv = [] 
          } ;
        
        -- indicates that i is owned by owner
        Owner c owner = PossNP c owner ;
        Owner2 c owner = AdvCN c (CRG.PrepNP genPrep owner) ;
        Owner3 c owner = CRG.PossNPGeneralPl c owner ;
        
        -- applies an adjective to a concept
        Prop c a = mkCN a c ;
        Prop2 c a = CRG.ForceStrongAdj (PositA a) c ;
        
        -- e.g., "der dem Termin folgende Tag"
        ActionProp c a = mkCN (CRG.PresPartAP a) c ;
        
        -- e.g., "das geliebte Kind"
        PassActionProp2 c v = mkCN (XG.PastPartAP (mkVPSlash v)) c ;
        
        -- e.g., "die der Gesellschaft mitgeteilte Adresse"
        PassActionProp3 c v i = mkCN (XG.PastPartAP (Slash3V3 v i)) c ;
        
        -- builds LawSection trees from a section number and a law name, e.g., "§ 181 BGB"
        LawSection n l = lin CN {
            s = \\_ => \\_,c => ("§" ++ n.s ++ l.s ! Sg ! R.Nom) ;
            g = masculine ;   
            rc = \\_ => [] ;
            ext,adv = [] 
          } ;
          
        -- e.g., "§ 51 Abs. 1 GmbHG"
        LawSectionWithParagraph s p l = lin CN {
            s = \\_ => \\_,c => ("§" ++ s.s ++ "Abs." ++ p.s ++ l.s ! Sg ! R.Nom) ;
            g = masculine ;   
            rc = \\_ => [] ;
            ext,adv = [] 
          } ;
        
        -- e.g., "§ 51 Abs. 1 S. 2 GmbHG"
        LawSectionWithParagraphAndSentence s p sen l = lin CN {
            s = \\_ => \\_,c => ("§" ++ s.s ++ "Abs." ++ p.s ++ "S." ++ sen.s ++ l.s ! Sg ! R.Nom) ;
            g = masculine ;   
            rc = \\_ => [] ;
            ext,adv = [] 
          } ;
        LawSectionWithParagraphAndSentence2 s p sen l = lin CN {
            s = \\_ => \\_,c => ("§" ++ s.s ++ "Abs." ++ p.s ++ "Satz" ++ sen.s ++ l.s ! Sg ! R.Nom) ;
            g = masculine ;   
            rc = \\_ => [] ;
            ext,adv = [] 
          } ;
          
        -- builds LawSection trees from a section number without a law name, e.g., "§ 181"
        LawSectionNumOnly n = lin CN {
            s = \\_ => \\_,c => ("§" ++ n.s) ;
            g = masculine ;   
            rc = \\_ => [] ;
            ext,adv = [] 
          } ;
          
        LawSectionNumOnlyWithParagraph n p = lin CN {
            s = \\_ => \\_,c => ("§" ++ n.s ++ "Abs." ++ p.s) ;
            g = masculine ;   
            rc = \\_ => [] ;
            ext,adv = [] 
          } ;
          
        LawSectionNumOnlyWithParagraphAndSentence n p sen = lin CN {
            s = \\_ => \\_,c => ("§" ++ n.s ++ "Abs." ++ p.s ++ "S." ++ sen.s) ;
            g = masculine ;   
            rc = \\_ => [] ;
            ext,adv = [] 
          } ;
          
        LawSectionNumOnlyWithParagraphAndSentence2 n p sen = lin CN {
            s = \\_ => \\_,c => ("§" ++ n.s ++ "Abs." ++ p.s ++ "Satz" ++ sen.s) ;
            g = masculine ;   
            rc = \\_ => [] ;
            ext,adv = [] 
          } ;
          
        -- builds HRB number trees from a HRB number, e.g, "HRB Nr. 12345"
        HRBNr n = lin CN {
            s = \\_ => \\_,c => "HRB Nr." ++ n.s ;
            g = feminine ;   
            rc = \\_ => [] ;
            ext,adv = [] 
          } ;
        HRBNr2 n = lin CN {
            s = \\_ => \\_,c => "HRB" ++ n.s ;
            g = feminine ;   
            rc = \\_ => [] ;
            ext,adv = [] 
          } ;
          
        -- builds dates from a day, month and year integer, e.g., "4.7.2023"
        Date d m y = lin CN {
            s = \\_ => \\_,c => d.s ++ "." ++ m.s ++ "." ++ y.s ;
            g = masculine ;
            rc = \\_ => [] ;
            ext,adv = []
        } ;
        
        -- builds dates from a day, month (as word) and year integer, e.g., "4. Mai 2023"
        Date2 d m y = lin CN {
            s = \\_ => \\_,c => d.s ++ "." ++ m.s ++ y.s ;
            g = masculine ;
            rc = \\_ => [] ;
            ext,adv = []
        } ;
        
        -- builds price in euros from a single integer (no cents), e.g., "10 €"
        Price n = lin CN {
            s = \\_ => \\_,_ => n.s ++ CRG.EURO ;
            g = masculine ;
            rc = \\_ => [] ;
            ext,adv = []
        } ;
        
        -- same as Price with a different notation for euro (due to compilation speed problems)
        Price2 n = lin CN {
            s = \\_ => \\_,_ => n.s ++ "," ++ "--" ++ CRG.EURO ;
            g = masculine ;
            rc = \\_ => [] ;
            ext,adv = []
        } ;
        
        Price3 n = lin CN {
            s = \\_ => \\_,_ => n.s ++ CRG.EURO2 ;
            g = masculine ;
            rc = \\_ => [] ;
            ext,adv = []
        } ;
        
        Price4 n = lin CN {
            s = \\_ => \\_,_ => n.s ++ "," ++ "--" ++ CRG.EURO2 ;
            g = masculine ;
            rc = \\_ => [] ;
            ext,adv = []
        } ;
        
        -- builds price in euros from two integers, e.g., "2,50 €"
        PriceComma e c = lin CN {
            s = \\_ => \\_,_ => e.s ++ "," ++ c.s ++ CRG.EURO ;
            g = masculine ;
            rc = \\_ => [] ;
            ext,adv = []
        } ;
        
        -- same as PriceComma with a different notation for euro (due to compilation speed problems)
        PriceComma2 e c = lin CN {
            s = \\_ => \\_,_ => e.s ++ "," ++ c.s ++ CRG.EURO2 ;
            g = masculine ;
            rc = \\_ => [] ;
            ext,adv = []
        } ;
        
        Address zip c s n = lin CN {
            s = \\x => \\y,z => ((ZipAndCity zip c).s ! x ! y ! z) ++ Coord.comma ++ ((StreetAndNr s n).s ! x ! y ! z) ;
            g = masculine ;
            rc = \\_ => [] ;
            ext,adv = []
        } ;
        
        Address2 zip c s n = lin CN {
            s = \\x => \\y,z => ((StreetAndNr s n).s ! x ! y ! z) ++ Coord.comma ++ ((ZipAndCity zip c).s ! x ! y ! z) ;
            g = masculine ;
            rc = \\_ => [] ;
            ext,adv = []
        } ;
        
        ZipAndCity zip c = lin CN {
            s = \\x => \\y,z => zip.s ++ (c.s ! x ! y ! z) ;
            g = masculine ;
            rc = \\_ => [] ;
            ext,adv = []
        } ;

        StreetAndNr s n = lin CN {
            s = \\x => \\y,z => (s.s ! x ! y ! z) ++ n.s ;
            g = masculine ;
            rc = \\_ => [] ;
            ext,adv = []
        } ;
          
          
        -- ========== Misc ========== -
        
        MakeCAdvP adv = adv ;
        
        AdvAnd adv1 adv2 = CG.mkAdv and_Conj adv1 adv2 ;
        
        AdvOr a1 a2 = CG.mkAdv or_Conj a1 a2 ;
        
        AdvNeitherNor a1 a2 = CG.mkAdv neither_nor_Conj a1 a2 ;
        
        Percentage n = lin Card {
            s = \\g,c => (n.s ++ "%") | (n.s ++ "Prozent") ;
            n = Pl | Sg
        } ;
        AtLeastP pc = mkCard at_least_AdN pc ;
        AtLeastP2 pc = mkCard mindestens_AdN pc ;
        AtMostP pc = mkCard at_most_AdN pc ;
        AlmostP pc = mkCard almost_AdN pc ;
        
        IntCard n = lin Card {
            s = \\g,c => n.s ;
            n = Pl | Sg
        } ;
        
        CardOr c1 c2 = lin Card {
            s = \\g,c => (c1.s ! g ! c) ++ "oder" ++ (c2.s ! g ! c) ;
            n = case c1.n of {
                Sg => c2.n ;
                Pl => Pl
            }
        } ;
        
        AtLeast n = mkCard at_least_AdN n ;
        AtLeast2 n = mkCard mindestens_AdN n ;
        AtMost n = mkCard at_most_AdN n ;
        Almost n = mkCard almost_AdN n ;
          
        ZurAdv = CRG.PrepNP LangGerWrapper.zu_Prep ;
        AsAdv = CRG.PrepNP als_Prep ;
        WhenExistsAdv = CRG.PrepNP bei_vorhandensein_von_Prep ;
        ReferringAdv = CRG.PrepNP bezueglich_Prep ;
        ReferringAdv2 = CRG.PrepNP gegenueber_Prep ;
        ReferringAdv3 = CRG.PrepNP with_regards_to_Prep ;
        ReferringAdv4 = CRG.PrepNP with_regards_to_2_Prep ;
        ReferringAdv5 = CRG.PrepNP with_regards_to_acc_Prep ;
        ReferringAdv6 = CRG.PrepNP ueber_acc_Prep ;
        LocationAdv = CRG.PrepNP above_Prep ;
        LocationAdv2 = CRG.PrepNP behind_Prep ;
        LocationAdv3 = CRG.PrepNP between_Prep ;
        LocationAdv4 = CRG.PrepNP in8front_Prep ;
        LocationAdv5 = CRG.PrepNP LangGerWrapper.in_Prep ;
        LocationAdv6 = CRG.PrepNP on_Prep ;
        LocationAdv7 = CRG.PrepNP under_Prep ;
        LocationAdv8 = CRG.PrepNP bei_Prep ;
        PartAdv = CRG.PrepNP part_Prep ;
        TargetAdv = CRG.PrepNP in_acc_Prep ;
        TargetAdv2 = CRG.PrepNP an_acc_Prep ;
        TargetAdv3 = CRG.PrepNP auf_acc_Prep ;
        DateAdv d m y = CRG.PrepNP an_Prep (Instance (Date d m y)) ;
        DateAdv2 d m y = CRG.PrepNP (R.noPreposition R.Acc) (Instance (Date d m y)) ;
        DateAdv3 d m y = CRG.PrepNP an_Prep (Instance (Date2 d m y)) ;
        DateAdv4 d m y = CRG.PrepNP (R.noPreposition R.Acc) (Instance (Date2 d m y)) ;
        DurchAdv = CRG.PrepNP by8agent_Prep ;
        MitAdv = CRG.PrepNP by8means_Prep ;
        MitAdv2 = CRG.PrepNP per_Prep ;
        UnterVerzichtAdv = CRG.PrepNP unter_verzicht_auf_Prep ;
        OhneAdv = CRG.PrepNP ohne_Prep ;
        ForAdv = CRG.PrepNP fuer_Prep ;
        DisregardingAdv = CRG.PrepNP ohne_ruecksicht_auf_Prep ;
        OriginAdv = CRG.PrepNP aus_Prep ;
        AccordingToAdv = CRG.PrepNP nach_Prep ;
        AccordingToAdv2 = CRG.PrepNP im_sinne_von_Prep ;
        AccordingToAdv3 = CRG.PrepNP im_sinne_von_dat_Prep ;
        AccordingToAdv4 = CRG.PrepNP gemaess_Prep ;
        AccordingToAdv5 = CRG.PrepNP im_sinne_von_dat_abbr_Prep ;
        AccordingToAdv6 = CRG.PrepNP im_sinne_von_gen_abbr_Prep ;
        AccordingToLawSectionAdv l = CRG.PrepNP brackets_Prep (Instance17 (ConceptLawSection l)) ;
        DueToAdv = CRG.PrepNP wegen_Prep ;
        BesidesAdv = CRG.PrepNP neben_Prep ;
        WithEffectToAdv d m y = CRG.PrepNP with_effect_to_Prep (Instance (Date d m y)) ;
        
        AdverbialInsertion a = a ** { s = Coord.comma ++ "also auch" ++ a.s ++ Coord.comma } ;
        AdverbialInsertion2 a = a ** { s = Coord.comma ++ "also auch" ++ a.s } ;
        AdverbialInsertion3 a = a ** { s = Coord.comma ++ "somit auch" ++ a.s ++ Coord.comma } ;
        AdverbialInsertion4 a = a ** { s = Coord.comma ++ "somit auch" ++ a.s } ;
        AdverbialInsertion5 a = a ** { s = Coord.comma ++ a.s ++ Coord.comma } ;
        AdverbialInsertion6 a = a ** { s = Coord.comma ++ a.s } ;
        
        AdverbialInsertionNeg a = a ** { s = Coord.comma ++ "also auch nicht" ++ a.s ++ Coord.comma } ;
        AdverbialInsertionNeg2 a = a ** { s = Coord.comma ++ "also auch nicht" ++ a.s } ;
        AdverbialInsertionNeg3 a = a ** { s = Coord.comma ++ "somit auch nicht" ++ a.s ++ Coord.comma } ;
        AdverbialInsertionNeg4 a = a ** { s = Coord.comma ++ "somit auch nicht" ++ a.s } ;
        
        -- e.g., "darüber , dass falsche Versicherungen strafbar sind"
        ThatAdv s = SubjS that_Subj s ;
        
        ModLeftA adj adv = (CRG.insertAdvLeft adv.s adj) ;
        ModLeftAA adj modAdj = (CRG.insertAdvLeft (CRG.AdjToAdv modAdj).s adj) ;
        
        ModRightA adj adv = (CRG.insertAdvRight adv.s adj) ;
        ModRightAA adj modAdj = (CRG.insertAdvRight (CRG.AdjToAdv modAdj).s adj) ;
        
        AdjAnd adj1 adj2 = CRG.adjConj adj1 and_Conj adj2 ;
        AdjAnd2 adj1 adj2 = CRG.adjCombineComma adj1 adj2 ;
        AdjOr adj1 adj2 = CRG.adjConj adj1 or_Conj adj2 ;
        
        -- builds a SC from an action
        SCFromAct a = mkSC a ;
        
        -- e.g., "er ist berechtigt, zu entscheiden, die Entscheidung zu blockieren und ..."
        SCAnd sc1 sc2 = sc1 ** { s = sc1.s ++ Coord.comma ++ sc2.s } ;
        SCAnd2 sc1 sc2 = sc1 ** { s = sc1.s ++ "und" ++ sc2.s } ;
        SCAnd3 sc1 sc2 = sc1 ** { s = sc1.s ++ "sowie" ++ sc2.s } ;
        
        
        -- ========== Additional vocabulary ========== --

        abberufen_A = mkA "abberufen" ;
        abgeben_V2 = mkV2 (prefixV "ab" (irregV "geben" "gibt" "gab" "gäbe" "gegeben")) ;
        abhalten_V2 = mkV2 (prefixV "ab" (irregV "halten" "hält" "hielt" "hielte" "gehalten")) ;
        abkommen_ueber_eu_wirtschaftsraum_N = R.mkN "Abkommen über den Europäischen Wirtschaftsraum" "Abkommen über den Europäischen Wirtschaftsraum" "Abkommen über den Europäischen Wirtschaftsraum" "Abkommens über den Europäischen Wirtschaftsraum" "Abkommen über den Europäischen Wirtschaftsraum" "Abkommen über den Europäischen Wirtschaftsraum" "Abkommen über den Europäischen Wirtschaftsraum" neuter ;
        absendung_N = mkN "Absendung" "Absendungen" feminine ;
        aendern_V2 = mkV2 (irregV "ändern" "ändert" "änderte" "ändere" "geändert") ;
        all_Det = M.detLikeAdj False Pl "all" ;
        alleinvertretungsberechtigt_A = mkA "alleinvertretungsberechtigt" ;
        allowed_to_A = regA "befugt" | regA "berechtigt" ;
        allowed_to_2_A = regA "bevollmächtigt" | regA "ermächtigt" ;
        als_Prep = mkPrep "als" nominative ;
        amtlich_Adv = ss "amtlich" ;
        amtsnachfolger_N = mkN "Amtsnachfolger" "Amtsnachfolger" masculine ;
        an_Prep = mkPrep [] (R.NPP R.CAnDat) | mkPrep "an" dative ;
        an_acc_Prep = mkPrep "an" accusative ;
        anders_A = mkA "anders" "ander" "anderer" "anderste" | mkA "weiter" "weiter" "weiterer" "weiterste" ;
        angestellter_N = R.mkN "Angestellter" "Angestellten" "Angestellten" "Angestellten" "Angestellten" "Angestellten" "Angestellten" masculine ;
        anmelden_VS = mkVS (prefixV "an" (regV "melden")) ;
        anmelden_V2 = mkV2 (prefixV "an" (regV "melden")) ;
        anteilsteilung_N = mkN "Anteilsteilung" "Anteilsteilungen" feminine ;
        anzeigen_V2 = mkV2 anzeigen_V ;
        anzeigen_V3 = mkV3 anzeigen_V ;
        april_CMonth = ss "April" ;
        auf_acc_Prep = mkPrep "auf" accusative ;
        august_CMonth = ss "August" ;
        aus_Prep = mkPrep "aus" dative ;
        auskunftspflicht_N = mkN "Auskunfts" pflicht_N ;
        ausland_N = mkN "Ausland" "Ausländer" neuter ;
        beachtung_N = mkN "Beachtung" "Beachtungen" feminine ;
        beantragen_V2 = mkV2 (irregV "beantragen" "beantragt" "beantragte" "beantrage" "beantragt") ;
        beauftragen_V2 = mkV2 (irregV "beauftragen" "beauftragt" "beauftragte" "beauftrage" "beauftragt") ;
        beauftragen_V3 = mkV3 (irregV "beauftragen" "beauftragt" "beauftragte" "beauftrage" "beauftragt") (R.noPreposition R.Acc) mit_Prep ;
        befinden_refl_V = reflV befinden_V accusative ;
        beglaubigen_V2 = mkV2 (irregV "beglaubigen" "beglaubigt" "beglaubigte" "beglaubige" "beglaubigt") ;
        beglaubigend_A = mkA "beglaubigend" ;
        beglaubigt_A = mkA "beglaubigt" | { s = \\d,a => "bgl." } ;
        begruenden_V2 = mkV2 (irregV "begründen" "begründet" "begründete" "begründe" "begründet") ;
        begruenden_VS = mkVS (irregV "begründen" "begründet" "begründete" "begründe" "begründet") ;
        behoerdlich_A = mkA "behördlich" ;
        beifuegen_V2 = mkV2 (prefixV "bei" (irregV "fügen" "fügt" "fügte" "füge" "gefügt")) ;
        beifuegen_V3 = mkV3 (prefixV "bei" (irregV "fügen" "fügt" "fügte" "füge" "gefügt")) (R.noPreposition R.Acc) (R.noPreposition R.Dat) ;
        beiliegen_V = prefixV "bei" (irregV "liegen" "liegt" "lag" "läge" "gelegen") ;
        bei_Prep = mkPrep "bei" dative ;
        bei_vorhandensein_von_Prep = mkPrep "bei Vorhandensein von" dative | mkPrep "bei Vorhandensein" genitive ;
        belehren_V = irregV "belehren" "belehrt" "belehrte" "belehre" "belehrt" ;
        belehren_V2 = mkV2 belehren_V ;
        berufszweig_N = mkN "Berufszweig" "Berufszweige" masculine ;
        beschliessen_V2 = mkV2 (irregV "beschließen" "beschließt" "beschloss" "beschließe" "beschlossen") ;
        beschliessen_VS = mkVS (irregV "beschließen" "beschließt" "beschloss" "beschließe" "beschlossen") ;
        besonders_A = mkA "besonders" "besonder" "besonderer" "besonderste" ;
        bestellen_V2 = mkV2 (bestellen_5_V) ;
        bestellt_A = mkA "bestellt" ;
        beteiligter_N = R.mkN "Beteiligter" "Beteiligten" "Beteiligten" "Beteiligten" "Beteiligten" "Beteiligten" "Beteiligten" masculine ;
        betragen_V2 = mkV2 (irregV "betragen" "beträgt" "betrug" "betrage" "betragen") ;
        bevollmaechtigen_V2V = mkV2V (irregV "bevollmächtigen" "bevollmächtigt" "bevollmächtigte" "bevollmächtige" "bevollmächtigt") ;
        bevollmaechtigen_V3 = mkV3 (irregV "bevollmächtigen" "bevollmächtigt" "bevollmächtigte" "bevollmächtige" "bevollmächtigt") (R.noPreposition R.Acc) LangGerWrapper.zu_Prep ;
        bevollmaechtigter_N = R.mkN "Bevollmächtigte" "Bevollmächtigten" "Bevollmächtigten" "Bevollmächtigten" "Bevollmächtigten" "Bevollmächtigten" "Bevollmächtigten" masculine ;
        beweiszweck_N = mkN "Beweiszweck" "Beweiszwecke" masculine ;
        bezueglich_Prep = mkPrep "bezüglich" genitive ;
        bgb_CLaw = R.mkN "BGB" "BGB" "BGB" "BGB" "BGB" "BGB" "BGB" neuter ;
        bitten_V = irregV "bitten" "bittet" "bat" "bitte" "gebeten" ;
        bitten_V2 = mkV2 bitten_V um_Prep ;
        brackets_Prep = mkPrep "(" dative ")" ;
        bzrg_CLaw = mkN "BZRG" "BZRG" neuter ;
        bzw_Conj = {s1 = [] ; s2 = "bzw." ; n = Pl} ;
        comma_Conj = {s1 = [] ; s2 = Coord.comma ; n = Pl} ;
        company_N = mkN  "Firma" "Firmen" feminine | mkN  "Unternehmen" "Unternehmen" neuter | gmbh_N | mkN  "Gesellschaft" "Gesellschaften" feminine ;
        dergestalt_Adv = ss "dergestalt" ;
        denken_VS = mkVS (irregV "denken" "denkt" "dachte" "dächte" "gedacht") ;
        deren_Pron = {
            s = \\_ => "deren" ;
            a = R.agrP3 Pl
        } ;
        dessen_Pron = {
            s = \\_ => "dessen" ;
            a = R.agrP3 Sg
        } ;
        dezember_CMonth = ss "Dezember" ;
        dienstansaessig_A = mkA "dienstansässig" ;
        dies_Pron = R.mkPronPers "dies" "dies" "dies" "dies" "dies" R.Neutr Sg P3 ;
        dieser_Pron = R.mkPronPers "dieser" "dieser" "dieser" "dieser" "dieser" R.Masc Sg P3 ;
        diese_Pron = R.mkPronPers "diese" "diese" "diese" "diese" "diese" R.Fem Sg P3 ;
        diese_Pl_Pron = R.mkPronPers "diese" "diese" "diese" "diese" "diese" R.Neutr Pl P3 | R.mkPronPers "diese" "diese" "diese" "diese" "diese" R.Masc Pl P3 | R.mkPronPers "diese" "diese" "diese" "diese" "diese" R.Fem Pl P3 ;
        dort_Adv = ss "dort" ;
        dritter_N = R.mkN "Dritter" "Dritten" "Dritten" "Dritten" "Dritten" "Dritten" "Dritten" masculine ;
        eilbeduerftigkeit_N = mkN "Eilbedürftigkeit" "Eilbedürftigkeiten" feminine ;
        einberufen_A = mkA "einberufen" ;
        einberufen_V2 = mkV2 (prefixV "ein" (irregV "berufen" "beruft" "berief" "beriefe" "berufen")) ;
        eingeschraenkt_Adv = ss "eingeschränkt" ;
        eingetragen_A = mkA "eingetragen" ;
        eingezahlt_A = mkA "eingezahlt" ;
        einholen_V2 = mkV2 (prefixV "ein" (irregV "holen" "holt" "holte" "hole" "geholt")) ;
        einlage_N = mkN "Einlage" "Einlagen" feminine ;
        einreichen_V2 = mkV2 (prefixV "ein" (irregV "reichen" "reicht" "reichte" "reiche" "gereicht")) ;
        eintragung_N = mkN "Eintragung" "Eintragungen" feminine ;
        eintragungsmitteilung_N = mkN "Eintragungsmitteilung" "Eintragungsmitteilungen" feminine ;
        eintragungsnachricht_N = mkN "Eintragungsnachricht" "Eintragungsnachrichten" feminine ;
        eintragungsverfahren_N = mkN "Eintragungsverfahren" "Eintragungsverfahren" neuter ;
        einverstanden_A2 = mkA2 einverstanden_A mit_Prep ; 
        einwilligungsvorbehalt_N = mkN "Einwilligungsvorbehalt" "Einwilligungsvorbehalte" masculine ;
        einzelvertretungsberechtigung_N = mkN "Einzelvertretungsberechtigung" "Einzelvertretungsberechtigungen" feminine ;
        entgegennehmen_V2 = mkV2 (prefixV "entgegen" (irregV "nehmen" "nimmt" "nahm" "nähme" "genommen")) ;
        entgegenstehen_V = CRG.prefixVPastPartSpace "entgegen" (irregV "stehen" "steht" "stand" "stehe" "gestanden") ;
        entgegenstehen_V2 = mkV2 entgegenstehen_V dative ;
        entwerfen_V2 = mkV2 entwerfen_V ;
        erbeten_V2 = mkV2 (irregV "erbeten" "erbittet" "erbat" "erbäte" "erbeten") ;
        ergaenzen_V2 = mkV2 ergaenzen_V ;
        ergaenzen_V3 = mkV3 ergaenzen_V (R.noPreposition R.Acc) um_Prep ;
        erklaeren_VS = mkVS erklaeren_V ;
        erlassen_V3 = mkV3 erlassen_V (R.noPreposition R.Acc) gegen_Prep ;
        erlauben_VV = mkVV erlauben_V ;
        erlauben_V2V = mkV2V erlauben_V (R.noPreposition R.Dat) ;
        erloeschen_V = (M.mkV "erlöschen" "erlischt" "erlischst" "erlischt" "erlischt" "erlisch" 
                          "erlosch" "erloschst" "erloschen" "erloscht"
                          "erlösche" "erloschen" [] R.VSein) ;
        ermaechtigen_V = irregV "ermächtigen" "ermächtigt" "ermächtigte" "ermächtige" "ermächtigt" ;
        ermaechtigen_V2 = mkV2 ermaechtigen_V ;
        ermaechtigen_V2V = mkV2V ermaechtigen_V ;
        ersuchen_V2 = mkV2 ersuchen_V um_Prep ;
        erteilen_V2 = mkV2 erteilen_V ;
        erteilung_N = mkN "Erteilung" "Erteilungen" feminine ;
        eu_N = mkN "EU" "EU" feminine | R.mkN "Europäische Union" "Europäische Union" "Europäischen Union" "Europäischen Union" "Europäische Unionen" "Europäischen Unionen" "Europäischen Unionen" feminine ;
        februar_CMonth = ss "Februar" ;
        folgen_V2 = mkV2 folgen_V dative ;
        folgendes_N = R.mkN "folgendes" "folgendes" "folgenden" "folgenden" "folgende" "folgenden" "folgenden" neuter | R.mkN "Folgendes" "Folgendes" "Folgenden" "Folgenden" "Folgende" "Folgenden" "Folgenden" neuter ;
        fristberechnung_N = mkN "Fristberechnung" "Fristberechnungen" feminine ;
        fuer_Prep = mkPrep "für" accusative ;
        ganz_Adv = ss "ganz" ;
        gebaeren_V2 = mkV2 (gebaeren_V) | mkV2 (R.mkV 
                "geb." "geb." "geb." "geb." "geb." "geb." 
                "geb." "geb." "geb." "geb."
                "geb." "geb." [] 
                R.VHaben) ;
        geben_V = irregV "geben" "gibt" "gab" "gäbe" "gegeben" ;
        geben_V3 = mkV3 geben_V ;
        geboren_A = mkA "geboren" | mkA "geb." ;
        gegen_Prep = mkPrep "gegen" accusative ;
        gegenueber_Prep = mkPrep "gegenüber" dative | mkPrep dative "gegenüber" ;
        gegenwaertig_Adv = ss "gegenwärtig" ;
        gemaess_Prep = mkPrep "gemäß" dative ;
        gemeinsam_Adv = ss "gemeinsam" ;
        gesamtprokura_N = R.mkN "Gesamtprokura" "Gesamtprokura" "Gesamtprokura" "Gesamtprokura" "Gesamtprokuren" "Gesamtprokuren" "Gesamtprokuren" feminine ;
        geschaeftsanschrift_N = mkN "Geschäftsanschrift" "Geschäftsanschriften" feminine ;
        geschaeftsanteil_N = mkN "Geschäftsanteil" "Geschäftsanteile" masculine ;
        geschaeftsfuehrerbestellung_N = mkN "Geschäftsführerbestellung" "Geschäftsführerbestellungen" feminine ;
        geschaeftsfuehrerin_N = mkN "Geschäftsführerin" "Geschäftsführerinnen" feminine ;
        geschaeftsfuehrung_N = mkN "Geschäfts" fuehrung_N ;
        geschaeftsraum_N = mkN "Geschäftsraum" "Geschäftsräume" masculine ;
        gesellschafterbeschluss_N = mkN gesellschafter_N beschluss_N ;
        gesellschafterbeschluss_N = mkN gesellschafter_N beschluss_N ;
        gesellschafterversammlung_N = mkN gesellschafter_N versammlung_N ;
        gesellschafterversammlungsniederschrift_N = mkN "Gesellschafterversammlungsniederschrift" "Gesellschafterversammlungsniederschriften" feminine ;
        gesellschaftervertrag_N = mkN "Gesellschaftsvertrag" "Gesellschaftsverträge" masculine | mkN "Gesellschaftervertrag" "Gesellschafterverträge" masculine ;
        getrennt_Adv = ss "getrennt" ;
        gewerbezweig_N = mkN "Gewerbezweig" "Gewerbezweige" masculine ;
        ggf_Adv = ss "ggf." ;
        gmbh_N = mkN "GmbH" "GmbHs" feminine ;
        gmbh_gesetz_CLaw = (mkN "GmbH-Gesetz" "GmbH-Gesetze" neuter) | (mkN "GmbH Gesetz" "GmbH Gesetze" neuter) | (R.mkN "GmbHG" "GmbHG" "GmbHG" "GmbHG" "GmbHG" "GmbHG" "GmbHG" neuter);
        grant_V2 = mkV2 (irregV "gewähren" "gewährt" "gewährte" "gewähre" "gewährt") | mkV2 (irregV "erlauben" "erlaubt" "erlaubte" "erlaube" "erlaubt") | mkV2 (erteilen_V) | mkV2 (gestatten_V) ;
        grant_V3 = mkV3 erteilen_V | mkV3 (irregV "gewähren" "gewährt" "gewährte" "gewähre" "gewährt") ;
        handelsregister_abteilung_A = R.mkN "Handelsregister Abteilung A" "Handelsregister Abteilung A" "Handelsregister Abteilung A" "Handelsregisters Abteilung A" "Handelsregister Abteilung A" "Handelsregistern Abteilung A" "Handelsregister Abteilung A" neuter ;
        handelsregister_abteilung_B = R.mkN "Handelsregister Abteilung B" "Handelsregister Abteilung B" "Handelsregister Abteilung B" "Handelsregisters Abteilung B" "Handelsregister Abteilung B" "Handelsregistern Abteilung B" "Handelsregister Abteilung B" neuter ;
        handelsregisteranmeldung_N = mkN "Handelsregisteranmeldung" "Handelsregisteranmeldungen" feminine ;
        handelsregisterauszug_N = mkN "Handelsregisterauszug" "Handelsregisterauszüge" masculine ;
        handelsregisterverfahren_N = mkN "Handelsregisterverfahren" "Handelsregisterverfahren" neuter ;
        herr_N = mkN "Herr" "Herren" masculine | R.mkN "Herr" "Herrn" "Herrn" "Herrn" "Herren" "Herren" "Herren" masculine ;
        heissen_V2 = mkV2 (irregV "heißen" "heißt" "hieß" "hieße" "geheißen") | mkV2 (irregV "lauten" "lautet" "lautete" "laute" "gelautet") nominative ;
        hinweisen_V2 = mkV2 hinweisen_V auf_acc_Prep ;
        im_uebrigen_Adv = ss "im übrigen" | ss "im Übrigen" ;
        im_sinne_von_Prep = mkPrep "im Sinne" genitive | mkPrep "in dem Sinne" genitive ;
        im_sinne_von_dat_Prep = mkPrep "im Sinne von" dative | mkPrep "in dem Sinne von" dative ;
        im_sinne_von_dat_abbr_Prep = mkPrep "i.S.v." dative ;
        im_sinne_von_gen_abbr_Prep = mkPrep "i.S.d." genitive ;
        in_Prep = mkPrep [] (R.NPP R.CInDat) | mkPrep "in" dative ;
        in_acc_Prep = mkPrep [] (R.NPP R.CInAcc) | mkPrep "in" accusative ;
        in_gleicher_weise_Adv = ss "in gleicher Weise" ;
        inlaendisch_A = mkA "inländisch" ;
        inland_N = mkN "Inland" "Inländer" neuter ;
        insolvenzverfahren_N = mkN insolvenz_N verfahren_N ;
        insolvenzverschleppung_N = mkN "Insolvenzverschleppung" "Insolvenzverschleppungen" feminine ;
        insoweit_Adv = ss "insoweit" ;
        its_allowed_to_A = mkA "erlaubt" | mkA "gestattet" ;
        januar_CMonth = ss "Januar" ;
        juli_CMonth = ss "Juli" ;
        juni_CMonth = ss "Juni" ;
        kapital_N = mkN "Kapital" "Kapitale" neuter ;
        kaufen_V2 = mkV2 (irregV "kaufen" "kauft" "kaufte" "kaufe" "gekauft") | mkV2 (irregV "erwerben" "erwirbt" "erwarb" "erwerbe" "erworben") ;
        kosten_N = mkN "Kosten" "Kosten" feminine ;
        kuerzer_A = mkA "kürzer" ;
        ladungsfrist_N = mkN "Ladungsfrist" "Ladungsfristen" feminine ;
        letzteres_N = mkN "letzteres" "letztere" neuter | mkN "Letzteres" "Letztere" neuter ;
        liquidator_N = mkN "Liquidator" "Liquidatoren" masculine ;
        maerz_CMonth = ss "März" ;
        mai_CMonth = ss "Mai" ;
        mann_N = mkN "Mann" "Männer" masculine ;
        may_VV = auxVV (R.mkV 
                "dürfen" "darf" "darfst" "darf" "dürft" "dürf" 
                "durfte" "durftest" "durften" "durftet"
                "dürfte" "gedurft" [] 
                R.VHaben) ;
        mehrere_Card = {
            s = \\g,c => case g of {
                    _ => case c of {
                        R.Nom => "mehrere" ;
                        R.Gen => "mehreren" ;
                        R.Dat => "mehreren" ;
                        R.Acc => "mehrere"
                    } 
                } ;
            n = Pl
        } ;
        mindestens_AdN = ss "mindestens" ;
        mit_Prep = mkPrep "mit" dative ;
        mitgliedsstaat_N = R.mkN "Mitgliedsstaat" "Mitgliedsstaat" "Mitgliedsstaat" "Mitgliedsstaates" "Mitgliedsstaaten" "Mitgliedsstaaten" "Mitgliedsstaaten" masculine ;
        mitrechnen_V2 = mkV2 (prefixV "mit" (irregV "rechnen" "rechnet" "rechnete" "rechne" "gerechnet")) | mkV2 (prefixV "mit" (irregV "berechnen" "berechnet" "berechnete" "berechne" "berechnet")) ;
        mitteilen_V2 = mkV2 (prefixV "mit" (irregV "teilen" "teilt" "teilte" "teile" "geteilt")) ;
        mitteilen_V3 = mkV3 (prefixV "mit" (irregV "teilen" "teilt" "teilte" "teile" "geteilt")) ;
        mitteilen_VS = mkVS (prefixV "mit" (irregV "teilen" "teilt" "teilte" "teile" "geteilt")) ;
        musterstadt_PN = mkPN "Musterstadt" feminine ;
        nach_Prep = mkPrep "nach" dative ;
        neben_Prep = mkPrep "neben" dative ;
        neither_nor_Conj = { s1 = "weder" ; s2 = "noch" ; n = Sg | Pl } ;
        neubestellt_A = mkA "neubestellt" ;
        neufassen_V2 = mkV2 (CRG.prefixVPastPartSpace "neu" (irregV "fassen" "fasst" "fasste" "fasse" "gefasst")) ;
        niederlegen_V2 = mkV2 (prefixV "nieder" (irregV "legen" "legt" "lag" "läge" "gelegt")) ;
        niederlegungsschreiben_N = R.mkN "Niederlegungsschreiben" "Niederlegungsschreiben" "Niederlegungsschreiben" "Niederlegungsschreibens" "Niederlegungsschreiben" "Niederlegungsschreiben" "Niederlegungsschreiben" neuter ;
        niederschrift_N = mkN "Niederschrift" "Niederschriften" feminine ;
        notarbescheinigung_N = mkN "Notarbescheinigung" "Notarbescheinigungen" feminine ;
        notariatsangestellter_N = R.mkN "Notariatsangestellter" "Notariatsangestellten" "Notariatsangestellten" "Notariatsangestellten" "Notariatsangestellten" "Notariatsangestellten" "Notariatsangestellten" masculine ;
        notariatsverwalter_N = mkN "Notariatsverwalter" "Notariatsverwalter" masculine ;
        november_CMonth = ss "November" ;
        nuernberg_PN = mkPN "Nürnberg" neuter ;
        nur_Adv = ss "nur" ;
        oben_genannt_A = mkA "oben genannt" | mkA "vorgenannt" | mkA "vorbezeichnet" | mkA "oben bezeichnet" ;
        obliged_to_A = mkA "verpflichtet" ;
        ohne_Prep = mkPrep "ohne" accusative ;
        ohne_ruecksicht_auf_Prep = mkPrep "ohne Rücksicht auf" accusative ;
        oktober_CMonth = ss "Oktober" ;
        per_Prep = mkPrep "per" dative ;
        postaufgabe_N = mkN "Postaufgabe" "Postaufgaben" feminine ;
        prokura_N = R.mkN "Prokura" "Prokura" "Prokura" "Prokura" "Prokuren" "Prokuren" "Prokuren" feminine ;
        pruefen_V = irregV "prüfen" "prüft" "prüfte" "prüfe" "geprüft" ;
        pruefen_V2 = mkV2 pruefen_V ;
        pruefen_VS = mkVS pruefen_V ;
        rechtsfolge_N = mkN "Rechtsfolge" "Rechtsfolgen" feminine ;
        rechtsgeschaeft_N = mkN "Rechtsgeschäft" "Rechtsgeschäfte" neuter ;
        rechtskrafteintritt_N = mkN "Rechtskrafteintritt" "Rechtskrafteintritte" masculine ;
        rechtskraeftig_A = mkA "rechtskräftig" ;
        registeranmeldung_N = mkN "Registeranmeldung" "Registeranmeldungen" feminine ;
        registerassistent_N = mkN "Registerassistent" "Registerassistenten" masculine ;
        registergericht_N = mkN "Registergericht" "Registergerichte" neuter ;
        registernachricht_N = mkN "Registernachricht" "Registernachrichten" feminine ;
        registersache_N = mkN "Registersache" "Registersachen" feminine ;
        saemtlich_Det = M.detLikeAdj False Pl "sämtlich" ;
        satzungsgemaess_A = mkA "satzungsgemäß" ;
        satzungssitz_N = mkN "Satzungs" sitz_N ;
        september_CMonth = ss "September" ;
        sich_selbst_Masc_Pron = R.mkPronPers "er selbst" "sich selbst" "sich selbst" "seiner" "sein"  R.Masc Sg P3 ;
        sich_selbst_Fem_Pron = R.mkPronPers "sie selbst" "sich selbst" "sich selbst" "ihr" "ihr"  R.Fem Sg P3 ;
        sich_selbst_Neut_Pron = R.mkPronPers "es selbst" "sich selbst" "sich selbst" "seiner" "sein"  R.Neutr Sg P3 ;
        slash_Conj = { s1 = [] ; s2 = "/" ; n = Pl } ;
        somit_Subj = ss "somit" ;
        soweit_Subj = ss "soweit" ;
        sowie_Conj = { s1 = [] ; s2 = "sowie" ; n = Pl } ;
        sozius_N = mkN "Sozius" "Sozii" masculine ;
        stammkapital_N = mkN stamm_N kapital_N ;
        stellen_V2 = mkV2 (irregV "stellen" "stellt" "stellte" "stelle" "gestellt") ;
        sterbeurkunde_N = mkN "Sterbeurkunde" "Sterbeurkunden" feminine ;
        tag_N = mkN "Tag" "Tage" masculine | R.mkN "Tag" "Tag" "Tage" "Tages" "Tage" "Tage" "Tage" masculine ;
        tragen_V2 = mkV2 (irregV "tragen" "trägt" "trug" "träge" "getragen") ;
        ueber_acc_Prep = mkPrep "über" accusative ;
        uebergabeeinschreiben_N = mkN "Übergabeeinschreiben" "Übergabeeinschreiben" neuter ;
        ueberreichen_V2 = mkV2 ueberreichen_V ;
        uebersendung_N = mkN "Übersendung" "Übersendungen" feminine ;
        um_Prep = mkPrep "um" accusative ;
        unberuehrt_A = mkA "unberührt" ;
        unbeschraenkt_A = mkA "unbeschränkt" ;
        uneingeschraenkt_A = mkA "uneingeschränkt" ;
        uneingeschraenkt_Adv = ss "uneingeschränkt" ;
        unterlassen_N = mkN "Unterlassen" "Unterlassen" neuter ;
        unterliegen_V2 = mkV2 (irregV "unterliegen" "unterliegt" "unterlag" "unterläge" "unterlegen") dative ;
        unternehmensgegenstand_N = mkN "Unternehmensgegenstand" "Unternehmensgegenstände" masculine ;
        untersagen_V2 = mkV2 untersagen_V ;
        untersagen_V3 = mkV3 untersagen_V ;
        untersagt_A = mkA "untersagt" ;
        untersagt_A2 = mkA2 untersagt_A datPrep ;
        unter_verzicht_auf_Prep = mkPrep "unter Verzicht auf" accusative ;
        unterzeichnender_N = R.mkN "Unterzeichnende" "Unterzeichnenden" "Unterzeichnenden" "Unterzeichnenden" "Unterzeichnenden" "Unterzeichnenden" "Unterzeichnenden" masculine ;
        unverschluesselt_A = mkA "unverschlüsselt" ;
        verkuerzen_V2 = mkV2 verkuerzen_V ;
        verlegen_V2 = mkV2 verlegen_V ;
        vermoegensangelegenheit_N = mkN "Vermögensangelegenheit" "Vermögensangelegenheiten" feminine ;
        versichern_V2 = mkV2 versichern_V ;
        versichern_VS = mkVS versichern_V ;
        vertragsstaat_N = R.mkN "Vertragsstaat" "Vertragsstaat" "Vertragsstaat" "Vertragsstaates" "Vertragsstaaten" "Vertragsstaaten" "Vertragsstaaten" masculine ;
        vertreten_A = mkA "vertreten" ;
        vertreten_V2 = mkV2 vertreten_V ;
        vertretungsbefugt_A = mkA "vertretungsbefugt" ;
        vertretungsberechtigt_A = mkA "vertretungsberechtigt" ;
        vertretungsberechtigung_N = mkN "Vertretungsberechtigung" "Vertretungsberechtigungen" feminine ;
        vertretungsregelung_N = mkN "Vertretungsregelung" "Vertretungsregelungen" feminine ;
        verurteilen_V2 = mkV2 (irregV "verurteilen" "verurteilt" "verurteilte" "verurteile" "verurteilt") ;
        verurteilung_N = mkN "Verurteilung" "Verurteilungen" feminine ;
        verwahren_V2 = mkV2 (irregV "verwahren" "verwahrt" "verwahrte" "verwahre" "verwahrt") ;
        verwaltungsbehoerde_N = mkN "Verwaltungs" behoerde_N ;
        verwaltungssitz_N = mkN "Verwaltungs" sitz_N ;
        verzichten_V2 = mkV2 verzichten_V auf_acc_Prep ;
        vollziehbar_A = mkA "vollziehbar" ;
        vollzug_N = mkN "Vollzug" "Vollzüge" masculine ;
        vollzugsmitteilung_N = mkN "Vollzugsmitteilung" "Vollzugsmitteilungen" feminine ;
        von_Prep = mkPrep [] (R.NPP R.CVonDat) | mkPrep "von" dative ;
        vorgeschrieben_A = mkA "vorgeschrieben" ;
        vorhandensein_N = mkN "Vorhandensein" "Vorhandensein" neuter ;
        vorlegen_V2 = mkV2 (prefixV "vor" (irregV "legen" "legt" "lag" "läge" "gelegt")) ;
        vornehmen_V2 = mkV2 (prefixV "vor" (irregV "nehmen" "nimmt" "nahm" "nähme" "genommen")) ;
        vorschrift_N = mkN "Vorschrift" "Vorschriften" feminine ;
        wiederholen_V2 = mkV2 wiederholen_V ;
        wegen_Prep = mkPrep "wegen" genitive | mkPrep "wegen" dative | mkPrep "aufgrund" genitive | mkPrep "auf Grund" genitive ;
        weiter_Adv = ss "weiter" ;
        widerruflich_A = mkA "widerruflich" ;
        wirksamkeit_N = mkN "Wirksamkeit" "Wirksamkeiten" feminine ;
        wissen_VS = mkVS (R.mkV 
                "wissen" "weiß" "weißt" "weiß" "wisst" "wisse" 
                "wusste" "wusstest" "wussten" "wusstet"
                "wisse" "gewusst" [] 
                R.VHaben) ;
        with_effect_to_Prep = mkPrep "mit Wirkung" (R.NPP R.CZuDat) | mkPrep "mit Effekt zu" dative ;
        with_regards_to_Prep = mkPrep "im Rahmen" genitive ;
        with_regards_to_2_Prep = mkPrep "in dem Rahmen" genitive ;
        with_regards_to_acc_Prep = mkPrep "mit Hinblick auf" accusative ;
        wohnhaft_A = mkA "wohnhaft" ;
        wohnhaft_A2 = mkA2 wohnhaft_A LangGerWrapper.in_Prep ;
        wonach_Subj = ss "wonach" ;
        zu_Prep = mkPrep [] (R.NPP R.CZuDat) | mkPrep "zu" dative ;
        zuruecknehmen_V2 = mkV2 (prefixV "zurück" (irregV "nehmen" "nimmt" "nahm" "nähme" "genommen")) ;
        zusammenlegen_V2 = mkV2 (prefixV "zusammen" (irregV "legen" "legt" "legte" "lege" "gelegt")) ;
        zusendung_N = mkN "Zusendung" "Zusendungen" feminine ;
        zwingend_A = mkA "zwingend" ;
}
