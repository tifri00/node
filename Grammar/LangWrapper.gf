{- --# -path=.:present -}

abstract LangWrapper = LangWithDict - [hier_Adv, dort_Adv, abhalten_V, PositAdvAdj, PrepNP, in_A, befugt_A, berechtigt_A, firma_N, unternehmen_N, gesellschaft_N, InflectionPrep, InLanguage, how_far_QCl, monthAdv, monthYearAdv, weekdayHabitualAdv, weekdayLastAdv, weekdayNextAdv, weekdayPunctualAdv, yearAdv, in_Prep, werden_V, ConjAdv, alle_Adv, have_V2, mindestens_Adv, ein_Adv, so_Adv, vorschrift_N, herr_N, efficacy_N, weder_Adv, saemtlich_A, an_Adv, tag_N, da_Adv, erloeschen_V, SubjS] ** open (R = ResGer), (PX = ParamX) in {

    flags startcat = CStatement ;

    cat
        CStatement ;
        CFact ;
        CAction ;
        CInstance ;
        CConcept ;
        CLawSection ;
        CNamedEntity ;
        CDate ;
        CLaw ; -- names/abbreviations of law texts (BGB, StGB, ...)
        CMonth ; -- month names like "Mai", "Juni", "Juli", ...
        CPercentageCard ; -- like a Card, but only for percentages (or things like "at least 75 %")
        CAdvP ;
        CSC ;

    fun
        -- Building statements
        
        StatementPos : Tense -> Ant -> CFact -> CStatement ;
        StatementNeg : Tense -> Ant -> CFact -> CStatement ;
        StatementAdv : CAdvP -> CStatement -> CStatement ;
        Condition : CStatement -> CStatement -> CStatement ;
        Condition2 : CStatement -> CStatement -> CStatement ;
        Condition3 : CStatement -> CStatement -> CStatement ;
        Condition4 : CStatement -> CStatement -> CStatement ;
        Condition5 : CStatement -> CStatement -> CStatement ;
        Condition6 : CStatement -> CStatement -> CStatement ;
        Condition7 : CStatement -> CStatement -> CStatement ;
        Condition8 : CStatement -> CStatement -> CStatement ;
        Condition9 : CStatement -> CStatement -> CStatement ;
        Reason : CStatement -> CStatement -> CStatement ;
        Reason2 : CStatement -> CStatement -> CStatement ;
        Reason3 : CStatement -> CStatement -> CStatement ;
        Contradiction : CStatement -> CStatement -> CStatement ;
        AndS : CStatement -> CStatement -> CStatement ;
        AndS2 : CStatement -> CStatement -> CStatement ;
        OrS : CStatement -> CStatement -> CStatement ;
        
        
        -- Building facts
        
        Fact : CInstance -> CAction -> CFact ;
        FactAllowedTo : CAction -> CFact ;
        FactAllowedTo2 : CAction -> CFact ;
        FactAllowedTo3 : CAction -> CFact ;
        FactAllowedTo4 : CAction -> CFact ;
        FactImpers : CAction -> CFact ;
        FactObjFocV2 : CInstance -> V2 -> CInstance -> CFact ;
        -- FactObjFocV3 : CInstance -> V3 -> CInstance -> CInstance -> CFact ;
        NoSubjFact : CAction -> CFact ;
        NoSubjFact2 : CAction -> CFact ;
        NoSubjFact3 : CAction -> CFact ;
        NoSubjFact4 : CAction -> CFact ;
        NoSubjFact5 : CAction -> CFact ;
        NoSubjFact6 : CAction -> CFact ;
        NoSubjFact7 : CAction -> CFact ;
        NoSubjFact8 : CAction -> CFact ;
        NoSubjFact9 : CAction -> CFact ;
        NoSubjFact10 : CAction -> CFact ;
        NoSubjFact11 : CAction -> CFact ;
        NoSubjFact12 : CAction -> CFact ;
        NoSubjFact13 : CAction -> CFact ;
        NoSubjFact14 : CAction -> CFact ;
        NoSubjFact15 : CAction -> CFact ;
        NoSubjFact16 : CAction -> CFact ;
        NoSubjFact17 : CAction -> CFact ;
        NoSubjFact18 : CAction -> CFact ;
        FactImpersV2Colon : CAction -> CInstance -> CFact ;
        FactImpersV2Colon2 : CAction -> CInstance -> CFact ;
        FactImpersV2Colon3 : CAction -> CInstance -> CFact ;
        Exists : CInstance -> CFact ;
        
        Annotation : CFact -> CInstance -> CFact ;
        
        -- Building actions
        
        Action : V -> CAction ;
        ActionAnd : V -> V -> CAction ;
        Action2 : V2 -> CInstance -> CAction ;
        Action2And : V2 -> V2 -> CInstance -> CAction ;
        ActionReflexive : V2 -> CAction ;
        Action2Colon : V2 -> CInstance -> CAction ;
        Action2Colon2 : V2 -> CInstance -> CAction ;
        Action3 : V3 -> CInstance -> CInstance -> CAction ;
        ActionVV : VV -> CAction -> CAction ;
        ActionV2V : V2V -> CInstance -> CAction -> CAction ;
        ActionSC : VS -> CStatement -> CAction ;
        ActionSCColon : VS -> CStatement -> CAction ;
        ActionSCWhatFollows : VS -> CStatement -> CAction ;
        ActionSCWhatFollows2 : VS -> CStatement -> CAction ;
        ActionSCWhatFollows3 : VS -> CStatement -> CAction ;
        ActionPass : V2 -> CAction ;
        ActionPassActor : V2 -> CInstance -> CAction ;
        ActionPassActor2 : V2 -> CInstance -> CAction ;
        ActionPassV2V : V2V -> CSC -> CAction ;
        ActionPassSC : VS -> CStatement -> CAction ;
        ActionPassSCColon : VS -> CStatement -> CAction ;
        ActionPassSCWhatFollows : VS -> CStatement -> CAction ;
        ActionPassV3 : V3 -> CInstance -> CAction ;
        PropAction : A -> CAction ;
        PropAction3 : A -> CAction ;
        PropAction5 : A -> CAction ;
        PropAction2 : A2 -> CInstance -> CAction ;
        PropAction4 : A2 -> CInstance -> CAction ;
        PropAction6 : A2 -> CInstance -> CAction ;
        PropActionColon : A2 -> CInstance -> CAction ;
        PropActionAnd : A -> A -> CAction ;
        PropActionOr : A -> A -> CAction ;
        PropCompareAction : A -> CInstance -> CAction ;
        AdvAction : Adv -> CAction ;
        AdvActionAnd : Adv -> Adv -> CAction ;
        AdvActionOr : Adv -> Adv -> CAction ;
        Is : CInstance -> CAction ;
        Own : CInstance -> CAction ;
        LocationAction : CInstance -> CAction ;
        
        Mod : CAction -> CAdvP -> CAction ;
        Mod2 : CAction -> CAdvP -> CAction ;
        Mod3 : CAction -> CAdvP -> CAction ;
        Mod4 : CAction -> CAdvP -> CAction ;
        Mod5 : CAction -> CAdvP -> CAction ;
        ModA : CAction -> A -> CAction ;
        ModA2 : CAction -> A -> CAction ;
        ModA3 : CAction -> A -> CAction ;
        ModA4 : CAction -> A -> CAction ;
        ModA5 : CAction -> A -> CAction ;
        
        Possibility : CAction -> CAction ;
        Possibility2 : CAction -> CAction ;
        Obligation : CAction -> CAction ;
        ObligationSC : CSC -> CAction ;
        ObligationSC2 : CSC -> CAction ;
        ObligationSC3 : CSC -> CAction ;
        ObligationPass : V2 -> CAction ;
        ObligationPassSC : VS -> CStatement -> CAction ;
        Permission : CAction -> CAction ;
        Permission2 : CAction -> CAction ;
        PermissionSC : CSC -> CAction ;
        PermissionSC2 : CSC -> CAction ;
        Desire : CAction -> CAction ;
        
        
        -- Building instances
        
        Instance : CConcept -> CInstance ;
        Instance2 : CConcept -> CInstance ;
        Instance3 : CConcept -> CInstance ;
        Instance4 : CConcept -> CInstance ;
        Instance5 : CConcept -> CInstance ;
        Instance6 : CConcept -> CInstance ;
        Instance7 : CConcept -> CInstance ;
        Instance8 : CConcept -> CInstance ;
        Instance9 : CConcept -> CInstance ;
        Instance10 : CConcept -> CInstance ;
        Instance11 : CConcept -> CInstance ;
        Instance12 : CConcept -> CInstance ;
        Instance13 : CConcept -> CInstance ;
        Instance14 : CConcept -> CInstance ;
        Instance15 : CConcept -> CInstance ;
        Instance16 : CConcept -> CInstance ;
        Instance17 : CConcept -> CInstance ;
        Instance18 : CConcept -> CInstance ;
        InstanceOwnerPronSg : CConcept -> CInstance ;
        InstanceOwnerPronPl : CConcept -> CInstance ;
        InstanceEvery : CConcept -> CInstance ;
        InstanceEvery2 : CConcept -> CInstance ;
        InstanceEvery3 : CConcept -> CInstance ;
        InstanceFew : CConcept -> CInstance ;
        InstanceMany : CConcept -> CInstance ;
        InstanceMany2 : CConcept -> CInstance ;
        InstanceNone : CConcept -> CInstance ;
        InstancePN : PN -> CInstance ;
        InstancePron : Pron -> CInstance ;
        InstanceDigits : CConcept -> Int -> CInstance ;
        -- InstanceNumeral : CConcept -> Numeral -> CInstance ;
        InstanceCard : CConcept -> Card -> CInstance ;
        And : CInstance -> CInstance -> CInstance ;
        And2 : CInstance -> CInstance -> CInstance ;
        And3 : CInstance -> CInstance -> CInstance ;
        And4 : CInstance -> CInstance -> CInstance ;
        And5 : CInstance -> CInstance -> CInstance ;
        And6 : CInstance -> CInstance -> CInstance ;
        Or : CInstance -> CInstance -> CInstance ;
        CommaDescription : CInstance -> A -> CInstance ;
        CommaDescription2 : CInstance -> A -> CInstance ;
        CommaDescriptionHometownMasc : CInstance -> String -> CInstance ;
        CommaDescriptionHometownMasc2 : CInstance -> String -> CInstance ;
        CommaDescriptionHometownFem : CInstance -> String -> CInstance ;
        CommaDescriptionHometownFem2 : CInstance -> String -> CInstance ;
        CommaDescriptionHometownNeut : CInstance -> String -> CInstance ;
        CommaDescriptionHometownNeut2 : CInstance -> String -> CInstance ;
        CommaDescriptionHometown2 : CInstance -> CInstance -> CInstance ;
        CommaDescriptionHometown3 : CInstance -> CInstance -> CInstance ;
        CommaDescriptionHometown4 : CInstance -> CInstance -> CInstance ;
        CommaDescriptionBirthday : CInstance -> CDate -> CInstance ;
        CommaDescriptionBirthday2 : CInstance -> CDate -> CInstance ;
        CommaDescriptionBirthday3 : CInstance -> CDate -> CInstance ;
        CommaDescriptionBirthday4 : CInstance -> CDate -> CInstance ;
        
        Location : CInstance -> CInstance -> CInstance ;
        Location2 : CInstance -> CInstance -> CInstance ;
        Location3 : CInstance -> CInstance -> CInstance ;
        Location4 : CInstance -> CInstance -> CInstance ;
        Location5 : CInstance -> CInstance -> CInstance ;
        Location6 : CInstance -> CInstance -> CInstance ;
        Location7 : CInstance -> CInstance -> CInstance ;
        
        Target : CInstance -> CInstance -> CInstance ;
        Target2 : CInstance -> CInstance -> CInstance ;
        Target3 : CInstance -> CInstance -> CInstance ;
        
        Referring : CInstance -> CInstance -> CInstance ;
        Referring2 : CInstance -> CInstance -> CInstance ;
        Referring3 : CInstance -> CInstance -> CInstance ;
        Referring4 : CInstance -> CInstance -> CInstance ;
        Referring5 : CInstance -> CInstance -> CInstance ;
        Referring6 : CInstance -> CInstance -> CInstance ;
        
        Origin : CInstance -> CInstance -> CInstance ;
        
        AccordingTo : CInstance -> CInstance -> CInstance ;
        AccordingTo2 : CInstance -> CInstance -> CInstance ;
        AccordingTo3 : CInstance -> CInstance -> CInstance ;
        AccordingTo4 : CInstance -> CInstance -> CInstance ;
        AccordingTo5 : CInstance -> CInstance -> CInstance ;
        AccordingTo6 : CInstance -> CInstance -> CInstance ;
        
        AccordingToLawSection : CInstance -> CLawSection -> CInstance ;
        
        Besides : CInstance -> CInstance -> CInstance ;
        
        InstancePercentage : CInstance -> CPercentageCard -> CInstance ;
        
        
        -- Building concepts
        
        Concept : N -> CConcept ;
        
        ConceptLawSection : CLawSection -> CConcept ;
        ConceptNamedEntity : CNamedEntity -> CConcept ;
        ConceptDate : CDate -> CConcept ;
        
        ConceptRel : CConcept -> CAction -> CConcept ;
        
        ConceptAnd : CConcept -> CConcept -> CConcept ;
        ConceptAnd2 : CConcept -> CConcept -> CConcept ;
        ConceptOr : CConcept -> CConcept -> CConcept ;
        
        NEConceptWithSaluteMasc : N -> String -> CNamedEntity ;
        NEConceptWithSaluteFem : N -> String -> CNamedEntity ;
        NEConceptWithSaluteNeut : N -> String -> CNamedEntity ;
        
        NEConceptMasc : String -> CNamedEntity ;
        NEConceptFem : String -> CNamedEntity ;
        NEConceptNeut : String -> CNamedEntity ;
        
        Owner : CConcept -> CInstance -> CConcept ;
        Owner2 : CConcept -> CInstance -> CConcept ;
        Owner3 : CConcept -> CInstance -> CConcept ;
        
        Prop : CConcept -> A -> CConcept ;
        Prop2 : CConcept -> A -> CConcept ;
        
        ActionProp : CConcept -> CAction -> CConcept ;
        
        PassActionProp2 : CConcept -> V2 -> CConcept ;
        PassActionProp3 : CConcept -> V3 -> CInstance -> CConcept ;
        
        LawSection : Int -> CLaw -> CLawSection ;
        LawSectionWithParagraph : Int -> Int -> CLaw -> CLawSection ;
        LawSectionWithParagraphAndSentence : Int -> Int -> Int -> CLaw -> CLawSection ;
        LawSectionWithParagraphAndSentence2 : Int -> Int -> Int -> CLaw -> CLawSection ;
        LawSectionNumOnly : Int -> CLawSection ;
        LawSectionNumOnlyWithParagraph : Int -> Int -> CLawSection ;
        LawSectionNumOnlyWithParagraphAndSentence : Int -> Int -> Int -> CLawSection ;
        LawSectionNumOnlyWithParagraphAndSentence2 : Int -> Int -> Int -> CLawSection ;
        
        HRBNr : Int -> CConcept ;
        HRBNr2 : Int -> CConcept ;
        
        Date : Int -> Int -> Int -> CDate ;
        Date2 : Int -> CMonth -> Int -> CDate ;
        
        Price : Int -> CConcept ;
        Price2 : Int -> CConcept ;
        Price3 : Int -> CConcept ;
        Price4 : Int -> CConcept ;
        
        PriceComma : Int -> Int -> CConcept ;
        PriceComma2 : Int -> Int -> CConcept ;
        
        Address : Int -> CNamedEntity -> CNamedEntity -> Int -> CConcept ;
        Address2 : Int -> CNamedEntity -> CNamedEntity -> Int -> CConcept ;
        ZipAndCity : Int -> CNamedEntity -> CConcept ;
        StreetAndNr : CNamedEntity -> Int -> CConcept ;
        
        
        -- ========== Misc ========== -
        
        MakeCAdvP : Adv -> CAdvP ;
        
        AdvAnd : CAdvP -> CAdvP -> CAdvP ;
        AdvOr : CAdvP -> CAdvP -> CAdvP ;
        AdvNeitherNor : CAdvP -> CAdvP -> CAdvP ;
        
        Percentage : Int -> CPercentageCard ;
        AtLeastP : CPercentageCard -> CPercentageCard ;
        AtLeastP2 : CPercentageCard -> CPercentageCard ;
        AtMostP : CPercentageCard -> CPercentageCard ;
        AlmostP : CPercentageCard -> CPercentageCard ;
        
        IntCard : Int -> Card ;
        
        CardOr : Card -> Card -> Card ;
        
        AtLeast : Card -> Card ;
        AtLeast2 : Card -> Card ;
        AtMost : Card -> Card ;
        Almost : Card -> Card ;
        
        ZurAdv : CInstance -> CAdvP ;
        AsAdv : CInstance -> CAdvP ;
        WhenExistsAdv : CInstance -> CAdvP ;
        ReferringAdv : CInstance -> CAdvP ;
        ReferringAdv2 : CInstance -> CAdvP ;
        ReferringAdv3 : CInstance -> CAdvP ;
        ReferringAdv4 : CInstance -> CAdvP ;
        ReferringAdv5 : CInstance -> CAdvP ;
        ReferringAdv6 : CInstance -> CAdvP ;
        LocationAdv : CInstance -> CAdvP ;
        LocationAdv2 : CInstance -> CAdvP ;
        LocationAdv3 : CInstance -> CAdvP ;
        LocationAdv4 : CInstance -> CAdvP ;
        LocationAdv5 : CInstance -> CAdvP ;
        LocationAdv6 : CInstance -> CAdvP ;
        LocationAdv7 : CInstance -> CAdvP ;
        LocationAdv8 : CInstance -> CAdvP ;
        PartAdv : CInstance -> CAdvP ;
        TargetAdv : CInstance -> CAdvP ;
        TargetAdv2 : CInstance -> CAdvP ;
        TargetAdv3 : CInstance -> CAdvP ;
        DateAdv : Int -> Int -> Int -> CAdvP ;
        DateAdv2 : Int -> Int -> Int -> CAdvP ;
        DateAdv3 : Int -> CMonth -> Int -> CAdvP ;
        DateAdv4 : Int -> CMonth -> Int -> CAdvP ;
        DurchAdv : CInstance -> CAdvP ;
        MitAdv : CInstance -> CAdvP ;
        MitAdv2 : CInstance -> CAdvP ;
        UnterVerzichtAdv : CInstance -> CAdvP ;
        OhneAdv : CInstance -> CAdvP ;
        ForAdv : CInstance -> CAdvP ;
        DisregardingAdv : CInstance -> CAdvP ;
        OriginAdv : CInstance -> CAdvP ;
        AccordingToAdv : CInstance -> CAdvP ;
        AccordingToAdv2 : CInstance -> CAdvP ;
        AccordingToAdv3 : CInstance -> CAdvP ;
        AccordingToAdv4 : CInstance -> CAdvP ;
        AccordingToAdv5 : CInstance -> CAdvP ;
        AccordingToAdv6 : CInstance -> CAdvP ;
        AccordingToLawSectionAdv : CLawSection -> CAdvP ;
        DueToAdv : CInstance -> CAdvP ;
        BesidesAdv : CInstance -> CAdvP ;
        WithEffectToAdv : Int -> Int -> Int -> CAdvP ;
        
        AdverbialInsertion : CAdvP -> CAdvP ;
        AdverbialInsertion2 : CAdvP -> CAdvP ;
        AdverbialInsertion3 : CAdvP -> CAdvP ;
        AdverbialInsertion4 : CAdvP -> CAdvP ;
        AdverbialInsertion5 : CAdvP -> CAdvP ;
        AdverbialInsertion6 : CAdvP -> CAdvP ;
        
        AdverbialInsertionNeg : CAdvP -> CAdvP ;
        AdverbialInsertionNeg2 : CAdvP -> CAdvP ;
        AdverbialInsertionNeg3 : CAdvP -> CAdvP ;
        AdverbialInsertionNeg4 : CAdvP -> CAdvP ;
        
        ThatAdv : CStatement -> CAdvP ;
        
        ModLeftA : A -> CAdvP -> A ;
        ModLeftAA : A -> A -> A ;
        
        ModRightA : A -> CAdvP -> A ;
        ModRightAA : A -> A -> A ;
        
        AdjAnd : A -> A -> A ;
        AdjAnd2 : A -> A -> A ;
        AdjOr : A -> A -> A ;
        
        SCFromAct : CAction -> CSC ;
        
        SCAnd : CSC -> CSC -> CSC ;
        SCAnd2 : CSC -> CSC -> CSC ;
        SCAnd3 : CSC -> CSC -> CSC ;
        
        
        -- Additional vocabulary
        
        abberufen_A : A ;
        abgeben_V2 : V2 ;
        abhalten_V2 : V2 ;
        abkommen_ueber_eu_wirtschaftsraum_N : N ;
        absendung_N : N ;
        aendern_V2 : V2 ;
        all_Det : Det ;
        alleinvertretungsberechtigt_A : A ;
        allowed_to_A : A ;
        allowed_to_2_A : A ;
        als_Prep : Prep ;
        amtlich_Adv : Adv ;
        amtsnachfolger_N : N ;
        an_Prep : Prep ;
        an_acc_Prep : Prep ;
        anders_A : A ;
        angestellter_N : N ;
        anmelden_VS : VS ;
        anmelden_V2 : V2 ;
        anteilsteilung_N : N ;
        anzeigen_V2 : V2 ;
        anzeigen_V3 : V3 ;
        april_CMonth : CMonth ;
        auf_acc_Prep : Prep ;
        august_CMonth : CMonth ;
        aus_Prep : Prep ;
        auskunftspflicht_N : N ;
        ausland_N : N ;
        beachtung_N : N ;
        beantragen_V2 : V2 ;
        beauftragen_V2 : V2 ;
        beauftragen_V3 : V3 ;
        befinden_refl_V : V ;
        beglaubigen_V2 : V2 ;
        beglaubigend_A : A ;
        beglaubigt_A : A ;
        begruenden_V2 : V2 ;
        begruenden_VS : VS ;
        behoerdlich_A : A ;
        beifuegen_V2 : V2 ;
        beifuegen_V3 : V3 ;
        beiliegen_V : V ;
        bei_Prep : Prep ;
        bei_vorhandensein_von_Prep : Prep ;
        belehren_V : V ;
        belehren_V2 : V2 ;
        berufszweig_N : N ;
        beschliessen_V2 : V2 ;
        beschliessen_VS : VS ;
        besonders_A : A ;
        bestellen_V2 : V2 ;
        bestellt_A : A ;
        beteiligter_N : N ;
        betragen_V2 : V2 ;
        bevollmaechtigen_V2V : V2V ;
        bevollmaechtigen_V3 : V3 ;
        bevollmaechtigter_N : N ;
        beweiszweck_N : N ;
        bezueglich_Prep : Prep ;
        bgb_CLaw : CLaw ;
        bitten_V : V ;
        bitten_V2 : V2 ;
        brackets_Prep : Prep ;
        bzrg_CLaw : CLaw ;
        bzw_Conj : Conj ;
        comma_Conj : Conj ;
        company_N : N ;
        dergestalt_Adv : Adv ;
        denken_VS : VS ;
        deren_Pron : Pron ;
        dessen_Pron : Pron ;
        dezember_CMonth : CMonth ;
        dienstansaessig_A : A ;
        dies_Pron : Pron ;
        dieser_Pron : Pron ;
        diese_Pron : Pron ;
        diese_Pl_Pron : Pron ;
        dort_Adv : Adv ;
        dritter_N : N ;
        eilbeduerftigkeit_N : N ;
        einberufen_A : A ;
        einberufen_V2 : V2 ;
        eingeschraenkt_Adv : Adv ;
        eingetragen_A : A ;
        eingezahlt_A : A ;
        einholen_V2 : V2 ;
        einlage_N : N ;
        einreichen_V2 : V2 ;
        eintragung_N : N ;
        eintragungsmitteilung_N : N ;
        eintragungsnachricht_N : N ;
        eintragungsverfahren_N : N ;
        einverstanden_A2 : A2 ;
        einwilligungsvorbehalt_N : N ;
        einzelvertretungsberechtigung_N : N ;
        entgegennehmen_V2 : V2 ;
        entgegenstehen_V : V ;
        entgegenstehen_V2 : V2 ;
        entwerfen_V2 : V2 ;
        erbeten_V2 : V2 ;
        ergaenzen_V2 : V2 ;
        ergaenzen_V3 : V3 ;
        erklaeren_VS : VS ;
        erlassen_V3 : V3 ;
        erlauben_VV : VV ;
        erlauben_V2V : V2V ;
        erloeschen_V : V ;
        ermaechtigen_V : V ;
        ermaechtigen_V2 : V2 ;
        ermaechtigen_V2V : V2V ;
        ersuchen_V2 : V2 ;
        erteilen_V2 : V2 ;
        erteilung_N : N ;
        eu_N : N ;
        februar_CMonth : CMonth ;
        folgen_V2 : V2 ;
        folgendes_N : N ;
        fristberechnung_N : N ;
        fuer_Prep : Prep ;
        ganz_Adv : Adv ;
        gebaeren_V2 : V2 ;
        geben_V : V ;
        geben_V3 : V3 ;
        geboren_A : A ;
        gegen_Prep : Prep ;
        gegenueber_Prep : Prep ;
        gegenwaertig_Adv : Adv ;
        gemaess_Prep : Prep ;
        gemeinsam_Adv : Adv ;
        gesamtprokura_N : N ;
        geschaeftsanschrift_N : N ;
        geschaeftsanteil_N : N ;
        geschaeftsfuehrerbestellung_N : N ;
        geschaeftsfuehrerin_N : N ;
        geschaeftsfuehrung_N : N ;
        geschaeftsraum_N : N ;
        gesellschafterbeschluss_N : N ;
        gesellschafterversammlung_N : N ;
        gesellschafterversammlungsniederschrift_N : N ;
        gesellschaftervertrag_N : N ;
        getrennt_Adv : Adv ;
        gewerbezweig_N : N ;
        ggf_Adv : Adv ;
        gmbh_N : N ;
        gmbh_gesetz_CLaw : CLaw ;
        grant_V2 : V2 ;
        grant_V3 : V3 ;
        handelsregister_abteilung_A : N ;
        handelsregister_abteilung_B : N ;
        handelsregisteranmeldung_N : N ;
        handelsregisterauszug_N : N ;
        handelsregisterverfahren_N : N ;
        heissen_V2 : V2 ;
        herr_N : N ;
        hinweisen_V2 : V2 ;
        im_uebrigen_Adv : Adv ;
        im_sinne_von_Prep : Prep ;
        im_sinne_von_dat_Prep : Prep ;
        im_sinne_von_dat_abbr_Prep : Prep ;
        im_sinne_von_gen_abbr_Prep : Prep ;
        in_Prep : Prep ;
        in_acc_Prep : Prep ;
        in_gleicher_weise_Adv : Adv ;
        inlaendisch_A : A ;
        inland_N : N ;
        insolvenzverfahren_N : N ;
        insolvenzverschleppung_N : N ;
        insoweit_Adv : Adv ;
        its_allowed_to_A : A ;
        januar_CMonth : CMonth ;
        juli_CMonth : CMonth ;
        juni_CMonth : CMonth ;
        kapital_N : N ;
        kaufen_V2 : V2 ;
        kosten_N : N ;
        kuerzer_A : A ;
        ladungsfrist_N : N ;
        letzteres_N : N ;
        liquidator_N : N ;
        maerz_CMonth : CMonth ;
        mai_CMonth : CMonth ;
        mann_N : N ;
        may_VV : VV ;
        mehrere_Card : Card ;
        mindestens_AdN : AdN ;
        mit_Prep : Prep ;
        mitgliedsstaat_N : N ;
        mitrechnen_V2 : V2 ;
        mitteilen_V2 : V2 ;
        mitteilen_V3 : V3 ;
        mitteilen_VS : VS ;
        musterstadt_PN : PN ;
        nach_Prep : Prep ;
        neben_Prep : Prep ;
        neither_nor_Conj : Conj ;
        neubestellt_A : A ;
        neufassen_V2 : V2 ;
        niederlegen_V2 : V2 ;
        niederlegungsschreiben_N : N ;
        niederschrift_N : N ;
        notarbescheinigung_N : N ;
        notariatsangestellter_N : N ;
        notariatsverwalter_N : N ;
        november_CMonth : CMonth ;
        nuernberg_PN : PN ;
        nur_Adv : Adv ;
        oben_genannt_A : A ;
        obliged_to_A : A ;
        ohne_Prep : Prep ;
        ohne_ruecksicht_auf_Prep : Prep ;
        oktober_CMonth : CMonth ;
        per_Prep : Prep ;
        postaufgabe_N : N ;
        prokura_N : N ;
        pruefen_V : V ;
        pruefen_V2 : V2 ;
        pruefen_VS : VS ;
        rechtsfolge_N : N ;
        rechtsgeschaeft_N : N ;
        rechtskrafteintritt_N : N ;
        rechtskraeftig_A : A ;
        registeranmeldung_N : N ;
        registerassistent_N : N ;
        registergericht_N : N ;
        registernachricht_N : N ;
        registersache_N : N ;
        saemtlich_Det : Det ;
        satzungsgemaess_A : A ;
        satzungssitz_N : N ;
        september_CMonth : CMonth ;
        sich_selbst_Masc_Pron : Pron ;
        sich_selbst_Fem_Pron : Pron ;
        sich_selbst_Neut_Pron : Pron ;
        slash_Conj : Conj ;
        somit_Subj : Subj ;
        soweit_Subj : Subj ;
        sowie_Conj : Conj ;
        sozius_N : N ;
        stammkapital_N : N ;
        stellen_V2 : V2 ;
        sterbeurkunde_N : N ;
        tag_N : N ;
        tragen_V2 : V2 ;
        ueber_acc_Prep : Prep ;
        uebergabeeinschreiben_N : N ;
        ueberreichen_V2 : V2 ;
        uebersendung_N : N ;
        um_Prep : Prep ;
        unberuehrt_A : A ;
        unbeschraenkt_A : A ;
        uneingeschraenkt_A : A ;
        uneingeschraenkt_Adv : Adv ;
        unterlassen_N : N ;
        unterliegen_V2 : V2 ;
        unternehmensgegenstand_N : N ;
        untersagen_V2 : V2 ;
        untersagen_V3 : V3 ;
        untersagt_A : A ;
        untersagt_A2 : A2 ;
        unter_verzicht_auf_Prep : Prep ;
        unterzeichnender_N : N ;
        unverschluesselt_A : A ;
        verkuerzen_V2 : V2 ;
        verlegen_V2 : V2 ;
        vermoegensangelegenheit_N : N ;
        versichern_V2 : V2 ;
        versichern_VS : VS ;
        vertragsstaat_N : N ;
        vertreten_A : A ;
        vertreten_V2 : V2 ;
        vertretungsbefugt_A : A ;
        vertretungsberechtigt_A : A ;
        vertretungsberechtigung_N : N ;
        vertretungsregelung_N : N ;
        verurteilen_V2 : V2 ;
        verurteilung_N : N ;
        verwahren_V2 : V2 ;
        verwaltungsbehoerde_N : N ;
        verwaltungssitz_N : N ;
        verzichten_V2 : V2 ;
        vollziehbar_A : A ;
        vollzug_N : N ;
        vollzugsmitteilung_N : N ;
        von_Prep : Prep ;
        vorgeschrieben_A : A ;
        vorhandensein_N : N ;
        vorlegen_V2 : V2 ;
        vornehmen_V2 : V2 ;
        vorschrift_N : N ;
        wiederholen_V2 : V2 ;
        wissen_VS : VS ;
        with_effect_to_Prep : Prep ;
        with_regards_to_Prep : Prep ;
        with_regards_to_2_Prep : Prep ;
        with_regards_to_acc_Prep : Prep ;
        wegen_Prep : Prep ;
        weiter_Adv : Adv ;
        widerruflich_A : A ;
        wirksamkeit_N : N ;
        wohnhaft_A : A ;
        wohnhaft_A2 : A2 ;
        wonach_Subj : Subj ;
        zu_Prep : Prep ;
        zuruecknehmen_V2 : V2 ;
        zusammenlegen_V2 : V2 ;
        zusendung_N : N ;
        zwingend_A : A ;
}