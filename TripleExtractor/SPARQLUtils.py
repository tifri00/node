import json
import requests

from constants import NODE_PREFIX, GRAPH_IRI

ENDPOINT = "http://localhost:9999/blazegraph/namespace/NoDE/sparql"

prefixes = ["PREFIX : <" + NODE_PREFIX + ">"]


def execQuery(query):
    params = {
        "query": "\n".join(prefixes) + "\n" + query,
        "format": "json",
    }

    response = requests.get(ENDPOINT, params=params)

    return json.loads(response.text) if response.status_code == 200 else None


def execUpdate(query):
    data = {
        "update": "\n".join(prefixes) + "\n" + str(query),
    }

    requests.post(ENDPOINT, data=data)


def clearGraph():
    execUpdate("DELETE WHERE { ?s ?p ?o . }")


def insertTriples(triples):
    query = "INSERT DATA {\n"
    query += "GRAPH <" + GRAPH_IRI + "> {\n"

    for triple in triples:
        query += str(triple[0].replace("\"", "\\\"")) + " " + str(triple[1].replace("\"", "\\\"")) + " " + str(triple[2].replace("\"", "\\\"")) + ".\n"

    query += "}}"

    data = {
        "update": "\n".join(prefixes) + "\n" + str(query),
    }

    response = requests.post(ENDPOINT, data=data)

    return response.status_code == 200


def getVars(result):
    return result['head']['vars']


def getMatrix(queryResult):
    result = []

    if not queryResult:
        return result

    for binding in queryResult["results"]["bindings"]:
        line = []
        for var in queryResult["head"]["vars"]:
            value = binding[var]["value"] if var in binding else None

            line.append(value)

        result.append(line)

    return result
