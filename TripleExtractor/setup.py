#!/usr/bin/env python

from distutils.core import setup

setup(name='NoDE Triple Extractor',
      version='1.0',
      description='Triple extractor for NoDE',
      author='Tim Friedrich',
      author_email='tim.tf.friedrich@fau.de',
      packages=['Nodes', 'Queries'],
      py_modules=['extractor', 'constants', 'GFUtils', 'SPARQLUtils', 'Utility', 'resources', 'gf'],
     )