import ast
import inspect
import re
import sys

from GFUtils import parse, GFParseException, startGF, stopGF
from Nodes.BaseNodes import *
import Nodes.StatementNodes
import Nodes.FactNodes
import Nodes.InstanceNodes
import Nodes.ActionNodes
import Nodes.MiscNodes
import Nodes.ConceptNodes

import Nodes
from SPARQLUtils import insertTriples, clearGraph
from Queries.queries import QUERIES


class CombinationAnd:
    def __init__(self, args):
        self.args = args


class CombinationOr:
    def __init__(self, args):
        self.args = args


def init():
    statementNodes = list(map(lambda x: x[1], inspect.getmembers(Nodes.StatementNodes, inspect.isclass)))
    Nodes.StatementNodes.init(statementNodes)

    factNodes = list(map(lambda x: x[1], inspect.getmembers(Nodes.FactNodes, inspect.isclass)))
    Nodes.FactNodes.init(factNodes)

    instanceNodes = list(map(lambda x: x[1], inspect.getmembers(Nodes.InstanceNodes, inspect.isclass)))
    Nodes.InstanceNodes.init(instanceNodes)

    actionNodes = list(map(lambda x: x[1], inspect.getmembers(Nodes.ActionNodes, inspect.isclass)))
    Nodes.ActionNodes.init(actionNodes)

    miscNodes = list(map(lambda x: x[1], inspect.getmembers(Nodes.MiscNodes, inspect.isclass)))
    Nodes.MiscNodes.init(miscNodes)

    conceptNodes = list(map(lambda x: x[1], inspect.getmembers(Nodes.ConceptNodes, inspect.isclass)))
    Nodes.ConceptNodes.init(conceptNodes)


def quoteTokens(tStr):
    result = ""

    inString = False
    for char in tStr:
        if char in ["(", ")", " "]:
            if inString:
                inString = False
                result += "'"
            result += char
            continue

        if not inString:
            inString = True
            result += "'"

        result += char

    return result


def fixNonListTokens(tree):
    if (len(tree) == 1):
        return

    args = tree[1:]

    for i, arg in enumerate(args):
        if not isinstance(arg, list):
            tree[i + 1] = [arg]

        fixNonListTokens(tree[i + 1])


def mkTree(tStr):
    tStrQuoted = quoteTokens(tStr)
    result = ast.literal_eval(tStrQuoted.replace("(", "[").replace(")", "]").replace(" ", ", "))

    return result


def extractTriples(tree):
    nodeTree = Nodes.StatementNodes.statementFromTree(tree)

    return KnowledgeChunk(
        nodeTree
    ).process()


def getVarMap(triples):
    result = {}
    for triple in triples:
        var = triple[0]
        if not var in result:
            result[var] = []

        doAppend = True
        if triple[1] in DYN_PROPERTIES:
            for i, t in enumerate(result[var]):
                if t[0] == triple[1]:
                    result[var][i] = (t[0], f"({t[1]}) & ({triple[2]})")
                    doAppend = False

        if doAppend:
            result[var].append(triple[1:])

    return result


def appendToCombination(prop, ref, combination):
    if isinstance(combination, CombinationAnd):
        result = CombinationAnd(list(map(lambda x: appendToCombination(prop, ref, x), combination.args)))
    elif isinstance(combination, CombinationOr):
        result = CombinationOr(list(map(lambda x: appendToCombination(prop, ref, x), combination.args)))
    else:
        result = combination.copy()
        result[prop] = ref

    return result


def combine(properties):
    if len(properties) == 0:
        return None

    prop = properties[0]
    otherProps = list(filter(lambda p: p != prop, properties))
    subCombinations = combine(otherProps)

    ors = []
    for clause in prop[1]:
        ands = []
        for ref in clause:
            if len(otherProps) == 0:
                ands.append({
                    prop[0]: ref
                })
            else:
                if subCombinations is None:
                    continue

                ands.append(appendToCombination(prop[0], ref, subCombinations))
        ors.append(ands)

    if len(ors) == 0:
        return None
    elif len(ors) == 1:
        if len(ors[0]) == 1:
            return ors[0][0]

        return CombinationAnd(ors[0])

    return CombinationOr(list(map(lambda x: x[0] if len(x) == 1 else CombinationAnd(x), ors)))


def doExpansion(combinations, var, varRenames):
    resultMap = {}

    if isinstance(combinations, CombinationAnd):
        results = list(map(lambda x: doExpansion(x, var, varRenames), combinations.args))
        resultExp = " & ".join(list(map(lambda x: "(" + x[0] + ")", results)))
        for subMap in list(map(lambda x: x[1], results)):
            for newKey in subMap:
                resultMap[newKey] = subMap[newKey]
    elif isinstance(combinations, CombinationOr):
        results = list(map(lambda x: doExpansion(x, var, varRenames), combinations.args))
        resultExp = " | ".join(list(map(lambda x: "(" + x[0] + ")", results)))
        for subMap in list(map(lambda x: x[1], results)):
            for newKey in subMap:
                resultMap[newKey] = subMap[newKey]
    else:
        resultExp = useId() if not var in varRenames else varRenames[var]
        resultMap = {
            resultExp: combinations
        }

    return resultExp, resultMap


def handleSubVar(newTriples, varMap, ref, alreadyHandled, varRenames):
    (exp, triples) = handleVar(varMap, ref, alreadyHandled, varRenames)

    expDNF = getDNFList(exp)
    if expDNF is not None and len(expDNF) == 1 and len(expDNF[0]) == 1:
        varRenames[ref] = exp

    newTriples.extend(triples)

    return "(" + exp + ")"


def handleVar(varMap, var, alreadyHandled=None, varRenames=None):
    if alreadyHandled is None:
        alreadyHandled = []
    if varRenames is None:
        varRenames = {}

    properties = varMap[var]
    newTriples = []

    fixProperties = list(filter(
        lambda p: p[0] not in DYN_PROPERTIES, properties))
    dynamicProperties = list(filter(lambda p: p not in fixProperties, properties))

    newDynProperties = []
    for prop in dynamicProperties:
        tripleRepresentation = (var, prop[0], prop[1])
        if tripleRepresentation in alreadyHandled:
            continue

        alreadyHandled.append(tripleRepresentation)

        clauses = getDNFList(prop[1])

        newExp = " | ".join(
            map(lambda refs: "(" + (" & ".join(list(map(lambda ref: handleSubVar(newTriples, varMap, ref, alreadyHandled, varRenames),
                                                        refs)))) + ")", clauses))

        newDynProperties.append((prop[0], getDNFList(newExp)))

    combinations = None
    if ("a", KNOWLEDGE_CHUNK_IRI) in properties:
        for prop in newDynProperties:
            if prop[0] == FACTS_IRI:
                for clause in prop[1]:
                    factListId = useId()
                    newTriples.append((factListId, "a", FACT_LIST_IRI))
                    fixProperties.append((FACTS_IRI, factListId))
                    for ref in clause:
                        newTriples.append((factListId, FACT_REL_IRI, ref))
            else:
                fixProperties.append(prop)
    else:
        combinations = combine(newDynProperties)

    if combinations is None:
        for fixProperty in fixProperties:
            newTriples.append((var, fixProperty[0], fixProperty[1]))
        return var, newTriples
    else:
        (newVarExp, combMap) = doExpansion(combinations, var, varRenames)

        for newVar in combMap:
            for prop in combMap[newVar]:
                newTriples.append((newVar, prop, combMap[newVar][prop]))
            for fixProperty in fixProperties:
                newTriples.append((newVar, fixProperty[0], fixProperty[1]))

    return newVarExp, list(set(newTriples))


def expandConjunctions(varMap):
    kc = None
    for var in varMap:
        if len(list(filter(lambda t: t == ("a", KNOWLEDGE_CHUNK_IRI), varMap[var]))) > 0:
            kc = var
            break

    if kc is None:
        raise Exception("No knowledge chunk found")

    (_, triples) = handleVar(varMap, kc)

    return triples

# StatementPos TPres ASimul (Fact (InstancePron he_Pron) (PropAction schoen_A))
def extractFromTrees(trees):
    resultTriples = []

    for i, tree in enumerate(trees):
        try:
            print(tree)
            triples = extractTriples(mkTree(tree))[1]
            triples = sorted(expandConjunctions(getVarMap(triples)), key=lambda t: t[0])
            resultTriples.append(triples)
            print("Result triples for tree #" + str(i + 1) + ":")
            for triple in triples:
                print(triple)
            print("\n" * 2)

        except Exception as e:
            print("Could not extract triples from tree #" + str(i + 1) + ": " + str(e))

    return resultTriples


def extractFromSentence(sentence):
    trees = None
    try:
        trees = parse(sentence)
    except GFParseException as e:
        print("The sentence '" + sentence + "' could not be parsed: " + str(e))

    if trees is None:
        return []

    print("Triple sets for sentence '" + sentence + "'")

    # TODO: there has to be some way to do this properly...
    trees = list(map(lambda t: t
                     .replace("\\252", "ü")
                     .replace("\\220", "Ü")
                     .replace("\\228", "ä")
                     .replace("\\196", "Ä")
                     .replace("\\246", "ö")
                     .replace("\\214", "Ö")
                     .replace("\\223", "ß")
                 , trees))

    return extractFromTrees(trees[:100]) # only the first 100 trees to limit the processing time


def extractFinalInformationMap(caseDataInformation):
    result = {}

    groupedInfo = {}
    for prio, info, sentence in caseDataInformation:
        for key in info:
            if not key in groupedInfo:
                groupedInfo[key] = []

            for val in info[key]["values"]:
                groupedInfo[key].append((prio, val, sentence, info[key]["conditions"]))

    for key in groupedInfo:
        if INFO_KEYS[key]["multiple"]:
            result[key] = []
            uniqueElems = []
            for elem in groupedInfo[key]:
                existingElems = list(filter(lambda x: x[1] == elem[1], uniqueElems))
                existingElem = None if len(existingElems) == 0 else existingElems[0]
                if existingElem is None:
                    uniqueElems.append(elem)
                elif existingElem[0] < elem[0]:
                    uniqueElems.remove(existingElem)
                    uniqueElems.append(elem)

            for i, elem in enumerate(uniqueElems):
                infoElem = {}
                infoElem["val"] = elem[1]
                infoElem["conf"] = elem[0]
                infoElem["conflictingMax"] = []
                infoElem["alt"] = []
                infoElem["sourceSentence"] = elem[2]
                infoElem["conditions"] = elem[3]

                result[key].append(infoElem)
        else:
            result[key] = {}

            maxElem = max(groupedInfo[key], key=lambda t: t[0])
            result[key]["val"] = maxElem[1]
            result[key]["conf"] = maxElem[0]
            result[key]["conflictingMax"] = list(set(map(lambda x: (x[1], x[2], x[0], tuple(x[3])), filter(
                lambda t: t[0] == maxElem[0]
                          and t != maxElem
                          and t[1] != maxElem[1]
                , groupedInfo[key]))))
            result[key]["alt"] = list(set(map(lambda x: (x[1], x[2], x[0], tuple(x[3])), sorted(list(filter(
                lambda t: t[0] != maxElem[0]
                          and t[1] != maxElem[1]
                          and len(list(filter(lambda y: y[0] == t[1], result[key]["conflictingMax"]))) == 0
                          and len(list(filter(lambda x: x[1] == t[1] and x[0] > t[0], groupedInfo[key]))) == 0
                , groupedInfo[key])), key=lambda t: t[0], reverse=True))))
            result[key]["sourceSentence"] = maxElem[2]
            result[key]["conditions"] = maxElem[3]


    return result


def isVar(exp):
    if not isinstance(exp, str):
        return False

    return re.search("^x\d+$", exp) is not None


def isRef(exp):
    if not isinstance(exp, str):
        return False

    return exp.startswith(NODE_PREFIX)


def extractFromSentenceList(sentences):
    init()
    caseDataInformation = []

    try:
        startGF()
        for sentence in sentences:
            tripleSets = extractFromSentence(sentence)
            for tripleSet in tripleSets:
                success = insertTriples(list(map(lambda t: ("<" + str(t[0]) + ">", "<" + str(t[1]) + ">" if isRef(t[1]) else str(t[1]), "<" + str(t[2]) + ">" if isVar(t[2]) or isRef(t[2]) else "'" + str(t[2]) + "'"), tripleSet)))

                if not success:
                    print("Unable to insert triples into graph database")
                    continue

                for query in QUERIES:
                    prio, info = query.run()
                    if info is not None:
                        caseDataInformation.append((prio, info, sentence))

                clearGraph()
    finally:
        stopGF()

    print("Extracted " + str(len(caseDataInformation)) + " pieces of relevant information")
    for info in caseDataInformation:
        print(info)

    print()

    print("The final extracted information is:")
    print(extractFinalInformationMap(caseDataInformation))

    return extractFinalInformationMap(caseDataInformation)

if __name__ == "__main__":
    if sys.argv[1] == "-st":
        try:
            init()
            extractFromTrees(sys.argv[2:])
        finally:
            clearGraph()
            stopGF()
    else:
        extractFromSentenceList(sys.argv[1:])
