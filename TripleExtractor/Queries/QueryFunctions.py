from GFUtils import linearize, GFLinearizeException


def getVariableValues(vars, var, matrix):
    result = []
    i = vars.index(var)

    for line in matrix:
        if line[i] is not None:
            result.append(line[i])

    return list(set(result))


def getLinearizedVariableValues(vars, var, matrix):
    result = []

    res = getVariableValues(vars, var, matrix)

    for val in res:
        try:
            linValue = linearize(val)
            result.append(linValue)
        except GFLinearizeException:
            result.append(val)

    return list(set(result))


def getVariableValuesInfoKeyMap(vars, var, matrix, key):
    result = getVariableValues(vars, var, matrix)

    return None if result is None or len(result) == 0 else {
        key: {"values": result, "conditions": getLinearizedVariableValues(vars, "cond", matrix)}
    }


def getLinearizedVariableValuesInfoKeyMap(vars, var, matrix, key):
    result = getLinearizedVariableValues(vars, var, matrix)

    return None if result is None or len(result) == 0 else {
        key: {"values": result, "conditions": getLinearizedVariableValues(vars, "cond", matrix)},
    }
