def And(exp1, exp2):
    return f"""
        {exp1}
        {exp2}
    """


def Or(exp1, exp2):
    return f"""
        {{
            {exp1}
        }}
        UNION
        {{
            {exp2}
        }}
    """


def Optional(exp):
    return f"""
        OPTIONAL {{
            {exp}
        }} .
    """


def Not(exp):
    return f"""
        FILTER NOT EXISTS {{
            {exp}
        }} .
    """


def getConditionSynTree(f="?f", cond="?cond"):
    return f"""
        {f} :cond ?condObj .
        ?condObj :syntree {cond} .
    """


def factIsCondition(f="?f", pf="?pf"):
    return f"""
        {pf} a :Fact .
        {pf} :cond {f} .
    """


def factNegated(f="?f"):
    return f"{f} :neg 'true' ."


def ifBoundToFact(exp, f="?f", rel="?rel", s="?s"):
    return f"""
        {{
            FILTER NOT EXISTS {{
                {f} a :Fact .
                {f} {rel} {s} .
            }}
        }}
        UNION
        {{
            {f} a :Fact .
            {f} {rel} {s} .
            {exp}
        }}
    """


def isLawSection(s="?s", law="?law", num="?num"):
    return f"""
        {s} a :LawSection .
        {s} :num '{num}' .
        {s} :law '{law}' .
    """


def hasNEConceptVar(conc, s="?s"):
    return f"""
        {s} a :NEConcept .
        {s} :conc {conc} .
    """


def hasConceptVar(conc, s="?s"):
    return f"""
        {s} a :Concept .
        {s} :conc {conc} .
    """


def hasConcept(conc, s="?s"):
    return f"""
        {s} a :Concept .
        {s} :conc '{conc}' .
    """


def hasLocation(locRef, s="?s"):
    return f"""
        {s} :loc {locRef} .
    """


def hasLocationConc(loc, s="?s", locRef="?locRef"):
    return f"""
        {s} :loc {locRef} .
        {locRef} :conc {loc} .
    """


def hasPropertyAdj(propRef, adj, s="?s"):
    return f"""
        {s} :prop {propRef} .
        {propRef} a :Property .
        {propRef} :adj '{adj}' .
    """


def hasOwner(ownerRef, s="?s"):
    return f"""
        {s} :owner {ownerRef} .
    """


def hasOwnerConc(owner, s="?s", ownerRef="?ownerRef"):
    return f"""
        {s} :owner {ownerRef} .
        {ownerRef} :conc '{owner}' .
    """


def factHasSubject(f="?f", subj="?s"):
    return f"""
        {f} :subj {subj} .
    """


def factHasObject(f="?f", obj="?s", act="?a"):
    return f"""
        {f} :act {act} .
        {act} a :Action .
        {act} :obj {obj} .
    """


def factHasSubjectOrObject(exp, f="?f", subj="?s", obj="?obj", act="?a"):
    return Or(
        And(
            factHasSubject(f, subj),

        ),
        factHasObject(f, obj, act),
    )


def factHasPredIri(pred, act="?a"):
    return f"{act} :pred <{pred}> ."


def factHasPred(pred, fact="?f", act="?a"):
    return f"""
        {fact} :act {act} .
        {act} :pred '{pred}' .
    """


def factHasModal(mod, act="?a"):
    return f"{act} :modal {mod} ."


def factHasPermission(act="?a", modalsRef="?modalsRef"):
    return f"""
        {act} :modal {modalsRef} .
        VALUES ({modalsRef}) {{ (:modPermission) (:modObligation) }}
    """


def factHasObligation(act="?a"):
    return f"{act} :modal :modObligation ."


def factHasPossibility(act="?a", modalsRef="?modalsRef"):
    return f"""
        {act} :modal {modalsRef} .
        VALUES ({modalsRef}) {{ (:modPossibility) (:modObligation) (:modPermission) }}
    """


def factHasDesire(act="?a"):
    return f"{act} :modal :modDesire ."


def actionHasSpecialRelIri(rel, target="?t", a="?a"):
    return f"""
        {a} <{rel}> {target} .
    """


def actionHasSpecialRelValue(rel, value, a="?a"):
    return f"""
        {a} <{rel}> '{value}' .
    """


def values(vals, optRef="?optRef"):
    return f"""
        {optRef} .
        VALUES ({optRef}) {{ {" ".join(list(map(lambda v: f"({v})", vals)))} }}
    """


def preamble(f="?f", cond="?cond", pf="?pf"):
    return And(
        Optional(getConditionSynTree(f, cond)),
        Not(factIsCondition(f, pf))
    )


def preamblePos(f="?f", cond="?cond", pf="?pf"):
    return And(
        preamble(f, cond, pf),
        Not(factNegated(f))
    )


def preambleNeg(f="?f", cond="?cond", pf="?pf"):
    return And(
        preamble(f, cond, pf),
        factNegated(f)
    )


def String(exp):
    return f"'{exp}'"
