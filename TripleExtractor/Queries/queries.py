from Queries.QueryFunctions import *
from SPARQLUtils import execQuery, getVars, getMatrix
from constants import GRAPH_IRI, SATZUNGSSITZ_LOCATION_KEY, EINZELVERTRETUNGSBERECHTIGUNG_POSSIBLE_KEY, \
    WAIVER_181_BGB_POSSIBLE_KEY, GESELLSCHAFTER_LIST_KEY, BE_IRI, NEW_GF_NAME_KEY, ZU_IRI, PART_IRI, \
    NEW_GF_WAIVER_181_BGB_KEY, NEW_GF_REPRESENTATION_TYPE_KEY, MOD_A_IRI
from Queries.QueryBuilder import *


class Query:
    def __init__(self, vars, query, resultBuilder, prio = 0.5):
        self.vars = list(map(lambda v: "?" + v, vars))
        self.query = query
        self.resultBuilder = resultBuilder
        self.prio = prio

    def run(self):
        q = "SELECT DISTINCT " + " ".join(self.vars) + " FROM <" + GRAPH_IRI + "> WHERE {" + self.query + "}"

        res = execQuery(q)
        vars = getVars(res)
        matrix = getMatrix(res)
        return self.prio, None if matrix is None else self.resultBuilder(vars, matrix)


# Satzungssitz having location, e.g., "der Satzungssitz ist in Nürnberg", "der Satzungssitz in Nürnberg ist ...", ...
q_satzungssitz_location = Query(
    ["loc", "cond"],
    And(
        ifBoundToFact(
            preamblePos()
        ),
        And(
            hasConcept("satzungssitz_N"),
            hasLocationConc("?loc")
        )
    ),
    lambda vars, matrix: getLinearizedVariableValuesInfoKeyMap(vars, "loc", matrix, SATZUNGSSITZ_LOCATION_KEY),
    0.5
)

# Satzungssitz der Gesellschaft having location (see above)
q_satzungssitz_der_gesellschaft_location = Query(
    ["loc", "cond"],
    And(
        ifBoundToFact(
            preamblePos()
        ),
        And(
            hasConcept("satzungssitz_N"),
            And(
                hasLocationConc("?loc"),
                hasOwnerConc("company_N")
            )
        )
    ),
    lambda vars, matrix: getLinearizedVariableValuesInfoKeyMap(vars, "loc", matrix, SATZUNGSSITZ_LOCATION_KEY),
    1
)

# Einzelvertretungsberechtigung ocurring as object
q_einzelvertretungsberechtigung_no_verbs_no_modals_possible = Query(
    ["f", "cond"],
    And(
        preamblePos(),
        And(
            factHasObject(),
            hasConcept("einzelvertretungsberechtigung_N")
        )
    ),
    lambda vars, matrix: None if len(matrix) == 0 else {
        EINZELVERTRETUNGSBERECHTIGUNG_POSSIBLE_KEY: {"values": [True], "conditions": getLinearizedVariableValues(vars, "cond", matrix)}
    },
    0.1
)

# Einzelvertretungsberechtigung being granted, e.g., "Einzelvertretungsberechtigung wird gewährt"
q_einzelvertretungsberechtigung_granted_no_modals_possible = Query(
    ["f", "cond"],
    And(
        preamblePos(),
        And(
            factHasObject(),
            And(
                hasConcept("einzelvertretungsberechtigung_N"),
                factHasPred("grant_V2")
            )
        )
    ),
    lambda vars, matrix: None if len(matrix) == 0 else {
        EINZELVERTRETUNGSBERECHTIGUNG_POSSIBLE_KEY: {"values": [True], "conditions": getLinearizedVariableValues(vars, "cond", matrix)}
    },
    0.5
)

# Einzelvertretungsberechtigung being granted with modal, e.g., "Einzelvertretungsberechtigung kann gewährt werden"
q_einzelvertretungsberechtigung_granted_with_modals_possible = Query(
    ["f", "cond"],
    And(
        preamblePos(),
        And(
            factHasObject(),
            And(
                hasConcept("einzelvertretungsberechtigung_N"),
                And(
                    factHasPred("grant_V2"),
                    factHasPermission()
                )
            )
        )
    ),
    lambda vars, matrix: None if len(matrix) == 0 else {
        EINZELVERTRETUNGSBERECHTIGUNG_POSSIBLE_KEY: {"values": [True], "conditions": getLinearizedVariableValues(vars, "cond", matrix)}
    },
    1
)

# Einzelvertretungsberechtigung ocurring as object in negated fact
q_einzelvertretungsberechtigung_no_verbs_no_modals_not_possible = Query(
    ["f", "cond"],
    And(
      preambleNeg(),
      And(
          factHasObject(),
          hasConcept("einzelvertretungsberechtigung_N")
      )
    ),
    lambda vars, matrix: None if len(matrix) == 0 else {
        EINZELVERTRETUNGSBERECHTIGUNG_POSSIBLE_KEY: {"values": [False], "conditions": getLinearizedVariableValues(vars, "cond", matrix)}
    },
    0.1
)

# Einzelvertretungsberechtigung not being granted
q_einzelvertretungsberechtigung_granted_no_modals_not_possible = Query(
    ["f", "cond"],
    And(
        preambleNeg(),
        And(
            factHasObject(),
            And(
                hasConcept("einzelvertretungsberechtigung_N"),
                factHasPred("grant_V2")
            )
        )
    ),
    lambda vars, matrix: None if len(matrix) == 0 else {
        EINZELVERTRETUNGSBERECHTIGUNG_POSSIBLE_KEY: {"values": [False], "conditions": getLinearizedVariableValues(vars, "cond", matrix)}
    },
    0.5
)

# Einzelvertretungsberechtigung not being granted with modals, e.g., "Einzelvertretungsberechtigung kann nicht gewährt werden"
q_einzelvertretungsberechtigung_granted_with_modals_not_possible = Query(
    ["f", "cond"],
    And(
        preambleNeg(),
        And(
            factHasObject(),
            And(
                hasConcept("einzelvertretungsberechtigung_N"),
                And(
                    factHasPred("grant_V2"),
                    factHasPermission()
                )
            )
        )
    ),
    lambda vars, matrix: None if len(matrix) == 0 else {
        EINZELVERTRETUNGSBERECHTIGUNG_POSSIBLE_KEY: {"values": [False], "conditions": getLinearizedVariableValues(vars, "cond", matrix)}
    },
    1
)

# § 181 BGB ocurring as subject or object
q_181BGB_subj_or_obj = Query(
    ["f", "cond"],
    And(
        preamblePos(),
        Or(
            And(
                factHasSubject(),
                isLawSection(law="bgb_CLaw", num=181)
            ),
            And(
                factHasObject(obj="?obj"),
                isLawSection(s="?obj", law="bgb_CLaw", num=181)
            )
        )
    ),
    lambda vars, matrix: None if len(matrix) == 0 else {
        WAIVER_181_BGB_POSSIBLE_KEY: {"values": [True], "conditions": getLinearizedVariableValues(vars, "cond", matrix)}
    },
    0.1
)

# Beschränkungen des § 181 BGB ocurring as subject or object
q_restrictions_of_181BGB_subj_or_obj = Query(
    ["f", "cond"],
    And(
        preamblePos(),
        Or(
            And(
                factHasSubject(),
                And(
                    hasConcept("beschraenkung_N"),
                    And(
                        hasOwner("?l"),
                        isLawSection(s="?l", law="bgb_CLaw", num=181)
                    )
                ),
            ),
            And(
                factHasObject(obj="?obj"),
                And(
                    hasConcept("beschraenkung_N", s="?obj"),
                    And(
                        hasOwner("?l", s="?obj"),
                        isLawSection(s="?l", law="bgb_CLaw", num=181)
                    )
                ),
            )
        )
    ),
    lambda vars, matrix: None if len(matrix) == 0 else {
        WAIVER_181_BGB_POSSIBLE_KEY: {"values": [True], "conditions": getLinearizedVariableValues(vars, "cond", matrix)}
    },
    0.25,
)

# Befreiung von § 181 BGB ocurring as subject or object
q_waiver_of_181BGB_subj_or_obj = Query(
    ["f", "cond"],
    And(
        preamblePos(),
        Or(
            And(
                factHasSubject(),
                And(
                    hasConcept("befreiung_N"),
                    And(
                        hasOwner("?l"),
                        isLawSection(s="?l", law="bgb_CLaw", num=181)
                    )
                ),
            ),
            And(
                factHasObject(obj="?obj"),
                And(
                    hasConcept("befreiung_N", s="?obj"),
                    And(
                        hasOwner("?l", s="?obj"),
                        isLawSection(s="?l", law="bgb_CLaw", num=181)
                    )
                ),
            )
        )
    ),
    lambda vars, matrix: None if len(matrix) == 0 else {
        WAIVER_181_BGB_POSSIBLE_KEY: {"values": [True], "conditions": getLinearizedVariableValues(vars, "cond", matrix)}
    },
    0.5,
)

# Befreiung von Beschränkungen des § 181 BGB ocurring as subject or object
q_waiver_of_restrictions_of_181BGB_subj_or_obj = Query(
    ["f", "cond"],
    And(
        preamblePos(),
        Or(
            And(
                factHasSubject(),
                And(
                    hasConcept("befreiung_N"),
                    And(
                        hasOwner("?restr"),
                        And(
                            hasConcept("beschraenkung_N", s="?restr"),
                            And(
                                hasOwner("?l", s="?restr"),
                                isLawSection(s="?l", law="bgb_CLaw", num=181),
                            ),
                        ),
                    ),
                ),
            ),
            And(
                factHasObject(obj="?obj"),
                And(
                    hasConcept("befreiung_N", s="?obj"),
                    And(
                        hasOwner("?restr", s="?obj"),
                        And(
                            hasConcept("beschraenkung_N", s="?restr"),
                            And(
                                hasOwner("?l", s="?restr"),
                                isLawSection(s="?l", law="bgb_CLaw", num=181),
                            ),
                        ),
                    ),
                ),
            ),
        )
    ),
    lambda vars, matrix: None if len(matrix) == 0 else {
        WAIVER_181_BGB_POSSIBLE_KEY: {"values": [True], "conditions": getLinearizedVariableValues(vars, "cond", matrix)}
    },
    0.75,
)

# Befreiung von Beschränkungen des § 181 BGB being granted
q_waiver_of_restrictions_of_181BGB_subj_or_obj_grant = Query(
    ["f", "cond"],
    And(
        preamblePos(),
        And(
            And(
                factHasObject(),
                And(
                    hasConcept("befreiung_N"),
                    And(
                        hasOwner("?restr"),
                        And(
                            hasConcept("beschraenkung_N", s="?restr"),
                            And(
                                hasOwner("?l", s="?restr"),
                                isLawSection(s="?l", law="bgb_CLaw", num=181),
                            ),
                        ),
                    ),
                ),
            ),
            factHasPred("grant_V2")
        ),
    ),
    lambda vars, matrix: None if len(matrix) == 0 else {
        WAIVER_181_BGB_POSSIBLE_KEY: {"values": [True], "conditions": getLinearizedVariableValues(vars, "cond", matrix)}
    },
    0.9,
)

# Befreiung von Beschränkungen des § 181 BGB being granted with possibility modal
q_waiver_of_restrictions_of_181BGB_subj_or_obj_grant_modal = Query(
    ["f", "cond"],
    And(
        preamblePos(),
        And(
            And(
                And(
                    factHasObject(),
                    And(
                        hasConcept("befreiung_N"),
                        And(
                            hasOwner("?restr"),
                            And(
                                hasConcept("beschraenkung_N", s="?restr"),
                                And(
                                    hasOwner("?l", s="?restr"),
                                    isLawSection(s="?l", law="bgb_CLaw", num=181),
                                ),
                            ),
                        ),
                    ),
                ),
                factHasPred("grant_V2"),
            ),
            factHasPossibility(),
        ),
    ),
    lambda vars, matrix: None if len(matrix) == 0 else {
        WAIVER_181_BGB_POSSIBLE_KEY: {"values": [True], "conditions": getLinearizedVariableValues(vars, "cond", matrix)}
    },
    1,
)

# § 181 BGB ocurring as subject or object in a negated fact
q_181BGB_subj_or_obj_negated = Query(
    ["f", "cond"],
    And(
        preambleNeg(),
        Or(
            And(
                factHasSubject(),
                isLawSection(law="bgb_CLaw", num=181)
            ),
            And(
                factHasObject(obj="?obj"),
                isLawSection(s="?obj", law="bgb_CLaw", num=181)
            )
        )
    ),
    lambda vars, matrix: None if len(matrix) == 0 else {
        WAIVER_181_BGB_POSSIBLE_KEY: {"values": [False], "conditions": getLinearizedVariableValues(vars, "cond", matrix)}
    },
    0.1,
)

# Beschränkungen des § 181 BGB ocurring as subject or object in a negated fact
q_restrictions_of_181BGB_subj_or_obj_negated = Query(
    ["f", "cond"],
    And(
        preambleNeg(),
        Or(
            And(
                factHasSubject(),
                And(
                    hasConcept("beschraenkung_N"),
                    And(
                        hasOwner("?l"),
                        isLawSection(s="?l", law="bgb_CLaw", num=181)
                    )
                ),
            ),
            And(
                factHasObject(obj="?obj"),
                And(
                    hasConcept("beschraenkung_N", s="?obj"),
                    And(
                        hasOwner("?l", s="?obj"),
                        isLawSection(s="?l", law="bgb_CLaw", num=181)
                    )
                ),
            )
        )
    ),
    lambda vars, matrix: None if len(matrix) == 0 else {
        WAIVER_181_BGB_POSSIBLE_KEY: {"values": [False], "conditions": getLinearizedVariableValues(vars, "cond", matrix)}
    },
    0.25,
)

# Befreiung von § 181 BGB ocurring as subject or object in a negated fact
q_waiver_of_181BGB_subj_or_obj_negated = Query(
    ["f", "cond"],
    And(
        preambleNeg(),
        Or(
            And(
                factHasSubject(),
                And(
                    hasConcept("befreiung_N"),
                    And(
                        hasOwner("?l"),
                        isLawSection(s="?l", law="bgb_CLaw", num=181)
                    )
                ),
            ),
            And(
                factHasObject(obj="?obj"),
                And(
                    hasConcept("befreiung_N", s="?obj"),
                    And(
                        hasOwner("?l", s="?obj"),
                        isLawSection(s="?l", law="bgb_CLaw", num=181)
                    )
                ),
            )
        )
    ),
    lambda vars, matrix: None if len(matrix) == 0 else {
        WAIVER_181_BGB_POSSIBLE_KEY: {"values": [False], "conditions": getLinearizedVariableValues(vars, "cond", matrix)}
    },
    0.5,
)

# Befreiung von Beschränkungen des § 181 BGB ocurring as subject or object in a negated fact
q_waiver_of_restrictions_of_181BGB_subj_or_obj_negated = Query(
    ["f", "cond"],
    And(
        preambleNeg(),
        Or(
            And(
                factHasSubject(),
                And(
                    hasConcept("befreiung_N"),
                    And(
                        hasOwner("?restr"),
                        And(
                            hasConcept("beschraenkung_N", s="?restr"),
                            And(
                                hasOwner("?l", s="?restr"),
                                isLawSection(s="?l", law="bgb_CLaw", num=181),
                            ),
                        ),
                    ),
                ),
            ),
            And(
                factHasObject(obj="?obj"),
                And(
                    hasConcept("befreiung_N", s="?obj"),
                    And(
                        hasOwner("?restr", s="?obj"),
                        And(
                            hasConcept("beschraenkung_N", s="?restr"),
                            And(
                                hasOwner("?l", s="?restr"),
                                isLawSection(s="?l", law="bgb_CLaw", num=181),
                            ),
                        ),
                    ),
                ),
            ),
        )
    ),
    lambda vars, matrix: None if len(matrix) == 0 else {
        WAIVER_181_BGB_POSSIBLE_KEY: {"values": [False], "conditions": getLinearizedVariableValues(vars, "cond", matrix)}
    },
    0.75,
)

# Befreiung von Beschränkungen des § 181 BGB being granted in a negated fact
q_waiver_of_restrictions_of_181BGB_subj_or_obj_grant_negated = Query(
    ["f", "cond"],
    And(
        preambleNeg(),
        And(
            And(
                factHasObject(),
                And(
                    hasConcept("befreiung_N"),
                    And(
                        hasOwner("?restr"),
                        And(
                            hasConcept("beschraenkung_N", s="?restr"),
                            And(
                                hasOwner("?l", s="?restr"),
                                isLawSection(s="?l", law="bgb_CLaw", num=181),
                            ),
                        ),
                    ),
                ),
            ),
            factHasPred("grant_V2")
        ),
    ),
    lambda vars, matrix: None if len(matrix) == 0 else {
        WAIVER_181_BGB_POSSIBLE_KEY: {"values": [False], "conditions": getLinearizedVariableValues(vars, "cond", matrix)}
    },
    0.9,
)

# Befreiung von Beschränkungen des § 181 BGB being granted in a negated fact with possibility modal
q_waiver_of_restrictions_of_181BGB_subj_or_obj_grant_modal_negated = Query(
    ["f", "cond"],
    And(
        preambleNeg(),
        And(
            And(
                And(
                    factHasObject(),
                    And(
                        hasConcept("befreiung_N"),
                        And(
                            hasOwner("?restr"),
                            And(
                                hasConcept("beschraenkung_N", s="?restr"),
                                And(
                                    hasOwner("?l", s="?restr"),
                                    isLawSection(s="?l", law="bgb_CLaw", num=181),
                                ),
                            ),
                        ),
                    ),
                ),
                factHasPred("grant_V2"),
            ),
            factHasPossibility(),
        ),
    ),
    lambda vars, matrix: None if len(matrix) == 0 else {
        WAIVER_181_BGB_POSSIBLE_KEY: {"values": [False], "conditions": getLinearizedVariableValues(vars, "cond", matrix)}
    },
    1,
)


q_is_shareholder = Query(
    ["sh", "cond"],
    And(
        preamblePos(),
        And(
            factHasSubject(),
            And(
                hasNEConceptVar("?sh"),
                And(
                    factHasPredIri(BE_IRI),
                    And(
                        factHasObject("?f", "?obj"),
                        hasConcept("gesellschafter_N", s="?obj"),
                    ),
                ),
            ),
        ),
    ),
    lambda vars, matrix: getVariableValuesInfoKeyMap(vars, "sh", matrix, GESELLSCHAFTER_LIST_KEY),
    0.5,
)


q_is_shareholder_of_company = Query(
    ["sh", "cond"],
    And(
        preamblePos(),
        And(
            factHasSubject(),
            And(
                hasNEConceptVar("?sh"),
                And(
                    factHasPredIri(BE_IRI),
                    And(
                        factHasObject(obj="?obj"),
                        And(
                            hasConcept("gesellschafter_N", s="?obj"),
                            hasOwnerConc("company_N", s="?obj"),
                        ),
                    ),
                ),
            ),
        ),
    ),
    lambda vars, matrix: getVariableValuesInfoKeyMap(vars, "sh", matrix, GESELLSCHAFTER_LIST_KEY),
    1,
)


q_is_director = Query(
    ["d", "cond"],
    And(
        preamblePos(),
        And(
            factHasSubject(),
            And(
                hasNEConceptVar("?d"),
                And(
                    factHasPredIri(BE_IRI),
                    And(
                        factHasObject(obj="?obj"),
                        hasConcept("geschaeftsfuehrer_N", s="?obj"),
                    ),
                ),
            ),
        ),
    ),
    lambda vars, matrix: getVariableValuesInfoKeyMap(vars, "d", matrix, NEW_GF_NAME_KEY),
    0.3,
)


q_is_director_of_company = Query(
    ["d", "cond"],
    And(
        preamblePos(),
        And(
            factHasSubject(),
            And(
                hasNEConceptVar("?d"),
                And(
                    factHasPredIri(BE_IRI),
                    And(
                        factHasObject(obj="?obj"),
                        And(
                            hasConcept("geschaeftsfuehrer_N", s="?obj"),
                            hasOwnerConc("company_N", s="?obj"),
                        ),
                    ),
                ),
            ),
        ),
    ),
    lambda vars, matrix: getVariableValuesInfoKeyMap(vars, "d", matrix, NEW_GF_NAME_KEY),
    0.5,
)


q_is_new_director = Query(
    ["d", "cond"],
    And(
        preamblePos(),
        And(
            factHasSubject(),
            And(
                hasNEConceptVar("?d"),
                And(
                    factHasPredIri(BE_IRI),
                    And(
                        factHasObject(obj="?obj"),
                        And(
                            hasConcept("geschaeftsfuehrer_N", s="?obj"),
                            And(
                                hasOwnerConc("company_N", s="?obj"),
                                hasPropertyAdj("?prop", "neu_A", "?obj"),
                            ),
                        ),
                    ),
                ),
            ),
        ),
    ),
    lambda vars, matrix: getVariableValuesInfoKeyMap(vars, "d", matrix, NEW_GF_NAME_KEY),
    0.75,
)


q_is_new_director_of_company = Query(
    ["d", "cond"],
    And(
        preamblePos(),
        And(
            factHasSubject(),
            And(
                hasNEConceptVar("?d"),
                And(
                    factHasPredIri(BE_IRI),
                    And(
                        factHasObject(obj="?obj"),
                        And(
                            hasConcept("geschaeftsfuehrer_N", s="?obj"),
                            hasPropertyAdj("?prop", "neu_A", "?obj"),
                        ),
                    ),
                ),
            ),
        ),
    ),
    lambda vars, matrix: getVariableValuesInfoKeyMap(vars, "d", matrix, NEW_GF_NAME_KEY),
    1,
)


q_is_appointed_director = Query(
    ["d", "cond"],
    And(
        preamblePos(),
        And(
            factHasObject(),
            And(
                hasNEConceptVar("?d"),
                And(
                    actionHasSpecialRelIri(ZU_IRI),
                    hasConcept("geschaeftsfuehrer_N", s="?t"),
                ),
            ),
        ),
    ),
    lambda vars, matrix: getVariableValuesInfoKeyMap(vars, "d", matrix, NEW_GF_NAME_KEY),
    0.5,
)

q_is_appointed_new_director = Query(
    ["d", "cond"],
    And(
        preamblePos(),
        And(
            factHasObject(),
            And(
                hasNEConceptVar("?d"),
                And(
                    actionHasSpecialRelIri(ZU_IRI),
                    And(
                        hasConcept("geschaeftsfuehrer_N", s="?t"),
                        hasPropertyAdj("?prop", "neu_A", "?t"),
                    ),
                ),
            ),
        ),
    ),
    lambda vars, matrix: getVariableValuesInfoKeyMap(vars, "d", matrix, NEW_GF_NAME_KEY),
    0.75,
)


q_is_appointed_new_director_of_company = Query(
    ["d", "cond"],
    And(
        preamblePos(),
        And(
            factHasObject(),
            And(
                hasNEConceptVar("?d"),
                And(
                    actionHasSpecialRelIri(ZU_IRI),
                    And(
                        hasConcept("geschaeftsfuehrer_N", s="?t"),
                        And(
                            hasOwnerConc("company_N", s="?t"),
                            hasPropertyAdj("?prop", "neu_A", "?t"),
                        ),
                    ),
                ),
            ),
        ),
    ),
    lambda vars, matrix: getVariableValuesInfoKeyMap(vars, "d", matrix, NEW_GF_NAME_KEY),
    1,
)


q_free_from_181_BGBG = Query(
    ["a", "cond"],
    And(
        preamblePos(),
        And(
            factHasPred("befreit_A"),
            And(
                actionHasSpecialRelIri(PART_IRI),
                isLawSection(s="?l", law="bgb_CLaw", num=181),
            ),
        ),
    ),
    lambda vars, matrix: None if len(matrix) == 0 else {
        NEW_GF_WAIVER_181_BGB_KEY: {"values": [True], "conditions": getLinearizedVariableValues(vars, "cond", matrix)}
    },
    0.5,
)


q_free_from_restrictions_of_181_BGBG = Query(
    ["a", "cond"],
    And(
        preamblePos(),
        And(
            factHasPred("befreit_A"),
            And(
                actionHasSpecialRelIri(PART_IRI),
                And(
                    hasConcept("beschraenkung_N", s="?t"),
                    And(
                        hasOwner("?l", s="?t"),
                        isLawSection(s="?l", law="bgb_CLaw", num=181),
                    ),
                ),
            ),
        ),
    ),
    lambda vars, matrix: None if len(matrix) == 0 else {
        NEW_GF_WAIVER_181_BGB_KEY: {"values": [True], "conditions": getLinearizedVariableValues(vars, "cond", matrix)}
    },
    1,
)


q_free_from_181_BGBG_negated = Query(
    ["a", "cond"],
    And(
        preambleNeg(),
        And(
            factHasPred("befreit_A"),
            And(
                actionHasSpecialRelIri(PART_IRI),
                isLawSection(s="?l", law="bgb_CLaw", num=181),
            ),
        ),
    ),
    lambda vars, matrix: None if len(matrix) == 0 else {
        NEW_GF_WAIVER_181_BGB_KEY: {"values": [False], "conditions": getLinearizedVariableValues(vars, "cond", matrix)}
    },
    0.5,
)


q_free_from_restrictions_of_181_BGBG_negated = Query(
    ["a", "cond"],
    And(
        preambleNeg(),
        And(
            factHasPred("befreit_A"),
            And(
                actionHasSpecialRelIri(PART_IRI),
                And(
                    hasConcept("beschraenkung_N", s="?t"),
                    And(
                        hasOwner("?l", s="?t"),
                        isLawSection(s="?l", law="bgb_CLaw", num=181),
                    ),
                ),
            ),
        ),
    ),
    lambda vars, matrix: None if len(matrix) == 0 else {
        NEW_GF_WAIVER_181_BGB_KEY: {"values": [False], "conditions": getLinearizedVariableValues(vars, "cond", matrix)}
    },
    1,
)


q_represents_alone = Query(
    ["a", "cond"],
    And(
        preamblePos(),
        And(
            factHasPred("vertreten_V2"),
            actionHasSpecialRelValue(MOD_A_IRI, "allein_A"),
        ),
    ),
    lambda vars, matrix: None if len(matrix) == 0 else {
        NEW_GF_REPRESENTATION_TYPE_KEY: {"values": ["alleinvertretung"], "conditions": getLinearizedVariableValues(vars, "cond", matrix)}
    },
    0.5,
)


q_represents_company_alone = Query(
    ["a", "cond"],
    And(
        preamblePos(),
        And(
            factHasPred("vertreten_V2"),
            And(
                factHasObject(),
                And(
                    hasConcept("company_N"),
                    actionHasSpecialRelValue(MOD_A_IRI, "allein_A"),
                ),
            ),
        ),
    ),
    lambda vars, matrix: None if len(matrix) == 0 else {
        NEW_GF_REPRESENTATION_TYPE_KEY: {"values": ["alleinvertretung"], "conditions": getLinearizedVariableValues(vars, "cond", matrix)}
    },
    1,
)



QUERIES = [
    q_satzungssitz_location,
    q_satzungssitz_der_gesellschaft_location,

    # q_einzelvertretungsberechtigung_no_verbs_no_modals_possible,
    q_einzelvertretungsberechtigung_granted_no_modals_possible,
    q_einzelvertretungsberechtigung_granted_with_modals_possible,

    # q_einzelvertretungsberechtigung_no_verbs_no_modals_not_possible,
    q_einzelvertretungsberechtigung_granted_no_modals_not_possible,
    q_einzelvertretungsberechtigung_granted_with_modals_not_possible,

    q_181BGB_subj_or_obj,
    q_restrictions_of_181BGB_subj_or_obj,
    q_waiver_of_181BGB_subj_or_obj,
    q_waiver_of_restrictions_of_181BGB_subj_or_obj,
    q_waiver_of_restrictions_of_181BGB_subj_or_obj_grant,
    q_waiver_of_restrictions_of_181BGB_subj_or_obj_grant_modal,

    q_181BGB_subj_or_obj_negated,
    q_restrictions_of_181BGB_subj_or_obj_negated,
    q_waiver_of_181BGB_subj_or_obj_negated,
    q_waiver_of_restrictions_of_181BGB_subj_or_obj_negated,
    q_waiver_of_restrictions_of_181BGB_subj_or_obj_grant_negated,
    q_waiver_of_restrictions_of_181BGB_subj_or_obj_grant_modal_negated,

    q_is_shareholder,
    q_is_shareholder_of_company,

    q_is_director,
    q_is_director_of_company,
    q_is_new_director,
    q_is_new_director_of_company,
    q_is_appointed_director,
    q_is_appointed_new_director,
    q_is_appointed_new_director_of_company,

    q_free_from_181_BGBG,
    q_free_from_restrictions_of_181_BGBG,

    q_free_from_181_BGBG_negated,
    q_free_from_restrictions_of_181_BGBG_negated,

    q_represents_alone,
    q_represents_company_alone,
]