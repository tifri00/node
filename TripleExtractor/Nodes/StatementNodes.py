import Nodes.FactNodes
import Nodes.MiscNodes
import Nodes.InstanceNodes
from Utility import useId, getDNFList, treeToString
from constants import *

nodes = []


def init(nodeList):
    global nodes
    nodes = nodeList


def statementFromTree(tree):
    if tree[0] in ["Condition5", "Condition6", "Condition7", "Condition8", "Condition9"]:
        tree[0] = "Condition"
    elif tree[0] in ["Reason2"]:
        tree[0] = "Reason"
    elif tree[0] in ["AndS2"]:
        tree[0] = "AndS"

    statementNodes = list(filter(lambda x: x.treeName == tree[0], nodes))

    if statementNodes is None or len(statementNodes) == 0:
        raise Exception("Cannot construct statement from given tree " + str(tree))

    return statementNodes[0].fromTree(tree)


class StatementPos:
    treeName = "StatementPos"

    def __init__(self, fact):
        self.facts = [fact]
        self.fact = fact
        self.id = useId()

    def process(self):
        return self.fact.process()

    @staticmethod
    def fromTree(tree):
        if tree[0] != StatementPos.treeName or len(tree) != 4:
            raise Exception("Cannot create " + StatementPos.treeName + " from given tree " + str(tree))

        return StatementPos(Nodes.FactNodes.factFromTree(tree[3]))


class StatementNeg:
    treeName = "StatementNeg"

    def __init__(self, fact):
        self.facts = [fact]
        self.fact = fact
        self.id = useId()
        self.factRefs = []

    def process(self):
        (factIds, factTriples) = self.fact.process()
        self.factRefs = getDNFList(factIds)

        triples = []
        for refs in self.factRefs:
            for ref in refs:
                triples.append((ref, NEG_IRI, "true"))

        triples.extend(factTriples)
        return factIds, triples

    @staticmethod
    def fromTree(tree):
        if tree[0] != StatementNeg.treeName or len(tree) != 4:
            raise Exception("Cannot create " + StatementNeg.treeName + " from given tree " + str(tree))

        return StatementNeg(Nodes.FactNodes.factFromTree(tree[3]))


class StatementAdv:
    treeName = "StatementAdv"

    def __init__(self, adv, s):
        self.adv = adv
        self.s = s
        self.facts = s.facts
        self.id = useId()
        self.advRefs = []
        self.sRefs = []

    def process(self):
        triples = []

        (sIds, sTriples) = self.s.process()
        self.sRefs = getDNFList(sIds)

        action = None
        for fact in self.facts:
            if fact is not None:
                if fact.actionRefs is not None and len(fact.actionRefs) > 0 and len(fact.actionRefs[0]) > 0:
                    action = fact.actionRefs[0][0]
                    break

        (advIds, advTriples) = self.adv.process([action], self.facts)
        self.advRefs = getDNFList(advIds)

        if advIds is not None:
            for fact in self.facts:
                if fact is not None:
                    actionRefs = fact.actionRefs
                    if actionRefs is not None:
                        for refs in actionRefs:
                            for ref in refs:
                                triples.append((ref, MOD_IRI, advIds))

        triples.extend(advTriples)
        triples.extend(sTriples)
        return sIds, triples

    @staticmethod
    def fromTree(tree):
        if tree[0] != StatementAdv.treeName or len(tree) != 3:
            raise Exception("Cannot create " + StatementAdv.treeName + " from given tree " + str(tree))

        return StatementAdv(Nodes.MiscNodes.advFromTree(tree[1]), Nodes.StatementNodes.statementFromTree(tree[2]))


class Condition:
    treeName = "Condition"

    def __init__(self, fact, condition, syntaxTree):
        self.facts = [fact]
        self.fact = fact
        self.condition = condition
        self.id = useId()
        self.factRefs = []
        self.conditionRefs = []
        self.syntaxTree = syntaxTree

    def process(self):
        (factIds, factTriples) = self.fact.process()
        self.factRefs = getDNFList(factIds)

        (conditionIds, conditionTriples) = self.condition.process()
        self.conditionRefs = getDNFList(conditionIds)

        triples = []
        if conditionIds is not None:
            for refs in self.factRefs:
                for ref in refs:
                    triples.append((ref, COND_IRI, conditionIds))

            for refs in self.conditionRefs:
                for ref in refs:
                    triples.append((ref, SYNTREE_IRI, self.syntaxTree))

        triples.extend(factTriples)
        triples.extend(conditionTriples)
        return factIds, triples

    @staticmethod
    def fromTree(tree):
        if tree[0] != Condition.treeName or len(tree) != 3:
            raise Exception("Cannot create " + Condition.treeName + " from given tree " + str(tree))

        return Condition(statementFromTree(tree[1]), statementFromTree(tree[2]), treeToString(tree[2]))


class Condition2:
    treeName = "Condition2"


    @staticmethod
    def fromTree(tree):
        if tree[0] != Condition2.treeName or len(tree) != 3:
            raise Exception("Cannot create " + Condition2.treeName + " from given tree " + str(tree))

        return Condition(statementFromTree(tree[1]), statementFromTree(tree[2]), treeToString(tree[2]))


class Condition3:
    treeName = "Condition3"


    @staticmethod
    def fromTree(tree):
        if tree[0] != Condition3.treeName or len(tree) != 3:
            raise Exception("Cannot create " + Condition3.treeName + " from given tree " + str(tree))

        return Condition(statementFromTree(tree[1]), statementFromTree(tree[2]), treeToString(tree[2]))


class Condition4:
    treeName = "Condition4"


    @staticmethod
    def fromTree(tree):
        if tree[0] != Condition4.treeName or len(tree) != 3:
            raise Exception("Cannot create " + Condition4.treeName + " from given tree " + str(tree))

        return Condition(statementFromTree(tree[1]), statementFromTree(tree[2]), treeToString(tree[2]))


class Reason:
    treeName = "Reason"

    def __init__(self, fact, reason):
        self.facts = [fact]
        self.fact = fact
        self.reason = reason
        self.id = useId()
        self.factRefs = []
        self.reasonRefs = []

    def process(self):
        (factIds, factTriples) = self.fact.process()
        self.factRefs = getDNFList(factIds)

        (reasonIds, reasonTriples) = self.reason.process()
        self.reasonRefs = getDNFList(reasonIds)

        triples = []
        if reasonIds is not None:
            for refs in self.factRefs:
                for ref in refs:
                    triples.append((ref, REASON_IRI, reasonIds))

        triples.extend(factTriples)
        triples.extend(reasonTriples)
        return factIds, triples

    @staticmethod
    def fromTree(tree):
        if tree[0] != Reason.treeName or len(tree) != 3:
            raise Exception("Cannot create " + Reason.treeName + " from given tree " + str(tree))

        return Reason(statementFromTree(tree[1]), statementFromTree(tree[2]))


class Contradiction:
    treeName = "Contradiction"

    def __init__(self, fact, contradiction):
        self.facts = [fact]
        self.fact = fact
        self.contradiction = contradiction
        self.id = useId()
        self.factRefs = []
        self.contradictionRefs = []

    def process(self):
        (factIds, factTriples) = self.fact.process()
        self.factRefs = getDNFList(factIds)

        (contradictionIds, contradictionTriples) = self.contradiction.process()
        self.contradictionRefs = getDNFList(contradictionIds)

        triples = []
        if contradictionIds is not None:
            for refs in self.factRefs:
                for ref in refs:
                    triples.append((ref, CONTRADICTION_IRI, contradictionIds))

        triples.extend(factTriples)
        triples.extend(contradictionTriples)
        return factIds, triples

    @staticmethod
    def fromTree(tree):
        if tree[0] != Contradiction.treeName or len(tree) != 3:
            raise Exception("Cannot create " + Contradiction.treeName + " from given tree " + str(tree))

        return Contradiction(statementFromTree(tree[1]), statementFromTree(tree[2]))


class AndS:
    treeName = "AndS"

    def __init__(self, f1, f2):
        self.f1 = f1
        self.f2 = f2
        self.facts = [f1, f2]
        self.id = useId()

    def process(self):
        (f1Ids, f1Triples) = self.f1.process()
        (f2Ids, f2Triples) = self.f2.process()

        triples = []
        triples.extend(f1Triples)
        triples.extend(f2Triples)

        return "(" + f1Ids + ") & (" + f2Ids + ")", triples

    @staticmethod
    def fromTree(tree):
        if tree[0] != AndS.treeName or len(tree) != 3:
            raise Exception("Cannot create " + AndS.treeName + " from given tree " + str(tree))

        return AndS(statementFromTree(tree[1]), statementFromTree(tree[2]))


class OrS:
    treeName = "OrS"

    def __init__(self, f1, f2):
        self.f1 = f1
        self.f2 = f2
        self.facts = [f1, f2]
        self.id = useId()

    def process(self):
        (f1Ids, f1Triples) = self.f1.process()
        (f2Ids, f2Triples) = self.f2.process()

        triples = []
        triples.extend(f1Triples)
        triples.extend(f2Triples)

        return "(" + f1Ids + ") | (" + f2Ids + ")", triples

    @staticmethod
    def fromTree(tree):
        if tree[0] != OrS.treeName or len(tree) != 3:
            raise Exception("Cannot create " + OrS.treeName + " from given tree " + str(tree))

        return OrS(statementFromTree(tree[1]), statementFromTree(tree[2]))
