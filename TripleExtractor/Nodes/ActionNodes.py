from Utility import getDNFList, useId, DNFListToString, treeToString

import Nodes.InstanceNodes
import Nodes.StatementNodes
import Nodes.FactNodes
import Nodes.MiscNodes
from constants import *

nodes = []


def init(nodeList):
    global nodes
    nodes = nodeList


def actionFromTree(tree):
    if tree[0] in ["PropAction3", "PropAction5"]:
        tree[0] = "PropAction"
    elif tree[0] in ["PropAction4", "PropAction6", "PropActionColon"]:
        tree[0] = "PropAction2"
    elif tree[0] in ["Mod2", "Mod3", "Mod4", "Mod5"]:
        tree[0] = "Mod"
    elif tree[0] in ["ModA2", "ModA3", "ModA4", "ModA5"]:
        tree[0] = "ModA"
    elif tree[0] in ["Possibility2"]:
        tree[0] = "Possibility"
    elif tree[0] in ["Permission2"]:
        tree[0] = "Permission"
    elif tree[0] in ["ObligationSC2", "ObligationSC3"]:
        tree[0] = "ObligationSC"
    elif tree[0] in ["ActionSCWhatFollows2", "ActionSCWhatFollows3"]:
        tree[0] = "ActionSCWhatFollows"
    elif tree[0] in ["ActionPassActor2"]:
        tree[0] = "ActionPassActor"
    elif tree[0] in ["PermissionSC2"]:
        tree[0] = "PermissionSC"
    elif tree[0] in ["Action2Colon2"]:
        tree[0] = "Action2Colon"

    actionNodes = list(filter(lambda x: x.treeName == tree[0], nodes))

    if actionNodes is None or len(actionNodes) == 0:
        raise Exception("Cannot construct action from given tree " + str(tree))

    return actionNodes[0].fromTree(tree)


class Action:
    treeName = "Action"

    def __init__(self, verb):
        self.verb = verb
        self.id = useId()
        self.isPassive = False

    def process(self, facts):
        triples = [(self.id, "a", ACTION_IRI), (self.id, PRED_IRI, self.verb)]

        return self.id, triples

    @staticmethod
    def fromTree(tree):
        if tree[0] != Action.treeName or len(tree) != 2:
            raise Exception("Cannot create " + Action.treeName + " from given tree " + str(tree))

        return Action(tree[1])


class ActionAnd:
    treeName = "ActionAnd"

    def __init__(self, v1, v2):
        self.v1 = v1
        self.v2 = v2
        self.id = useId()
        self.isPassive = False

    def process(self, facts):
        action2Id = useId()

        triples = [(self.id, "a", ACTION_IRI),
                   (self.id, PRED_IRI, self.v1),
                   (action2Id, "a", ACTION_IRI),
                   (action2Id, PRED_IRI, self.v2)]

        return f"{self.id} & {action2Id}", triples

    @staticmethod
    def fromTree(tree):
        if tree[0] != ActionAnd.treeName or len(tree) != 3:
            raise Exception("Cannot create " + ActionAnd.treeName + " from given tree " + str(tree))

        return ActionAnd(tree[1], tree[2])


class ModZur:
    treeName = "ModZur"

    def __init__(self, action, i):
        self.id = useId()
        self.action = action
        self.i = i
        self.actionRefs = []
        self.iRefs = []
        self.isPassive = action.isPassive

    def process(self, facts):
        triples = []

        (actionIds, actionTriples) = self.action.process(facts)
        self.actionRefs = getDNFList(actionIds)

        (iIds, iTriples) = self.i.process(facts)
        self.iRefs = getDNFList(iIds)

        for refs in self.actionRefs:
            for ref in refs:
                triples.append((ref, ZU_IRI, iIds))

        triples.extend(actionTriples)
        triples.extend(iTriples)
        return actionIds, triples

    @staticmethod
    def fromTree(tree):
        if tree[0] != ModZur.treeName or len(tree) != 3:
            raise Exception("Cannot create " + ModZur.treeName + " from given tree " + str(tree))

        return ModZur(actionFromTree(tree[1]), Nodes.InstanceNodes.instanceFromTree(tree[2]))


class Action2:
    treeName = "Action2"

    def __init__(self, verb, obj):
        self.verb = verb
        self.obj = obj
        self.id = useId()
        self.objRefs = []
        self.isPassive = False

    def process(self, facts):
        triples = [(self.id, "a", ACTION_IRI), (self.id, PRED_IRI, self.verb)]

        (objIds, objTriples) = self.obj.process(facts)
        self.objRefs = getDNFList(objIds)

        if objIds is not None:
            triples.append((self.id, OBJ_IRI, objIds))

        triples.extend(objTriples)
        return self.id, triples

    @staticmethod
    def fromTree(tree):
        if tree[0] != Action2.treeName or len(tree) != 3:
            raise Exception("Cannot create " + Action2.treeName + " from given tree " + str(tree))

        return Action2(tree[1], Nodes.InstanceNodes.instanceFromTree(tree[2]))


class Action2And:
    treeName = "Action2And"

    def __init__(self, v1, v2, obj):
        self.v1 = v1
        self.v2 = v2
        self.obj = obj
        self.id = useId()
        self.objRefs = []
        self.isPassive = False

    def process(self, facts):
        action2Id = useId()

        triples = [(self.id, "a", ACTION_IRI),
                   (self.id, PRED_IRI, self.v1),
                   (action2Id, "a", ACTION_IRI),
                   (action2Id, PRED_IRI, self.v2)]

        (objIds, objTriples) = self.obj.process(facts)
        self.objRefs = getDNFList(objIds)

        if objIds is not None:
            triples.append((self.id, OBJ_IRI, objIds))
            triples.append((action2Id, OBJ_IRI, objIds))

        triples.extend(objTriples)
        return f"{self.id} & {action2Id}", triples

    @staticmethod
    def fromTree(tree):
        if tree[0] != Action2And.treeName or len(tree) != 4:
            raise Exception("Cannot create " + Action2And.treeName + " from given tree " + str(tree))

        return Action2And(tree[1], tree[2], Nodes.InstanceNodes.instanceFromTree(tree[3]))


class Action2Colon:
    treeName = "Action2Colon"

    @staticmethod
    def fromTree(tree):
        if tree[0] != Action2Colon.treeName or len(tree) != 3:
            raise Exception("Cannot create " + Action2Colon.treeName + " from given tree " + str(tree))

        return Action2(tree[1], Nodes.InstanceNodes.instanceFromTree(tree[2]))


class Action3:
    treeName = "Action3"

    def __init__(self, verb, obj, indirObj):
        self.verb = verb
        self.obj = obj
        self.indirObj = indirObj
        self.id = useId()
        self.objRefs = []
        self.indirObjRefs = []
        self.isPassive = False

    def process(self, facts):
        triples = [(self.id, "a", ACTION_IRI), (self.id, PRED_IRI, self.verb)]

        (objIds, objTriples) = self.obj.process(facts)
        self.objRefs = getDNFList(objIds)

        if objIds is not None:
            triples.append((self.id, OBJ_IRI, objIds))

        (indirObjIds, indirObjTriples) = self.indirObj.process(facts)
        self.indirObjRefs = getDNFList(indirObjIds)

        if indirObjIds is not None:
            triples.append((self.id, INDIR_OBJ_IRI, indirObjIds))

        triples.extend(objTriples)
        triples.extend(indirObjTriples)
        return self.id, triples

    @staticmethod
    def fromTree(tree):
        if tree[0] != Action3.treeName or len(tree) != 4:
            raise Exception("Cannot create " + Action3.treeName + " from given tree " + str(tree))

        return Action3(tree[1], Nodes.InstanceNodes.instanceFromTree(tree[2]), Nodes.InstanceNodes.instanceFromTree(tree[3]))


class ActionSC:
    treeName = "ActionSC"

    def __init__(self, verb, sc):
        self.verb = verb
        self.sc = sc
        self.id = useId()
        self.scRefs = []
        self.isPassive = False

    def process(self, facts):
        triples = [(self.id, "a", ACTION_IRI), (self.id, PRED_IRI, self.verb)]

        (scIds, scTriples) = self.sc.process()
        self.scRefs = getDNFList(scIds)

        if scIds is not None:
            triples.append((self.id, OBJ_IRI, scIds))

        triples.extend(scTriples)
        return self.id, triples

    @staticmethod
    def fromTree(tree):
        if tree[0] != ActionSC.treeName or len(tree) != 3:
            raise Exception("Cannot create " + ActionSC.treeName + " from given tree " + str(tree))

        return ActionSC(tree[1], Nodes.StatementNodes.statementFromTree(tree[2]))


class ObligationSC:
    treeName = "ObligationSC"

    def __init__(self, sc):
        self.sc = sc
        self.id = useId()
        self.scRefs = []
        self.isPassive = False

    def process(self, facts):
        triples = []

        (scIds, scTriples) = self.sc.process(facts)
        self.scRefs = getDNFList(scIds)

        for refs in self.scRefs:
            for ref in refs:
                triples.append((ref, MODAL_IRI, MOD_OBLIGATION_IRI))

        triples.extend(scTriples)
        return scIds, triples

    @staticmethod
    def fromTree(tree):
        if tree[0] != ObligationSC.treeName or len(tree) != 2:
            raise Exception("Cannot create " + ObligationSC.treeName + " from given tree " + str(tree))

        return ObligationSC(Nodes.MiscNodes.scFromTree(tree[1]))


class PermissionSC:
    treeName = "PermissionSC"

    def __init__(self, sc):
        self.sc = sc
        self.id = useId()
        self.scRefs = []
        self.isPassive = False

    def process(self, facts):
        triples = []

        (scIds, scTriples) = self.sc.process(facts)
        self.scRefs = getDNFList(scIds)

        for refs in self.scRefs:
            for ref in refs:
                triples.append((ref, MODAL_IRI, MOD_PERMISSION_IRI))

        triples.extend(scTriples)
        return scIds, triples

    @staticmethod
    def fromTree(tree):
        if tree[0] != PermissionSC.treeName or len(tree) != 2:
            raise Exception("Cannot create " + PermissionSC.treeName + " from given tree " + str(tree))

        return PermissionSC(Nodes.MiscNodes.scFromTree(tree[1]))


class ActionSCWhatFollows:
    treeName = "ActionSCWhatFollows"

    @staticmethod
    def fromTree(tree):
        if tree[0] != ActionSCWhatFollows.treeName or len(tree) != 3:
            raise Exception("Cannot create " + ActionSCWhatFollows.treeName + " from given tree " + str(tree))

        return ActionSC(tree[1], Nodes.StatementNodes.statementFromTree(tree[2]))


class ActionSCColon:
    treeName = "ActionSCColon"

    @staticmethod
    def fromTree(tree):
        if tree[0] != ActionSCColon.treeName or len(tree) != 3:
            raise Exception("Cannot create " + ActionSCColon.treeName + " from given tree " + str(tree))

        return ActionSC(tree[1], Nodes.StatementNodes.statementFromTree(tree[2]))


class ActionPass:
    treeName = "ActionPass"

    def __init__(self, verb):
        self.verb = verb
        self.id = useId()
        self.isPassive = True

    def process(self, facts):
        triples = [(self.id, "a", ACTION_IRI), (self.id, PRED_IRI, self.verb)]

        for f in facts:
            triples.append((f.id, SUBJ_IRI, "?"))
            triples.append((self.id, OBJ_IRI, DNFListToString(f.subjectRefs)))

        return self.id, triples

    @staticmethod
    def fromTree(tree):
        if tree[0] != ActionPass.treeName or len(tree) != 2:
            raise Exception("Cannot create " + ActionPass.treeName + " from given tree " + str(tree))

        return ActionPass(tree[1])


class ActionPassActor:
    treeName = "ActionPassActor"

    def __init__(self, verb, s):
        self.verb = verb
        self.s = s
        self.id = useId()
        self.isPassive = True
        self.sRefs = []

    def process(self, facts):
        triples = [(self.id, "a", ACTION_IRI), (self.id, PRED_IRI, self.verb)]

        (sIds, sTriples) = self.s.process(facts)
        self.sRefs = getDNFList(sIds)

        for f in facts:
            triples.append((f.id, SUBJ_IRI, "?"))
            triples.append((f.id, SUBJ_IRI, sIds))
            triples.append((self.id, OBJ_IRI, DNFListToString(f.subjectRefs)))

        triples.extend(sTriples)
        return self.id, triples

    @staticmethod
    def fromTree(tree):
        if tree[0] != ActionPassActor.treeName or len(tree) != 3:
            raise Exception("Cannot create " + ActionPassActor.treeName + " from given tree " + str(tree))

        return ActionPassActor(tree[1], Nodes.InstanceNodes.instanceFromTree(tree[2]))


class ActionPassSC:
    treeName = "ActionPassSC"

    def __init__(self, verb, sc):
        self.verb = verb
        self.sc = sc
        self.id = useId()
        self.scRefs = []
        self.isPassive = True

    def process(self, facts):
        triples = [(self.id, "a", ACTION_IRI), (self.id, PRED_IRI, self.verb)]

        (scIds, scTriples) = self.sc.process()
        self.scRefs = getDNFList(scIds)

        for f in facts:
            triples.append((f.id, SUBJ_IRI, "?"))
            if scIds is not None:
                triples.append((self.id, OBJ_IRI, scIds))

        triples.extend(scTriples)
        return self.id, triples

    @staticmethod
    def fromTree(tree):
        if tree[0] != ActionPassSC.treeName or len(tree) != 3:
            raise Exception("Cannot create " + ActionPassSC.treeName + " from given tree " + str(tree))

        return ActionPassSC(tree[1], Nodes.StatementNodes.statementFromTree(tree[2]))


class ActionPassV2V:
    treeName = "ActionPassV2V"

    def __init__(self, verb, sc):
        self.verb = verb
        self.sc = sc
        self.id = useId()
        self.scRefs = []
        self.isPassive = True

    def process(self, facts):
        (scIds, scTriples) = self.sc.process(facts)
        self.scRefs = getDNFList(scIds)

        triples = [(self.id, "a", ACTION_IRI), (self.id, PRED_IRI, self.verb), (self.id, OBJ_IRI, scIds)]

        for f in facts:
            triples.append((f.id, SUBJ_IRI, "?"))
            triples.append((self.id, INDIR_OBJ_IRI, DNFListToString(f.subjectRefs)))

        triples.extend(scTriples)
        return self.id, triples

    @staticmethod
    def fromTree(tree):
        if tree[0] != ActionPassV2V.treeName or len(tree) != 3:
            raise Exception("Cannot create " + ActionPassV2V.treeName + " from given tree " + str(tree))

        return ActionPassV2V(tree[1], Nodes.MiscNodes.scFromTree(tree[2]))


class ActionPassSCWhatFollows:
    treeName = "ActionPassSCWhatFollows"

    @staticmethod
    def fromTree(tree):
        if tree[0] != ActionPassSCWhatFollows.treeName or len(tree) != 3:
            raise Exception("Cannot create " + ActionPassSCWhatFollows.treeName + " from given tree " + str(tree))

        return ActionPassSC(tree[1], Nodes.StatementNodes.statementFromTree(tree[2]))


class ActionPassSCColon:
    treeName = "ActionPassSCColon"

    @staticmethod
    def fromTree(tree):
        if tree[0] != ActionPassSCColon.treeName or len(tree) != 3:
            raise Exception("Cannot create " + ActionPassSCColon.treeName + " from given tree " + str(tree))

        return ActionPassSC(tree[1], Nodes.StatementNodes.statementFromTree(tree[2]))


class ObligationPass:
    treeName = "ObligationPass"

    def __init__(self, verb):
        self.verb = verb
        self.id = useId()
        self.isPassive = True

    def process(self, facts):
        triples = [(self.id, "a", ACTION_IRI), (self.id, PRED_IRI, self.verb), (self.id, MODAL_IRI, MOD_OBLIGATION_IRI)]

        for f in facts:
            triples.append((f.id, SUBJ_IRI, "?"))
            triples.append((self.id, OBJ_IRI, DNFListToString(f.subjectRefs)))

        return self.id, triples

    @staticmethod
    def fromTree(tree):
        if tree[0] != ObligationPass.treeName or len(tree) != 2:
            raise Exception("Cannot create " + ObligationPass.treeName + " from given tree " + str(tree))

        return ObligationPass(tree[1])


class ObligationPassSC:
    treeName = "ObligationPassSC"

    def __init__(self, verb, sc):
        self.verb = verb
        self.sc = sc
        self.id = useId()
        self.scRefs = []
        self.isPassive = True

    def process(self, facts):
        triples = [(self.id, "a", ACTION_IRI), (self.id, PRED_IRI, self.verb), (self.id, MODAL_IRI, MOD_OBLIGATION_IRI)]

        (scIds, scTriples) = self.sc.process()
        self.scRefs = getDNFList(scIds)

        for f in facts:
            triples.append((f.id, SUBJ_IRI, "?"))
            if scIds is not None:
                triples.append((self.id, OBJ_IRI, scIds))

        triples.extend(scTriples)
        return self.id, triples

    @staticmethod
    def fromTree(tree):
        if tree[0] != ObligationPassSC.treeName or len(tree) != 3:
            raise Exception("Cannot create " + ObligationPassSC.treeName + " from given tree " + str(tree))

        return ObligationPassSC(tree[1], Nodes.StatementNodes.statementFromTree(tree[2]))


class ActionVV:
    treeName = "ActionVV"

    def __init__(self, verb, a):
        self.verb = verb
        self.a = a
        self.id = useId()
        self.aRefs = []
        self.isPassive = False

    def process(self, facts):
        triples = [(self.id, "a", ACTION_IRI), (self.id, PRED_IRI, self.verb)]

        (aIds, aTriples) = self.a.process(facts)
        self.aRefs = getDNFList(aIds)

        if aIds is not None:
            triples.append((self.id, OBJ_IRI, aIds))

        triples.extend(aTriples)
        return self.id, triples

    @staticmethod
    def fromTree(tree):
        if tree[0] != ActionVV.treeName or len(tree) != 3:
            raise Exception("Cannot create " + ActionVV.treeName + " from given tree " + str(tree))

        return ActionVV(tree[1], actionFromTree(tree[2]))


class ActionV2V:
    treeName = "ActionV2V"

    def __init__(self, verb, i, a):
        self.verb = verb
        self.i = i
        self.a = a
        self.id = useId()
        self.iRefs = []
        self.aRefs = []
        self.isPassive = False

    def process(self, facts):
        (iIds, iTriples) = self.i.process(facts)
        self.iRefs = getDNFList(iIds)

        triples = [(self.id, "a", ACTION_IRI), (self.id, PRED_IRI, self.verb), (self.id, INDIR_OBJ_IRI, iIds)]

        (aIds, aTriples) = self.a.process(facts)
        self.aRefs = getDNFList(aIds)

        if aIds is not None:
            triples.append((self.id, OBJ_IRI, aIds))

        triples.extend(aTriples)
        triples.extend(iTriples)
        return self.id, triples

    @staticmethod
    def fromTree(tree):
        if tree[0] != ActionV2V.treeName or len(tree) != 4:
            raise Exception("Cannot create " + ActionV2V.treeName + " from given tree " + str(tree))

        return ActionV2V(tree[1], Nodes.InstanceNodes.instanceFromTree(tree[2]), actionFromTree(tree[3]))


class PropAction:
    treeName = "PropAction"

    def __init__(self, adj):
        self.adj = adj
        self.actionId = useId()
        self.propId = useId()
        self.isPassive = False

    def process(self, facts):
        (adjS, adjTriples) = self.adj.process(self.actionId, facts)

        triples = [(self.actionId, "a", ACTION_IRI),
                   (self.actionId, PRED_IRI, adjS),
                   (self.propId, "a", PROPERTY_IRI),
                   (self.propId, ADJ_IRI, adjS)]

        for f in facts:
            for subjs in f.subjectRefs:
                for s in subjs:
                    triples.append((s, PROP_IRI, self.propId))

        triples.extend(adjTriples)
        return self.actionId, triples

    @staticmethod
    def fromTree(tree):
        if tree[0] != PropAction.treeName or len(tree) != 2:
            raise Exception("Cannot create " + PropAction.treeName + " from given tree " + str(tree))

        return PropAction(Nodes.MiscNodes.adjFromTree(tree[1]))


class PropAction2:
    treeName = "PropAction2"

    def __init__(self, a, i):
        self.a = a
        self.i = i
        self.actionId = useId()
        self.propId = useId()
        self.iRefs = []
        self.isPassive = False

    def process(self, facts):
        (iIds, iTriples) = self.i.process(facts)
        self.iRefs = getDNFList(iIds)

        triples = [(self.actionId, "a", ACTION_IRI),
                   (self.actionId, PRED_IRI, self.a),
                   (self.actionId, INDIR_OBJ_IRI, iIds),
                   (self.propId, "a", PROPERTY_IRI),
                   (self.propId, ADJ_IRI, self.a)]

        for f in facts:
            for subjs in f.subjectRefs:
                for s in subjs:
                    triples.append((s, PROP_IRI, self.propId))

        triples.extend(iTriples)
        return self.actionId, triples

    @staticmethod
    def fromTree(tree):
        if tree[0] != PropAction2.treeName or len(tree) != 3:
            raise Exception("Cannot create " + PropAction2.treeName + " from given tree " + str(tree))

        return PropAction2(tree[1], Nodes.InstanceNodes.instanceFromTree(tree[2]))


class PropActionAnd:
    treeName = "PropActionAnd"

    def __init__(self, adj1, adj2):
        self.adj1 = adj1
        self.adj2 = adj2
        self.action1Id = useId()
        self.action2Id = useId()
        self.prop1Id = useId()
        self.prop2Id = useId()
        self.isPassive = False

    def process(self, facts):
        (adj1S, adj1Triples) = self.adj1.process(self.action1Id, facts)
        (adj2S, adj2Triples) = self.adj2.process(self.action2Id, facts)

        triples = [(self.action1Id, "a", ACTION_IRI),
                   (self.action1Id, PRED_IRI, adj1S),
                   (self.prop1Id, "a", PROPERTY_IRI),
                   (self.prop1Id, ADJ_IRI, adj1S),
                   (self.action2Id, "a", ACTION_IRI),
                   (self.action2Id, PRED_IRI, adj2S),
                   (self.prop2Id, "a", PROPERTY_IRI),
                   (self.prop2Id, ADJ_IRI, adj2S)]

        for f in facts:
            for subjs in f.subjectRefs:
                for s in subjs:
                    triples.append((s, PROP_IRI, f"({self.prop1Id}) & ({self.prop2Id})"))

        triples.extend(adj1Triples)
        triples.extend(adj2Triples)
        return f"({self.action1Id}) & ({self.action2Id})", triples

    @staticmethod
    def fromTree(tree):
        if tree[0] != PropActionAnd.treeName or len(tree) != 3:
            raise Exception("Cannot create " + PropActionAnd.treeName + " from given tree " + str(tree))

        return PropActionAnd(Nodes.MiscNodes.adjFromTree(tree[1]), Nodes.MiscNodes.adjFromTree(tree[2]))


class PropActionOr:
    treeName = "PropActionOr"

    def __init__(self, adj1, adj2):
        self.adj1 = adj1
        self.adj2 = adj2
        self.action1Id = useId()
        self.action2Id = useId()
        self.prop1Id = useId()
        self.prop2Id = useId()
        self.isPassive = False

    def process(self, facts):
        (adj1S, adj1Triples) = self.adj1.process(self.action1Id, facts)
        (adj2S, adj2Triples) = self.adj2.process(self.action2Id, facts)

        triples = [(self.action1Id, "a", ACTION_IRI),
                   (self.action1Id, PRED_IRI, adj1S),
                   (self.prop1Id, "a", PROPERTY_IRI),
                   (self.prop1Id, ADJ_IRI, adj1S),
                   (self.action2Id, "a", ACTION_IRI),
                   (self.action2Id, PRED_IRI, adj2S),
                   (self.prop2Id, "a", PROPERTY_IRI),
                   (self.prop2Id, ADJ_IRI, adj2S)]

        for f in facts:
            for subjs in f.subjectRefs:
                for s in subjs:
                    triples.append((s, PROP_IRI, f"({self.prop1Id}) | ({self.prop2Id})"))

        triples.extend(adj1Triples)
        triples.extend(adj2Triples)
        return f"({self.action1Id}) | ({self.action2Id})", triples

    @staticmethod
    def fromTree(tree):
        if tree[0] != PropActionOr.treeName or len(tree) != 3:
            raise Exception("Cannot create " + PropActionOr.treeName + " from given tree " + str(tree))

        return PropActionOr(Nodes.MiscNodes.adjFromTree(tree[1]), Nodes.MiscNodes.adjFromTree(tree[2]))


class PropActionZur:
    treeName = "PropActionZur"

    def __init__(self, adj, i):
        self.adj = adj
        self.i = i
        self.actionId = useId()
        self.propId = useId()
        self.iRefs = []
        self.isPassive = False

    def process(self, facts):
        (adjS, adjTriples) = self.adj.process(self.actionId, facts)

        triples = [(self.actionId, "a", ACTION_IRI),
                   (self.actionId, PRED_IRI, adjS),
                   (self.propId, "a", PROPERTY_IRI),
                   (self.propId, ADJ_IRI, adjS)]

        for f in facts:
            for subjs in f.subjectRefs:
                for s in subjs:
                    triples.append((s, PROP_IRI, self.propId))

        (iIds, iTriples) = self.i.process(facts)
        self.iRefs = getDNFList(iIds)

        triples.append((self.actionId, ZU_IRI, iIds))

        triples.extend(iTriples)
        triples.extend(adjTriples)
        return self.actionId, triples

    @staticmethod
    def fromTree(tree):
        if tree[0] != PropActionZur.treeName or len(tree) != 3:
            raise Exception("Cannot create " + PropActionZur.treeName + " from given tree " + str(tree))

        return PropActionZur(Nodes.MiscNodes.adjFromTree(tree[1]), Nodes.InstanceNodes.instanceFromTree(tree[2]))


class PropCompareAction:
    treeName = "PropCompareAction"

    def __init__(self, adj, obj):
        self.adj = adj
        self.obj = obj
        self.actionId = useId()
        self.propId = useId()
        self.objRefs = []
        self.isPassive = False

    def process(self, facts):
        (adjS, adjTriples) = self.adj.process(self.actionId, facts)

        triples = [(self.actionId, "a", ACTION_IRI),
                   (self.actionId, PRED_IRI, adjS),
                   (self.propId, "a", PROPERTY_IRI),
                   (self.propId, ADJ_IRI, adjS)]

        for f in facts:
            for subjs in f.subjectRefs:
                for s in subjs:
                    triples.append((s, PROP_IRI, self.propId))

        (objIds, objTriples) = self.obj.process(facts)
        self.objRefs = getDNFList(objIds)

        if objIds is not None:
            triples.append((self.propId, COMP_IRI, objIds))

        triples.extend(objTriples)
        triples.extend(adjTriples)
        return self.actionId, triples

    @staticmethod
    def fromTree(tree):
        if tree[0] != PropCompareAction.treeName or len(tree) != 3:
            raise Exception("Cannot create " + PropCompareAction.treeName + " from given tree " + str(tree))

        return PropCompareAction(Nodes.MiscNodes.adjFromTree(tree[1]), Nodes.InstanceNodes.instanceFromTree(tree[2]))


class AdvAction:
    treeName = "AdvAction"

    def __init__(self, adv):
        self.adv = adv
        self.id = useId()
        self.advRefs = []
        self.isPassive = False

    def process(self, facts):
        triples = [(self.id, "a", PROPERTY_IRI)]

        for f in facts:
            for subjs in f.subjectRefs:
                for s in subjs:
                    triples.append((s, PROP_IRI, self.id))

        (advIds, advTriples) = self.adv.process([self.id], facts)
        self.advRefs = getDNFList(advIds)

        if advIds is not None:
            triples.append((self.id, ADV_IRI, advIds))

        triples.extend(advTriples)
        return None, triples

    @staticmethod
    def fromTree(tree):
        if tree[0] != AdvAction.treeName or len(tree) != 2:
            raise Exception("Cannot create " + AdvAction.treeName + " from given tree " + str(tree))

        return AdvAction(Nodes.MiscNodes.advFromTree(tree[1]))


class AdvActionAnd:
    treeName = "AdvActionAnd"

    def __init__(self, adv1, adv2):
        self.adv1 = adv1
        self.adv2 = adv2
        self.id = useId()
        self.adv1Refs = []
        self.adv2Refs = []
        self.isPassive = False

    def process(self, facts):
        triples = [(self.id, "a", PROPERTY_IRI)]

        for f in facts:
            for subjs in f.subjectRefs:
                for s in subjs:
                    triples.append((s, PROP_IRI, self.id))

        (adv1Ids, adv1Triples) = self.adv1.process(self.id, facts)
        self.adv1Refs = getDNFList(adv1Ids)

        (adv2Ids, adv2Triples) = self.adv2.process(self.id, facts)
        self.adv2Refs = getDNFList(adv2Ids)

        if adv1Ids is not None and adv2Ids is not None:
            triples.append((self.id, ADV_IRI, f"({adv1Ids}) & ({adv2Ids})"))
        elif adv1Ids is not None:
            triples.append((self.id, ADV_IRI, adv1Ids))
        else:
            triples.append((self.id, ADV_IRI, adv2Ids))

        triples.extend(adv1Triples)
        triples.extend(adv2Triples)
        return None, triples

    @staticmethod
    def fromTree(tree):
        if tree[0] != AdvActionAnd.treeName or len(tree) != 3:
            raise Exception("Cannot create " + AdvActionAnd.treeName + " from given tree " + str(tree))

        return AdvActionAnd(Nodes.MiscNodes.advFromTree(tree[1]), Nodes.MiscNodes.advFromTree(tree[2]))


class AdvActionOr:
    treeName = "AdvActionOr"

    def __init__(self, adv1, adv2):
        self.adv1 = adv1
        self.adv2 = adv2
        self.id = useId()
        self.adv1Refs = []
        self.adv2Refs = []
        self.isPassive = False

    def process(self, facts):
        triples = [(self.id, "a", PROPERTY_IRI)]

        for f in facts:
            for subjs in f.subjectRefs:
                for s in subjs:
                    triples.append((s, PROP_IRI, self.id))

        (adv1Ids, adv1Triples) = self.adv1.process(self.id, facts)
        self.adv1Refs = getDNFList(adv1Ids)

        (adv2Ids, adv2Triples) = self.adv2.process(self.id, facts)
        self.adv2Refs = getDNFList(adv2Ids)

        if adv1Ids is not None and adv2Ids is not None:
            triples.append((self.id, ADV_IRI, f"({adv1Ids}) | ({adv2Ids})"))
        elif adv1Ids is not None:
            triples.append((self.id, ADV_IRI, adv1Ids))
        else:
            triples.append((self.id, ADV_IRI, adv2Ids))

        triples.extend(adv1Triples)
        triples.extend(adv2Triples)
        return None, triples

    @staticmethod
    def fromTree(tree):
        if tree[0] != AdvActionOr.treeName or len(tree) != 3:
            raise Exception("Cannot create " + AdvActionOr.treeName + " from given tree " + str(tree))

        return AdvActionOr(Nodes.MiscNodes.advFromTree(tree[1]), Nodes.MiscNodes.advFromTree(tree[2]))


class Is:
    treeName = "Is"

    def __init__(self, obj):
        self.obj = obj
        self.id = useId()
        self.objRefs = []
        self.isPassive = False

    def process(self, facts):
        triples = [(self.id, "a", ACTION_IRI), (self.id, PRED_IRI, BE_IRI)]

        (objIds, objTriples) = self.obj.process(facts)
        self.objRefs = getDNFList(objIds)

        if objIds is not None:
            triples.append((self.id, OBJ_IRI, objIds))

        triples.extend(objTriples)
        return self.id, triples

    @staticmethod
    def fromTree(tree):
        if tree[0] != Is.treeName or len(tree) != 2:
            raise Exception("Cannot create " + Is.treeName + " from given tree " + str(tree))

        return Is(Nodes.InstanceNodes.instanceFromTree(tree[1]))


class Own:
    treeName = "Own"

    def __init__(self, obj):
        self.obj = obj
        self.id = useId()
        self.objRefs = []
        self.isPassive = False

    def process(self, facts):
        triples = [(self.id, "a", ACTION_IRI), (self.id, PRED_IRI, "own")]

        (objIds, objTriples) = self.obj.process(facts)
        self.objRefs = getDNFList(objIds)

        for f in facts:
            if objIds is not None:
                triples.append((self.id, OBJ_IRI, objIds))

            for clause in self.objRefs:
                for c in clause:
                    for ref in f.subjectRefs:
                        triples.append((c, OWNER_IRI, " & ".join(ref)))

        triples.extend(objTriples)
        return self.id, triples

    @staticmethod
    def fromTree(tree):
        if tree[0] != Own.treeName or len(tree) != 2:
            raise Exception("Cannot create " + Own.treeName + " from given tree " + str(tree))

        return Own(Nodes.InstanceNodes.instanceFromTree(tree[1]))


class LocationAction:
    treeName = "LocationAction"

    def __init__(self, loc):
        self.loc = loc
        self.id = useId()
        self.locRefs = []
        self.isPassive = False

    def process(self, facts):
        triples = []

        (locIds, locTriples) = self.loc.process(facts)
        self.locRefs = getDNFList(locIds)

        if locIds is not None:
            for f in facts:
                for subjs in f.subjectRefs:
                    for s in subjs:
                        triples.append((s, LOC_IRI, locIds))

        triples.extend(locTriples)
        return None, triples

    @staticmethod
    def fromTree(tree):
        if tree[0] != LocationAction.treeName or len(tree) != 2:
            raise Exception("Cannot create " + LocationAction.treeName + " from given tree " + str(tree))

        return LocationAction(Nodes.InstanceNodes.instanceFromTree(tree[1]))


class Mod:
    treeName = "Mod"

    def __init__(self, action, adv):
        self.action = action
        self.adv = adv
        self.id = useId()
        self.actionRefs = []
        self.advRefs = []
        self.isPassive = action.isPassive

    def process(self, facts):
        triples = []

        (actionIds, actionTriples) = self.action.process(facts)
        self.actionRefs = getDNFList(actionIds)

        (advIds, advTriples) = self.adv.process([a for cl in self.actionRefs for a in cl], facts)
        self.advRefs = getDNFList(advIds)

        if advIds is not None:
            for ref in self.actionRefs:
                for a in ref:
                    triples.append((a, MOD_IRI, advIds))

        triples.extend(actionTriples)
        triples.extend(advTriples)
        return actionIds, triples

    @staticmethod
    def fromTree(tree):
        if tree[0] != Mod.treeName or len(tree) != 3:
            raise Exception("Cannot create " + Mod.treeName + " from given tree " + str(tree))

        return Mod(actionFromTree(tree[1]), Nodes.MiscNodes.advFromTree(tree[2]))


class ModA:
    treeName = "ModA"

    def __init__(self, action, adj):
        self.action = action
        self.adj = adj
        self.id = useId()
        self.actionRefs = []
        self.isPassive = action.isPassive

    def process(self, facts):
        triples = []

        (actionIds, actionTriples) = self.action.process(facts)
        self.actionRefs = getDNFList(actionIds)

        for ref in self.actionRefs:
            for a in ref:
                (adjS, adjTriples) = self.adj.process(a, facts)
                triples.append((a, MOD_A_IRI, adjS))
                triples.extend(adjTriples)

        triples.extend(actionTriples)
        return actionIds, triples

    @staticmethod
    def fromTree(tree):
        if tree[0] != ModA.treeName or len(tree) != 3:
            raise Exception("Cannot create " + ModA.treeName + " from given tree " + str(tree))

        return ModA(actionFromTree(tree[1]), Nodes.MiscNodes.adjFromTree(tree[2]))


class ModPart:
    treeName = "ModPart"

    def __init__(self, action, i):
        self.action = action
        self.i = i
        self.id = useId()
        self.actionRefs = []
        self.iRefs = []
        self.isPassive = action.isPassive

    def process(self, facts):
        triples = []

        (actionIds, actionTriples) = self.action.process(facts)
        self.actionRefs = getDNFList(actionIds)

        (iIds, iTriples) = self.i.process(facts)
        self.iRefs = getDNFList(iIds)

        for ref in self.actionRefs:
            for a in ref:
                triples.append((a, PART_IRI, iIds))

        triples.extend(actionTriples)
        triples.extend(iTriples)
        return actionIds, triples

    @staticmethod
    def fromTree(tree):
        if tree[0] != ModPart.treeName or len(tree) != 3:
            raise Exception("Cannot create " + ModPart.treeName + " from given tree " + str(tree))

        return ModPart(actionFromTree(tree[1]), Nodes.InstanceNodes.instanceFromTree(tree[2]))


class ModAs:
    treeName = "ModAs"

    def __init__(self, action, i):
        self.action = action
        self.i = i
        self.id = useId()
        self.actionRefs = []
        self.iRefs = []
        self.isPassive = action.isPassive

    def process(self, facts):
        triples = []

        (actionIds, actionTriples) = self.action.process(facts)
        self.actionRefs = getDNFList(actionIds)

        (iIds, iTriples) = self.i.process()
        self.iRefs = getDNFList(iIds)

        beActId = useId()
        triples.extend([(beActId, "a", ACTION_IRI), (beActId, PRED_IRI, BE_IRI), (beActId, OBJ_IRI, iIds)])

        for f in facts:
            beFactIds = []
            for sRefs in f.subjectRefs:
                for sRef in sRefs:
                    beFactId = useId()
                    triples.extend([(beFactId, "a", FACT_IRI),
                                    (beFactId, SUBJ_IRI, sRef),
                                    (beFactId, ACT_IRI, beActId)])
                    beFactIds.append(beFactId)

            triples.append((f.id, REASON_IRI, " & ".join(list(map(lambda fIds: f"({fIds})", beFactIds)))))

        triples.extend(actionTriples)
        triples.extend(iTriples)
        return actionIds, triples

    @staticmethod
    def fromTree(tree):
        if tree[0] != ModAs.treeName or len(tree) != 3:
            raise Exception("Cannot create " + ModAs.treeName + " from given tree " + str(tree))

        return ModAs(actionFromTree(tree[1]), Nodes.InstanceNodes.instanceFromTree(tree[2]))


class ModWhenExists:
    treeName = "ModWhenExists"

    def __init__(self, action, i, iTree):
        self.action = action
        self.i = i
        self.id = useId()
        self.actionRefs = []
        self.iRefs = []
        self.iTree = iTree
        self.isPassive = action.isPassive

    def process(self, facts):
        triples = []

        (actionIds, actionTriples) = self.action.process(facts)
        self.actionRefs = getDNFList(actionIds)

        (iIds, iTriples) = self.i.process()
        self.iRefs = getDNFList(iIds)

        existsActId = useId()
        triples.extend([(existsActId, "a", ACTION_IRI), (existsActId, PRED_IRI, EXIST_IRI)])

        for f in facts:
            existsFactId = useId()
            triples.extend([(existsFactId, "a", FACT_IRI),
                            (existsFactId, SUBJ_IRI, iIds),
                            (existsFactId, ACT_IRI, existsActId),
                            (existsFactId, SYNTREE_IRI, f"StatementPos TPres ASimul (Fact ({self.iTree}) (PropAction vorhanden_A))"),
                            (f.id, COND_IRI, existsFactId)])

        triples.extend(actionTriples)
        triples.extend(iTriples)
        return actionIds, triples

    @staticmethod
    def fromTree(tree):
        if tree[0] != ModWhenExists.treeName or len(tree) != 3:
            raise Exception("Cannot create " + ModWhenExists.treeName + " from given tree " + str(tree))

        return ModWhenExists(actionFromTree(tree[1]), Nodes.InstanceNodes.instanceFromTree(tree[2]), treeToString(tree[2]))


class ModWhenExistsLeft:
    treeName = "ModWhenExistsLeft"

    @staticmethod
    def fromTree(tree):
        if tree[0] != ModWhenExistsLeft.treeName or len(tree) != 3:
            raise Exception("Cannot create " + ModWhenExistsLeft.treeName + " from given tree " + str(tree))

        return ModWhenExists(actionFromTree(tree[1]), Nodes.InstanceNodes.instanceFromTree(tree[2]), treeToString(tree[2]))


class LocationMod:
    treeName = "LocationMod"

    def __init__(self, action, loc):
        self.action = action
        self.loc = loc
        self.id = useId()
        self.actionRefs = []
        self.locRefs = []
        self.isPassive = action.isPassive

    def process(self, facts):
        triples = []

        (actionIds, actionTriples) = self.action.process(facts)
        self.actionRefs = getDNFList(actionIds)

        (locIds, locTriples) = self.loc.process()
        self.locRefs = getDNFList(locIds)

        if locIds is not None:
            for ref in self.actionRefs:
                for a in ref:
                    triples.append((a, LOC_IRI, locIds))

        triples.extend(actionTriples)
        triples.extend(locTriples)
        return actionIds, triples

    @staticmethod
    def fromTree(tree):
        if tree[0] != LocationMod.treeName or len(tree) != 3:
            raise Exception("Cannot create " + LocationMod.treeName + " from given tree " + str(tree))

        return LocationMod(actionFromTree(tree[1]), Nodes.InstanceNodes.instanceFromTree(tree[2]))


class ModReferring:
    treeName = "ModReferring"

    def __init__(self, action, i):
        self.action = action
        self.i = i
        self.id = useId()
        self.actionRefs = []
        self.iRefs = []
        self.isPassive = action.isPassive

    def process(self, facts):
        triples = []

        (actionIds, actionTriples) = self.action.process(facts)
        self.actionRefs = getDNFList(actionIds)

        (iIds, iTriples) = self.i.process()
        self.iRefs = getDNFList(iIds)

        if iIds is not None:
            for ref in self.actionRefs:
                for a in ref:
                    triples.append((a, REGARDING_IRI, iIds))

        triples.extend(actionTriples)
        triples.extend(iTriples)
        return actionIds, triples

    @staticmethod
    def fromTree(tree):
        if tree[0] != ModReferring.treeName or len(tree) != 3:
            raise Exception("Cannot create " + ModReferring.treeName + " from given tree " + str(tree))

        return ModReferring(actionFromTree(tree[1]), Nodes.InstanceNodes.instanceFromTree(tree[2]))


class ModTarget:
    treeName = "ModTarget"

    def __init__(self, action, i):
        self.action = action
        self.i = i
        self.id = useId()
        self.actionRefs = []
        self.iRefs = []
        self.isPassive = action.isPassive

    def process(self, facts):
        triples = []

        (actionIds, actionTriples) = self.action.process(facts)
        self.actionRefs = getDNFList(actionIds)

        (iIds, iTriples) = self.i.process()
        self.iRefs = getDNFList(iIds)

        if iIds is not None:
            for ref in self.actionRefs:
                for a in ref:
                    triples.append((a, TARGET_IRI, iIds))

        triples.extend(actionTriples)
        triples.extend(iTriples)
        return actionIds, triples

    @staticmethod
    def fromTree(tree):
        if tree[0] != ModTarget.treeName or len(tree) != 3:
            raise Exception("Cannot create " + ModTarget.treeName + " from given tree " + str(tree))

        return ModTarget(actionFromTree(tree[1]), Nodes.InstanceNodes.instanceFromTree(tree[2]))


class ModDate:
    treeName = "ModDate"

    def __init__(self, action, d, m, y):
        self.action = action
        self.d = d
        self.m = m
        self.y = y
        self.id = useId()
        self.actionRefs = []
        self.isPassive = action.isPassive

    def process(self, facts):
        dateId = useId()
        triples = [(dateId, "a", DATE_IRI),
                   (dateId, DATE_DAY_IRI, self.d),
                   (dateId, DATE_MONTH_IRI, self.m),
                   (dateId, DATE_YEAR_IRI, self.y)]

        (actionIds, actionTriples) = self.action.process(facts)
        self.actionRefs = getDNFList(actionIds)

        for ref in self.actionRefs:
            for a in ref:
                triples.append((a, DATE_PROP_IRI, dateId))

        triples.extend(actionTriples)
        return actionIds, triples

    @staticmethod
    def fromTree(tree):
        if tree[0] != ModDate.treeName or len(tree) != 5:
            raise Exception("Cannot create " + ModDate.treeName + " from given tree " + str(tree))

        return ModDate(actionFromTree(tree[1]), tree[2], tree[3], tree[4])


class ModDateLeft:
    treeName = "ModDateLeft"

    @staticmethod
    def fromTree(tree):
        if tree[0] != ModDateLeft.treeName or len(tree) != 5:
            raise Exception("Cannot create " + ModDateLeft.treeName + " from given tree " + str(tree))

        return ModDate(actionFromTree(tree[1]), tree[2], tree[3], tree[4])


class ModDurch:
    treeName = "ModDurch"

    def __init__(self, action, i):
        self.action = action
        self.i = i
        self.id = useId()
        self.actionRefs = []
        self.isPassive = action.isPassive

    def process(self, facts):
        triples = []

        (actionIds, actionTriples) = self.action.process(facts)
        self.actionRefs = getDNFList(actionIds)

        (iIds, iTriples) = self.i.process()
        self.iRefs = getDNFList(iIds)


        if iIds is not None:
            if self.isPassive:
                for f in facts:
                    triples.append((f.id, SUBJ_IRI, iIds))
            else:
                for ref in self.actionRefs:
                    for a in ref:
                        triples.append((a, DURCH_IRI, iIds))

        triples.extend(actionTriples)
        triples.extend(iTriples)
        return actionIds, triples

    @staticmethod
    def fromTree(tree):
        if tree[0] != ModDurch.treeName or len(tree) != 3:
            raise Exception("Cannot create " + ModDurch.treeName + " from given tree " + str(tree))

        return ModDurch(actionFromTree(tree[1]), Nodes.InstanceNodes.instanceFromTree(tree[2]))


class ModMit:
    treeName = "ModMit"

    def __init__(self, action, i):
        self.action = action
        self.i = i
        self.id = useId()
        self.actionRefs = []
        self.isPassive = action.isPassive

    def process(self, facts):
        triples = []

        (actionIds, actionTriples) = self.action.process(facts)
        self.actionRefs = getDNFList(actionIds)

        (iIds, iTriples) = self.i.process()
        self.iRefs = getDNFList(iIds)


        if iIds is not None:
            for ref in self.actionRefs:
                for a in ref:
                    triples.append((a, MIT_IRI, iIds))

        triples.extend(actionTriples)
        triples.extend(iTriples)
        return actionIds, triples

    @staticmethod
    def fromTree(tree):
        if tree[0] != ModMit.treeName or len(tree) != 3:
            raise Exception("Cannot create " + ModMit.treeName + " from given tree " + str(tree))

        return ModMit(actionFromTree(tree[1]), Nodes.InstanceNodes.instanceFromTree(tree[2]))


class ModUnterVerzicht:
    treeName = "ModUnterVerzicht"

    def __init__(self, action, i):
        self.action = action
        self.i = i
        self.id = useId()
        self.actionRefs = []
        self.iRefs = []
        self.isPassive = action.isPassive

    def process(self, facts):
        triples = []

        (actionIds, actionTriples) = self.action.process(facts)
        self.actionRefs = getDNFList(actionIds)

        (iIds, iTriples) = self.i.process()
        self.iRefs = getDNFList(iIds)

        waivingActId = useId()
        triples.extend([(waivingActId, "a", ACTION_IRI), (waivingActId, PRED_IRI, "verzichten_V2"), (waivingActId, OBJ_IRI, iIds)])

        triples.extend(actionTriples)
        triples.extend(iTriples)
        return f"({actionIds}) & ({waivingActId})", triples

    @staticmethod
    def fromTree(tree):
        if tree[0] != ModUnterVerzicht.treeName or len(tree) != 3:
            raise Exception("Cannot create " + ModUnterVerzicht.treeName + " from given tree " + str(tree))

        return ModUnterVerzicht(actionFromTree(tree[1]), Nodes.InstanceNodes.instanceFromTree(tree[2]))


class Possibility:
    treeName = "Possibility"

    def __init__(self, action):
        self.action = action
        self.id = useId()
        self.actionRefs = []
        self.isPassive = action.isPassive

    def process(self, facts):
        triples = []

        (actionIds, actionTriples) = self.action.process(facts)
        self.actionRefs = getDNFList(actionIds)

        for ref in self.actionRefs:
            for a in ref:
                triples.append((a, MODAL_IRI, MOD_POSSIBILITY_IRI))

        triples.extend(actionTriples)
        return actionIds, triples

    @staticmethod
    def fromTree(tree):
        if tree[0] != Possibility.treeName or len(tree) != 2:
            raise Exception("Cannot create " + Possibility.treeName + " from given tree " + str(tree))

        return Possibility(actionFromTree(tree[1]))


class Obligation:
    treeName = "Obligation"

    def __init__(self, action):
        self.action = action
        self.id = useId()
        self.actionRefs = []
        self.isPassive = action.isPassive

    def process(self, facts):
        triples = []

        (actionIds, actionTriples) = self.action.process(facts)
        self.actionRefs = getDNFList(actionIds)

        for ref in self.actionRefs:
            for a in ref:
                triples.append((a, MODAL_IRI, MOD_OBLIGATION_IRI))

        triples.extend(actionTriples)
        return actionIds, triples

    @staticmethod
    def fromTree(tree):
        if tree[0] != Obligation.treeName or len(tree) != 2:
            raise Exception("Cannot create " + Obligation.treeName + " from given tree " + str(tree))

        return Obligation(actionFromTree(tree[1]))


class Permission:
    treeName = "Permission"

    def __init__(self, action):
        self.action = action
        self.id = useId()
        self.actionRefs = []
        self.isPassive = action.isPassive

    def process(self, facts):
        triples = []

        (actionIds, actionTriples) = self.action.process(facts)
        self.actionRefs = getDNFList(actionIds)

        for ref in self.actionRefs:
            for a in ref:
                triples.append((a, MODAL_IRI, MOD_PERMISSION_IRI))

        triples.extend(actionTriples)
        return actionIds, triples

    @staticmethod
    def fromTree(tree):
        if tree[0] != Permission.treeName or len(tree) != 2:
            raise Exception("Cannot create " + Permission.treeName + " from given tree " + str(tree))

        return Permission(actionFromTree(tree[1]))


class Desire:
    treeName = "Desire"

    def __init__(self, action):
        self.action = action
        self.id = useId()
        self.actionRefs = []
        self.isPassive = action.isPassive

    def process(self, facts):
        triples = []

        (actionIds, actionTriples) = self.action.process(facts)
        self.actionRefs = getDNFList(actionIds)

        for ref in self.actionRefs:
            for a in ref:
                triples.append((a, MODAL_IRI, MOD_DESIRE_IRI))

        triples.extend(actionTriples)
        return actionIds, triples

    @staticmethod
    def fromTree(tree):
        if tree[0] != Desire.treeName or len(tree) != 2:
            raise Exception("Cannot create " + Desire.treeName + " from given tree " + str(tree))

        return Desire(actionFromTree(tree[1]))
