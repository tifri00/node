from Utility import getDNFList, useId
from constants import *

class KnowledgeChunk:
    def __init__(self, statement):
        self.statement = statement
        self.id = useId()
        self.statementRefs = []

    def process(self):
        (statementIds, triples) = self.statement.process()

        self.statementRefs = getDNFList(statementIds)

        resultTriples = triples.copy()

        resultTriples.append((self.id, "a", KNOWLEDGE_CHUNK_IRI))

        resultTriples.append((self.id, FACTS_IRI, statementIds))

        return None, resultTriples
