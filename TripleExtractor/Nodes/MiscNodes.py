from Utility import useId, getDNFList, numeralToInt, treeToString
from constants import *
import Nodes.InstanceNodes
import Nodes.ActionNodes
import Nodes.ConceptNodes
import Nodes.StatementNodes

nodes = []

def init(nodeList):
    global nodes
    nodes = nodeList


def percentageFromTree(tree):
    if tree[0] in ["AtLeastP2"]:
        tree[0] = "AtLeastP"

    percentageNodes = list(filter(lambda x: x.treeName == tree[0] and x.treeName in ["Percentage", "AtLeastP", "AtMostP", "AlmostP"], nodes))

    if percentageNodes is None or len(percentageNodes) == 0:
        raise Exception("Cannot construct percentage from given tree " + str(tree))

    return percentageNodes[0].fromTree(tree)


def advFromTree(tree):
    if not isinstance(tree, list):
        return Adverb.fromTree(tree)

    if tree[0] in ["ReferringAdv2", "ReferringAdv3", "ReferringAdv4", "ReferringAdv5", "ReferringAdv6"]:
        tree[0] = "ReferringAdv"
    elif tree[0] in ["LocationAdv2", "LocationAdv3", "LocationAdv4", "LocationAdv5", "LocationAdv6",
                     "LocationAdv7", "LocationAdv8"]:
        tree[0] = "LocationAdv"
    elif tree[0] in ["TargetAdv2", "TargetAdv3"]:
        tree[0] = "TargetAdv"
    elif tree[0] in ["DateAdv2"]:
        tree[0] = "DateAdv"
    elif tree[0] in ["DateAdv4"]:
        tree[0] = "DateAdv3"
    elif tree[0] in ["MitAdv2"]:
        tree[0] = "MitAdv"
    elif tree[0] in ["AccordingToAdv2", "AccordingToAdv3", "AccordingToAdv4", "AccordingToAdv5", "AccordingToAdv6"]:
        tree[0] = "AccordingToAdv"
    elif tree[0] in ["AdverbialInsertion2", "AdverbialInsertion3", "AdverbialInsertion4", "AdverbialInsertion5", "AdverbialInsertion6"]:
        tree[0] = "AdverbialInsertion"
    elif tree[0] in ["AdverbialInsertionNeg2", "AdverbialInsertionNeg3", "AdverbialInsertionNeg4"]:
        tree[0] = "AdverbialInsertionNeg"

    advNodes = list(filter(lambda x: x.treeName == tree[0] and x.treeName in ["MakeCAdvP", "AdvAnd", "AdvOr", "AdvNeitherNor", "PositAdvAdj", "ZurAdv", "AsAdv", "WhenExistsAdv", "ReferringAdv", "LocationAdv", "PartAdv", "TargetAdv", "DateAdv", "DateAdv3", "DurchAdv", "MitAdv", "UnterVerzichtAdv", "OhneAdv", "ForAdv", "DisregardingAdv", "OriginAdv", "AccordingToAdv", "DueToAdv", "BesidesAdv", "WithEffectToAdv", "AccordingToLawSectionAdv", "AdverbialInsertion", "AdverbialInsertionNeg", "ThatAdv"], nodes))

    if advNodes is None or len(advNodes) == 0:
        raise Exception("Cannot construct adv from given tree " + str(tree))

    return advNodes[0].fromTree(tree)


def adjFromTree(tree):
    if not isinstance(tree, list):
        return Adjective(tree)

    adjNodes = list(filter(lambda x: x.treeName == tree[0] and x.treeName in ["ModLeftA", "ModLeftAA", "ModRightA", "ModRightAA", "AdjAnd"], nodes))

    if adjNodes is None or len(adjNodes) == 0:
        raise Exception("Cannot construct adj from given tree " + str(tree))

    return adjNodes[0].fromTree(tree)


def scFromTree(tree):
    if tree[0] in ["SCAnd2", "SCAnd3"]:
        tree[0] = "SCAnd"

    scNodes = list(filter(lambda x: x.treeName == tree[0] and x.treeName in ["SCFromAct", "SCAnd"], nodes))

    if scNodes is None or len(scNodes) == 0:
        raise Exception("Cannot construct sc from given tree " + str(tree))

    return scNodes[0].fromTree(tree)


def cardToInt(card):
    if card == "mehrere_Card":
        return "> 1"
    elif card[0] == "NumNumeral":
        return numeralToInt(card[1])
    elif card[0] == "IntCard":
        return card[1]

    return None


def intFromCardTree(tree):
    if tree[0] in ["AtLeast2"]:
        tree[0] = "AtLeast"

    cardNodes = list(filter(lambda x: x.treeName == tree[0] and x.treeName in ["AtLeast", "AtMost", "Almost", "CardOr"], nodes))

    if cardNodes is None or len(cardNodes) == 0:
        res = cardToInt(tree)
        if res is None:
            raise Exception("Cannot construct card from given tree " + str(tree))

        return res

    return cardNodes[0].fromTree(tree).process()


class Adverb:
    treeName = "Adv"

    def __init__(self, adv):
        self.adv = adv
        self.id = useId()

    def process(self, actions=None, facts=None):
        return self.id, [(self.id, "a", ADVERB_IRI), (self.id, ADV_S_IRI, self.adv)]

    @staticmethod
    def fromTree(tree):
        if isinstance(tree, list):
            raise Exception("Cannot create " + Adverb.treeName + " from given tree " + str(tree))

        return Adverb(tree)


class MakeCAdvP:
    treeName = "MakeCAdvP"

    def __init__(self, adv):
        self.adv = adv
        self.id = useId()

    def process(self, actions=None, facts=None):
        return self.id, [(self.id, "a", ADVERB_IRI), (self.id, ADV_S_IRI, self.adv)]

    @staticmethod
    def fromTree(tree):
        if tree[0] != MakeCAdvP.treeName or len(tree) != 2:
            raise Exception("Cannot create " + MakeCAdvP.treeName + " from given tree " + str(tree))

        return Adverb(tree[1])


class AdvAnd:
    treeName = "AdvAnd"

    def __init__(self, adv1, adv2):
        self.adv1 = adv1
        self.adv2 = adv2
        self.id = useId()

    def process(self, actions=None, facts=None):
        (adv1Ids, adv1Triples) = self.adv1.process(actions, facts)
        (adv2Ids, adv2Triples) = self.adv2.process(actions, facts)

        triples = []
        triples.extend(adv1Triples)
        triples.extend(adv2Triples)

        if adv1Ids is not None and adv2Ids is not None:
            return "(" + adv1Ids + ") & (" + adv2Ids + ")", triples
        elif adv1Ids is not None:
            return "(" + adv1Ids + ")", triples
        else:
            return "(" + adv2Ids + ")", triples

    @staticmethod
    def fromTree(tree):
        if tree[0] != AdvAnd.treeName or len(tree) != 3:
            raise Exception("Cannot create " + AdvAnd.treeName + " from given tree " + str(tree))

        return AdvAnd(advFromTree(tree[1]), advFromTree(tree[2]))


class AdvOr:
    treeName = "AdvOr"

    def __init__(self, adv1, adv2):
        self.adv1 = adv1
        self.adv2 = adv2
        self.id = useId()

    def process(self, actions=None, facts=None):
        (adv1Ids, adv1Triples) = self.adv1.process(actions, facts)
        (adv2Ids, adv2Triples) = self.adv2.process(actions, facts)

        triples = []
        triples.extend(adv1Triples)
        triples.extend(adv2Triples)

        if adv1Ids is not None and adv2Ids is not None:
            return "(" + adv1Ids + ") | (" + adv2Ids + ")", triples
        elif adv1Ids is not None:
            return "(" + adv1Ids + ")", triples
        else:
            return "(" + adv2Ids + ")", triples

    @staticmethod
    def fromTree(tree):
        if tree[0] != AdvOr.treeName or len(tree) != 3:
            raise Exception("Cannot create " + AdvOr.treeName + " from given tree " + str(tree))

        return AdvOr(advFromTree(tree[1]), advFromTree(tree[2]))


class AdvNeitherNor:
    treeName = "AdvNeitherNor"

    def __init__(self, adv1, adv2):
        self.adv1 = adv1
        self.adv2 = adv2
        self.id = useId()

    def process(self, actions=None, facts=None):
        (adv1Ids, adv1Triples) = self.adv1.process(actions, facts)
        (adv2Ids, adv2Triples) = self.adv2.process(actions, facts)

        triples = []
        triples.extend(adv1Triples)
        triples.extend(adv2Triples)

        return None, [] # TODO

    @staticmethod
    def fromTree(tree):
        if tree[0] != AdvNeitherNor.treeName or len(tree) != 3:
            raise Exception("Cannot create " + AdvNeitherNor.treeName + " from given tree " + str(tree))

        return AdvNeitherNor(advFromTree(tree[1]), advFromTree(tree[2]))


class PositAdvAdj:
    treeName = "PositAdvAdj"

    @staticmethod
    def fromTree(tree, actions=None, facts=None):
        if tree[0] != PositAdvAdj.treeName or len(tree) != 2:
            raise Exception("Cannot create " + PositAdvAdj.treeName + " from given tree " + str(tree))

        return Adverb(tree[1])


class Percentage:
    treeName = "Percentage"

    def __init__(self, n):
        self.n = n
        self.id = useId()

    def process(self):
        triples = [(self.id, "a", QUANTIFIER_IRI), (self.id, NUM_IRI, self.n + "%")]

        return self.id, triples

    @staticmethod
    def fromTree(tree):
        if tree[0] != Percentage.treeName or len(tree) != 2:
            raise Exception("Cannot create " + Percentage.treeName + " from given tree " + str(tree))

        return Percentage(tree[1])


class AtLeastP:
    treeName = "AtLeastP"

    def __init__(self, p):
        self.p = p
        self.id = useId()
        self.pRefs = []

    def process(self):
        triples = []

        (pIds, pTriples) = self.p.process()
        self.pRefs = getDNFList(pIds)

        for refs in self.pRefs:
            for ref in refs:
                triples.append((ref, OP_IRI, ">="))

        triples.extend(pTriples)
        return pIds, triples

    @staticmethod
    def fromTree(tree):
        if tree[0] != AtLeastP.treeName or len(tree) != 2:
            raise Exception("Cannot create " + AtLeastP.treeName + " from given tree " + str(tree))

        return AtLeastP(percentageFromTree(tree[1]))


class AtMostP:
    treeName = "AtMostP"

    def __init__(self, p):
        self.p = p
        self.id = useId()

    def process(self):
        triples = []

        (pIds, pTriples) = self.p.process()
        self.pRefs = getDNFList(pIds)

        for refs in self.pRefs:
            for ref in refs:
                triples.append((ref, OP_IRI, "<="))

        triples.extend(pTriples)
        return pIds, triples

    @staticmethod
    def fromTree(tree):
        if tree[0] != AtMostP.treeName or len(tree) != 2:
            raise Exception("Cannot create " + AtMostP.treeName + " from given tree " + str(tree))

        return AtMostP(percentageFromTree(tree[1]))


class AlmostP:
    treeName = "AlmostP"

    def __init__(self, p):
        self.p = p
        self.id = useId()

    def process(self):
        triples = []

        (pIds, pTriples) = self.p.process()
        self.pRefs = getDNFList(pIds)

        for refs in self.pRefs:
            for ref in refs:
                triples.append((ref, OP_IRI, "~<"))

        triples.extend(pTriples)
        return pIds, triples

    @staticmethod
    def fromTree(tree):
        if tree[0] != AlmostP.treeName or len(tree) != 2:
            raise Exception("Cannot create " + AlmostP.treeName + " from given tree " + str(tree))

        return AlmostP(percentageFromTree(tree[1]))


class AtLeast:
    treeName = "AtLeast"

    def __init__(self, c):
        self.c = c
        self.id = useId()

    def process(self):
        return ">= " + self.c

    @staticmethod
    def fromTree(tree):
        if tree[0] != AtLeast.treeName or len(tree) != 2:
            raise Exception("Cannot create " + AtLeast.treeName + " from given tree " + str(tree))

        return AtLeast(cardToInt(tree[1]))


class AtMost:
    treeName = "AtMost"

    def __init__(self, c):
        self.c = c
        self.id = useId()

    def process(self):
        return "<= " + cardToInt(self.c)

    @staticmethod
    def fromTree(tree):
        if tree[0] != AtMost.treeName or len(tree) != 2:
            raise Exception("Cannot create " + AtMost.treeName + " from given tree " + str(tree))

        return AtMost(cardToInt(tree[1]))


class Almost:
    treeName = "Almost"

    def __init__(self, c):
        self.c = c
        self.id = useId()

    def process(self):
        return "~< " + cardToInt(self.c)

    @staticmethod
    def fromTree(tree):
        if tree[0] != Almost.treeName or len(tree) != 2:
            raise Exception("Cannot create " + Almost.treeName + " from given tree " + str(tree))

        return Almost(cardToInt(tree[1]))


class CardOr:
    treeName = "CardOr"

    def __init__(self, c1, c2):
        self.c1 = c1
        self.c2 = c2
        self.id = useId()

    def process(self):
        return f"{self.c1} | {self.c2}"

    @staticmethod
    def fromTree(tree):
        if tree[0] != CardOr.treeName or len(tree) != 3:
            raise Exception("Cannot create " + CardOr.treeName + " from given tree " + str(tree))

        return CardOr(cardToInt(tree[1]), cardToInt(tree[2]))


class Adjective:
    treeName = "Adj"

    def __init__(self, adj):
        self.adj = adj
        self.id = useId()

    def process(self, action=None, facts=None):
        return self.adj, []

    @staticmethod
    def fromTree(tree):
        if isinstance(tree, list):
            raise Exception("Cannot create " + Adjective.treeName + " from given tree " + str(tree))

        return Adjective(tree)


class ModLeftA:
    treeName = "ModLeftA"

    def __init__(self, adj, adv):
        self.adj = adj
        self.adv = adv
        self.id = useId()

    def process(self, action, facts=None):
        triples = []

        (aS, aTriples) = self.adj.process(action, facts)
        (advIds, advTriples) = self.adv.process([action], facts)

        if action is not None and advIds is not None:
            triples.append((action, MOD_IRI, advIds))

        triples.extend(aTriples)
        triples.extend(advTriples)
        return aS, triples

    @staticmethod
    def fromTree(tree):
        if tree[0] != ModLeftA.treeName or len(tree) != 3:
            raise Exception("Cannot create " + ModLeftA.treeName + " from given tree " + str(tree))

        return ModLeftA(adjFromTree(tree[1]), advFromTree(tree[2]))


class ModRightA:
    treeName = "ModRightA"

    def __init__(self, adj, adv):
        self.adj = adj
        self.adv = adv
        self.id = useId()

    @staticmethod
    def fromTree(tree):
        if tree[0] != ModRightA.treeName or len(tree) != 3:
            raise Exception("Cannot create " + ModRightA.treeName + " from given tree " + str(tree))

        return ModLeftA(adjFromTree(tree[1]), advFromTree(tree[2]))


class ModLeftAA:
    treeName = "ModLeftAA"

    def __init__(self, adj, mod):
        self.adj = adj
        self.mod = mod
        self.id = useId()

    def process(self, action, facts=None):
        triples = []

        (aS, aTriples) = self.adj.process(action, facts)
        (modS, modTriples) = self.mod.process(action, facts)

        if action is not None:
            triples.append((action, MOD_A_IRI, modS))

        triples.extend(aTriples)
        triples.extend(modTriples)
        return aS, triples

    @staticmethod
    def fromTree(tree):
        if tree[0] != ModLeftAA.treeName or len(tree) != 3:
            raise Exception("Cannot create " + ModLeftAA.treeName + " from given tree " + str(tree))

        return ModLeftAA(adjFromTree(tree[1]), adjFromTree(tree[2]))


class ModRightAA:
    treeName = "ModRightAA"

    def __init__(self, adj, mod):
        self.adj = adj
        self.mod = mod
        self.id = useId()

    @staticmethod
    def fromTree(tree):
        if tree[0] != ModRightAA.treeName or len(tree) != 3:
            raise Exception("Cannot create " + ModRightAA.treeName + " from given tree " + str(tree))

        return ModLeftAA(adjFromTree(tree[1]), adjFromTree(tree[2]))


class ZurAdv:
    treeName = "ZurAdv"

    def __init__(self, i):
        self.i = i
        self.id = useId()
        self.iRefs = []

    def process(self, actions=None, facts=None):
        triples = []

        (iIds, iTriples) = self.i.process(facts)
        self.iRefs = getDNFList(iIds)

        if actions is not None:
            for a in actions:
                triples.append((a, ZU_IRI, iIds))

        triples.extend(iTriples)
        return None, triples

    @staticmethod
    def fromTree(tree):
        if tree[0] != ZurAdv.treeName or len(tree) != 2:
            raise Exception("Cannot create " + ZurAdv.treeName + " from given tree " + str(tree))

        return ZurAdv(Nodes.InstanceNodes.instanceFromTree(tree[1]))


class AsAdv:
    treeName = "AsAdv"

    def __init__(self, i):
        self.i = i
        self.id = useId()
        self.iRefs = []

    def process(self, actions=None, facts=None):
        triples = []

        (iIds, iTriples) = self.i.process(facts)
        self.iRefs = getDNFList(iIds)

        beActId = useId()
        triples.extend([(beActId, "a", ACTION_IRI), (beActId, PRED_IRI, BE_IRI), (beActId, OBJ_IRI, iIds)])

        if facts is not None:
            for fact in facts:
                beFactIds = []
                for sRefs in fact.subjectRefs:
                    for sRef in sRefs:
                        beFactId = useId()
                        triples.extend([(beFactId, "a", FACT_IRI),
                                        (beFactId, SUBJ_IRI, sRef),
                                        (beFactId, ACT_IRI, beActId)])
                        beFactIds.append(beFactId)

                triples.append((fact.id, REASON_IRI, " & ".join(list(map(lambda fIds: f"({fIds})", beFactIds)))))

        triples.extend(iTriples)
        return None, triples

    @staticmethod
    def fromTree(tree):
        if tree[0] != AsAdv.treeName or len(tree) != 2:
            raise Exception("Cannot create " + AsAdv.treeName + " from given tree " + str(tree))

        return AsAdv(Nodes.InstanceNodes.instanceFromTree(tree[1]))


class WhenExistsAdv:
    treeName = "WhenExistsAdv"

    def __init__(self, i, iTree):
        self.i = i
        self.iTree = iTree
        self.id = useId()
        self.iRefs = []

    def process(self, actions=None, facts=None):
        triples = []

        (iIds, iTriples) = self.i.process(facts)
        self.iRefs = getDNFList(iIds)

        existsActId = useId()
        triples.extend([(existsActId, "a", ACTION_IRI), (existsActId, PRED_IRI, EXIST_IRI)])

        if facts is not None:
            for fact in facts:
                existsFactId = useId()
                triples.extend([(existsFactId, "a", FACT_IRI),
                                (existsFactId, SUBJ_IRI, iIds),
                                (existsFactId, ACT_IRI, existsActId),
                                (existsFactId, SYNTREE_IRI, f"StatementPos TPres ASimul (Fact ({self.iTree}) (PropAction vorhanden_A))"),
                                (fact.id, COND_IRI, existsFactId)])

        triples.extend(iTriples)
        return None, triples

    @staticmethod
    def fromTree(tree):
        if tree[0] != WhenExistsAdv.treeName or len(tree) != 2:
            raise Exception("Cannot create " + WhenExistsAdv.treeName + " from given tree " + str(tree))

        return WhenExistsAdv(Nodes.InstanceNodes.instanceFromTree(tree[1]), treeToString(tree[1]))


class ReferringAdv:
    treeName = "ReferringAdv"

    def __init__(self, i):
        self.i = i
        self.id = useId()
        self.iRefs = []

    def process(self, actions=None, facts=None):
        triples = []

        (iIds, iTriples) = self.i.process(facts)
        self.iRefs = getDNFList(iIds)

        if actions is not None:
            for a in actions:
                triples.append((a, REGARDING_IRI, iIds))

        triples.extend(iTriples)
        return None, triples

    @staticmethod
    def fromTree(tree):
        if tree[0] != ReferringAdv.treeName or len(tree) != 2:
            raise Exception("Cannot create " + ReferringAdv.treeName + " from given tree " + str(tree))

        return ReferringAdv(Nodes.InstanceNodes.instanceFromTree(tree[1]))


class LocationAdv:
    treeName = "LocationAdv"

    def __init__(self, i):
        self.i = i
        self.id = useId()
        self.iRefs = []

    def process(self, actions=None, facts=None):
        triples = []

        (iIds, iTriples) = self.i.process(facts)
        self.iRefs = getDNFList(iIds)

        if actions is not None:
            for a in actions:
                triples.append((a, LOC_IRI, iIds))

        triples.extend(iTriples)
        return None, triples

    @staticmethod
    def fromTree(tree):
        if tree[0] != LocationAdv.treeName or len(tree) != 2:
            raise Exception("Cannot create " + LocationAdv.treeName + " from given tree " + str(tree))

        return LocationAdv(Nodes.InstanceNodes.instanceFromTree(tree[1]))


class PartAdv:
    treeName = "PartAdv"

    def __init__(self, i):
        self.i = i
        self.id = useId()
        self.iRefs = []

    def process(self, actions=None, facts=None):
        triples = []

        (iIds, iTriples) = self.i.process(facts)
        self.iRefs = getDNFList(iIds)

        if actions is not None:
            for a in actions:
                triples.append((a, PART_IRI, iIds))

        triples.extend(iTriples)
        return None, triples

    @staticmethod
    def fromTree(tree):
        if tree[0] != PartAdv.treeName or len(tree) != 2:
            raise Exception("Cannot create " + PartAdv.treeName + " from given tree " + str(tree))

        return PartAdv(Nodes.InstanceNodes.instanceFromTree(tree[1]))


class TargetAdv:
    treeName = "TargetAdv"

    def __init__(self, i):
        self.i = i
        self.id = useId()
        self.iRefs = []

    def process(self, actions=None, facts=None):
        triples = []

        (iIds, iTriples) = self.i.process(facts)
        self.iRefs = getDNFList(iIds)

        if actions is not None:
            for a in actions:
                triples.append((a, TARGET_IRI, iIds))

        triples.extend(iTriples)
        return None, triples

    @staticmethod
    def fromTree(tree):
        if tree[0] != TargetAdv.treeName or len(tree) != 2:
            raise Exception("Cannot create " + TargetAdv.treeName + " from given tree " + str(tree))

        return TargetAdv(Nodes.InstanceNodes.instanceFromTree(tree[1]))


class DateAdv:
    treeName = "DateAdv"

    def __init__(self, d, m, y):
        self.d = d
        self.m = m
        self.y = y
        self.id = useId()

    def process(self, actions=None, facts=None):
        dateId = useId()
        triples = [(dateId, "a", DATE_IRI),
                   (dateId, DATE_DAY_IRI, self.d),
                   (dateId, DATE_MONTH_IRI, self.m),
                   (dateId, DATE_YEAR_IRI, self.y)]

        if actions is not None:
            for a in actions:
                triples.append((a, DATE_PROP_IRI, dateId))

        return None, triples

    @staticmethod
    def fromTree(tree):
        if tree[0] != DateAdv.treeName or len(tree) != 4:
            raise Exception("Cannot create " + DateAdv.treeName + " from given tree " + str(tree))

        return DateAdv(tree[1], tree[2], tree[3])


class DateAdv3:
    treeName = "DateAdv3"

    @staticmethod
    def fromTree(tree):
        if tree[0] != DateAdv3.treeName or len(tree) != 4:
            raise Exception("Cannot create " + DateAdv3.treeName + " from given tree " + str(tree))

        return DateAdv(tree[1], Nodes.ConceptNodes.monthIntFromName(tree[2]), tree[3])


class DurchAdv:
    treeName = "DurchAdv"

    def __init__(self, i):
        self.i = i
        self.id = useId()
        self.iRefs = []

    def process(self, actions=None, facts=None):
        triples = []

        (iIds, iTriples) = self.i.process(facts)
        self.iRefs = getDNFList(iIds)

        if actions is not None:
            for a in actions:
                triples.append((a, DURCH_IRI, iIds))

        triples.extend(iTriples)
        return None, triples

    @staticmethod
    def fromTree(tree):
        if tree[0] != DurchAdv.treeName or len(tree) != 2:
            raise Exception("Cannot create " + DurchAdv.treeName + " from given tree " + str(tree))

        return DurchAdv(Nodes.InstanceNodes.instanceFromTree(tree[1]))


class MitAdv:
    treeName = "MitAdv"

    def __init__(self, i):
        self.i = i
        self.id = useId()
        self.iRefs = []

    def process(self, actions=None, facts=None):
        triples = []

        (iIds, iTriples) = self.i.process(facts)
        self.iRefs = getDNFList(iIds)

        if actions is not None:
            for a in actions:
                triples.append((a, MIT_IRI, iIds))

        triples.extend(iTriples)
        return None, triples

    @staticmethod
    def fromTree(tree):
        if tree[0] != MitAdv.treeName or len(tree) != 2:
            raise Exception("Cannot create " + MitAdv.treeName + " from given tree " + str(tree))

        return MitAdv(Nodes.InstanceNodes.instanceFromTree(tree[1]))


class UnterVerzichtAdv:
    treeName = "UnterVerzichtAdv"

    def __init__(self, i):
        self.i = i
        self.id = useId()
        self.iRefs = []

    def process(self, actions=None, facts=None):
        triples = []

        (iIds, iTriples) = self.i.process(facts)
        self.iRefs = getDNFList(iIds)

        waivingActId = useId()
        triples.extend([(waivingActId, "a", ACTION_IRI), (waivingActId, PRED_IRI, "verzichten_V2"), (waivingActId, OBJ_IRI, iIds)])

        if facts is not None:
            for f in facts:
                subjRefs = [[""]]
                if len(f.subjRefs) > 0:
                    subjRefs = f.subjRefs

                if subjRefs is not None:
                    for refs in subjRefs:
                        for ref in refs:
                            waivingFactId = useId()
                            if ref != "":
                                triples.append((waivingFactId, SUBJ_IRI, ref))
                            triples.extend([(waivingFactId, "a", FACT_IRI),
                                            (waivingFactId, ACT_IRI, waivingActId)])

        triples.extend(iTriples)
        return None, triples

    @staticmethod
    def fromTree(tree):
        if tree[0] != UnterVerzichtAdv.treeName or len(tree) != 2:
            raise Exception("Cannot create " + UnterVerzichtAdv.treeName + " from given tree " + str(tree))

        return UnterVerzichtAdv(Nodes.InstanceNodes.instanceFromTree(tree[1]))


class OhneAdv:
    treeName = "OhneAdv"

    def __init__(self, i):
        self.i = i
        self.id = useId()
        self.iRefs = []

    def process(self, actions=None, facts=None):
        triples = []

        (iIds, iTriples) = self.i.process(facts)
        self.iRefs = getDNFList(iIds)

        if actions is not None:
            for a in actions:
                triples.append((a, OHNE_IRI, iIds))

        triples.extend(iTriples)
        return None, triples

    @staticmethod
    def fromTree(tree):
        if tree[0] != OhneAdv.treeName or len(tree) != 2:
            raise Exception("Cannot create " + OhneAdv.treeName + " from given tree " + str(tree))

        return OhneAdv(Nodes.InstanceNodes.instanceFromTree(tree[1]))


class ForAdv:
    treeName = "ForAdv"

    def __init__(self, i):
        self.i = i
        self.id = useId()
        self.iRefs = []

    def process(self, actions=None, facts=None):
        triples = []

        (iIds, iTriples) = self.i.process(facts)
        self.iRefs = getDNFList(iIds)

        if actions is not None:
            for a in actions:
                triples.append((a, FOR_IRI, iIds))

        triples.extend(iTriples)
        return None, triples

    @staticmethod
    def fromTree(tree):
        if tree[0] != ForAdv.treeName or len(tree) != 2:
            raise Exception("Cannot create " + ForAdv.treeName + " from given tree " + str(tree))

        return ForAdv(Nodes.InstanceNodes.instanceFromTree(tree[1]))


class DisregardingAdv:
    treeName = "DisregardingAdv"

    def __init__(self, i):
        self.i = i
        self.id = useId()
        self.iRefs = []

    def process(self, actions=None, facts=None):
        triples = []

        (iIds, iTriples) = self.i.process(facts)
        self.iRefs = getDNFList(iIds)

        if actions is not None:
            for a in actions:
                triples.append((a, DISREGARDING_IRI, iIds))

        triples.extend(iTriples)
        return None, triples

    @staticmethod
    def fromTree(tree):
        if tree[0] != DisregardingAdv.treeName or len(tree) != 2:
            raise Exception("Cannot create " + DisregardingAdv.treeName + " from given tree " + str(tree))

        return DisregardingAdv(Nodes.InstanceNodes.instanceFromTree(tree[1]))


class SCFromAct:
    treeName = "SCFromAct"

    def __init__(self, action):
        self.action = action
        self.id = useId()
        self.actionRefs = []

    def process(self, facts):
        (actionIds, actionTriples) = self.action.process(facts)
        self.actionRefs = getDNFList(actionIds)

        return actionIds, actionTriples

    @staticmethod
    def fromTree(tree):
        if tree[0] != SCFromAct.treeName or len(tree) != 2:
            raise Exception("Cannot create " + SCFromAct.treeName + " from given tree " + str(tree))

        return SCFromAct(Nodes.ActionNodes.actionFromTree(tree[1]))


class SCAnd:
    treeName = "SCAnd"

    def __init__(self, sc1, sc2):
        self.sc1 = sc1
        self.sc2 = sc2
        self.id = useId()
        self.sc1Refs = []
        self.sc2Refs = []

    def process(self, facts):
        triples = []

        (sc1Ids, sc1Triples) = self.sc1.process(facts)
        self.sc1Refs = getDNFList(sc1Ids)

        (sc2Ids, sc2Triples) = self.sc2.process(facts)
        self.sc2Refs = getDNFList(sc2Ids)

        triples.extend(sc1Triples)
        triples.extend(sc2Triples)
        return f"({sc1Ids}) & ({sc2Ids})", triples

    @staticmethod
    def fromTree(tree):
        if tree[0] != SCAnd.treeName or len(tree) != 3:
            raise Exception("Cannot create " + SCAnd.treeName + " from given tree " + str(tree))

        return SCAnd(Nodes.MiscNodes.scFromTree(tree[1]), Nodes.MiscNodes.scFromTree(tree[2]))


class OriginAdv:
    treeName = "OriginAdv"

    def __init__(self, i):
        self.i = i
        self.id = useId()
        self.iRefs = []

    def process(self, actions=None, facts=None):
        triples = []

        (iIds, iTriples) = self.i.process(facts)
        self.iRefs = getDNFList(iIds)

        if actions is not None:
            for a in actions:
                triples.append((a, ORIGIN_IRI, iIds))

        triples.extend(iTriples)
        return None, triples

    @staticmethod
    def fromTree(tree):
        if tree[0] != OriginAdv.treeName or len(tree) != 2:
            raise Exception("Cannot create " + OriginAdv.treeName + " from given tree " + str(tree))

        return OriginAdv(Nodes.InstanceNodes.instanceFromTree(tree[1]))


class AccordingToAdv:
    treeName = "AccordingToAdv"

    def __init__(self, i):
        self.i = i
        self.id = useId()
        self.iRefs = []

    def process(self, actions=None, facts=None):
        triples = []

        (iIds, iTriples) = self.i.process(facts)
        self.iRefs = getDNFList(iIds)

        if actions is not None:
            for a in actions:
                triples.append((a, ACCORDING_TO_IRI, iIds))

        triples.extend(iTriples)
        return None, triples

    @staticmethod
    def fromTree(tree):
        if tree[0] != AccordingToAdv.treeName or len(tree) != 2:
            raise Exception("Cannot create " + AccordingToAdv.treeName + " from given tree " + str(tree))

        return AccordingToAdv(Nodes.InstanceNodes.instanceFromTree(tree[1]))


class DueToAdv:
    treeName = "DueToAdv"

    def __init__(self, i):
        self.i = i
        self.id = useId()
        self.iRefs = []

    def process(self, actions=None, facts=None):
        triples = []

        (iIds, iTriples) = self.i.process(facts)
        self.iRefs = getDNFList(iIds)

        if actions is not None:
            for a in actions:
                triples.append((a, REASON_IRI, iIds))

        triples.extend(iTriples)
        return None, triples

    @staticmethod
    def fromTree(tree):
        if tree[0] != DueToAdv.treeName or len(tree) != 2:
            raise Exception("Cannot create " + DueToAdv.treeName + " from given tree " + str(tree))

        return DueToAdv(Nodes.InstanceNodes.instanceFromTree(tree[1]))


class BesidesAdv:
    treeName = "BesidesAdv"

    def __init__(self, i):
        self.i = i
        self.id = useId()
        self.iRefs = []

    def process(self, actions=None, facts=None):
        (iIds, iTriples) = self.i.process(facts)
        self.iRefs = getDNFList(iIds)

        factId = useId()
        triples = [(factId, "a", FACT_IRI), (factId, SUBJ_IRI, iIds)]

        if actions is not None:
            for a in actions:
                triples.append((a, LINKED_FACT_IRI, factId))
                triples.append((factId, ACT_IRI, a))

        triples.extend(iTriples)
        return None, triples

    @staticmethod
    def fromTree(tree):
        if tree[0] != BesidesAdv.treeName or len(tree) != 2:
            raise Exception("Cannot create " + BesidesAdv.treeName + " from given tree " + str(tree))

        return BesidesAdv(Nodes.InstanceNodes.instanceFromTree(tree[1]))


class WithEffectToAdv:
    treeName = "WithEffectToAdv"

    def __init__(self, d, m, y):
        self.d = d
        self.m = m
        self.y = y
        self.id = useId()
        self.iRefs = []

    def process(self, actions=None, facts=None):
        dateId = useId()
        triples = [(dateId, "a", DATE_IRI),
                   (dateId, DATE_DAY_IRI, self.d),
                   (dateId, DATE_MONTH_IRI, self.m),
                   (dateId, DATE_YEAR_IRI, self.y)]

        if actions is not None:
            for a in actions:
                triples.append((a, TAKES_EFFECT_DATE_IRI, dateId))

        return None, triples

    @staticmethod
    def fromTree(tree):
        if tree[0] != WithEffectToAdv.treeName or len(tree) != 4:
            raise Exception("Cannot create " + WithEffectToAdv.treeName + " from given tree " + str(tree))

        return WithEffectToAdv(tree[1], tree[2], tree[3])


class AccordingToLawSectionAdv:
    treeName = "AccordingToLawSectionAdv"

    def __init__(self, ls):
        self.ls = ls
        self.id = useId()
        self.lsRefs = []

    def process(self, actions=None, facts=None):
        triples = []

        (lsIds, lsTriples) = self.ls.process(facts)
        self.lsRefs = getDNFList(lsIds)

        if actions is not None:
            for a in actions:
                triples.append((a, ACCORDING_TO_IRI, lsIds))

        triples.extend(lsTriples)
        return None, triples

    @staticmethod
    def fromTree(tree):
        if tree[0] != AccordingToLawSectionAdv.treeName or len(tree) != 2:
            raise Exception("Cannot create " + AccordingToLawSectionAdv.treeName + " from given tree " + str(tree))

        return AccordingToLawSectionAdv(Nodes.ConceptNodes.lawSectionFromTree(tree[1]))


class AdverbialInsertion:
    treeName = "AdverbialInsertion"

    def __init__(self, adv):
        self.adv = adv
        self.id = useId()

    def process(self, actions=None, facts=None):
        return self.adv.process(actions, facts)

    @staticmethod
    def fromTree(tree):
        if tree[0] != AdverbialInsertion.treeName or len(tree) != 2:
            raise Exception("Cannot create " + AdverbialInsertion.treeName + " from given tree " + str(tree))

        return AdverbialInsertion(advFromTree(tree[1]))


class AdverbialInsertionNeg:
    treeName = "AdverbialInsertionNeg"

    def __init__(self, adv):
        self.adv = adv
        self.id = useId()

    def process(self, actions=None, facts=None):
        triples = []
        (advIds, advTriples) = self.adv.process(actions, facts)

        if facts is not None:
            for f in facts:
                triples.append((f.id, NEG_IRI, "true"))

        triples.extend(advTriples)
        return advIds, triples

    @staticmethod
    def fromTree(tree):
        if tree[0] != AdverbialInsertionNeg.treeName or len(tree) != 2:
            raise Exception("Cannot create " + AdverbialInsertionNeg.treeName + " from given tree " + str(tree))

        return AdverbialInsertionNeg(advFromTree(tree[1]))


class ThatAdv:
    treeName = "ThatAdv"

    def __init__(self, s):
        self.s = s
        self.id = useId()
        self.sRefs = []

    def process(self, actions=None, facts=None):
        triples = []

        (sIds, sTriples) = self.s.process()
        self.sRefs = getDNFList(sIds)

        if actions is not None:
            for a in actions:
                triples.append((a, THAT_IRI, sIds))

        triples.extend(sTriples)
        return None, triples

    @staticmethod
    def fromTree(tree):
        if tree[0] != ThatAdv.treeName or len(tree) != 2:
            raise Exception("Cannot create " + ThatAdv.treeName + " from given tree " + str(tree))

        return ThatAdv(Nodes.StatementNodes.statementFromTree(tree[1]))
