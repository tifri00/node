from Utility import getDNFList, useId
from constants import *

import Nodes.InstanceNodes
import Nodes.ActionNodes

nodes = []


def init(nodeList):
    global nodes
    nodes = nodeList


def factFromTree(tree):
    if tree[0] in ["FactAllowedTo2", "FactAllowedTo3", "FactAllowedTo4"]:
        tree[0] = "FactAllowedTo"
    elif tree[0] in ["NoSubjFact2", "NoSubjFact3", "NoSubjFact4", "NoSubjFact5", "NoSubjFact6",
                     "NoSubjFact7", "NoSubjFact8", "NoSubjFact9", "NoSubjFact10", "NoSubjFact11",
                     "NoSubjFact12", "NoSubjFact13", "NoSubjFact14", "NoSubjFact15", "NoSubjFact16",
                     "NoSubjFact17", "NoSubjFact18"]:
        tree[0] = "NoSubjFact"
    elif tree[0] in ["FactImpersV2Colon2", "FactImpersV2Colon3"]:
        tree[0] = "FactImpersV2Colon"

    factNodes = list(filter(lambda x: x.treeName == tree[0], nodes))

    if factNodes is None or len(factNodes) == 0:
        raise Exception("Cannot construct fact from given tree " + str(tree))

    return factNodes[0].fromTree(tree)


class Fact:
    treeName = "Fact"

    def __init__(self, subject, action):
        self.subject = subject
        self.action = action
        self.id = useId()
        self.subjectRefs = []
        self.actionRefs = []

    def process(self):
        triples = [(self.id, "a", FACT_IRI)]

        (subjIds, subjTriples) = self.subject.process([self])
        self.subjectRefs = getDNFList(subjIds)

        (actionIds, actionTriples) = self.action.process([self])
        self.actionRefs = getDNFList(actionIds)

        if subjIds is not None and not (self.id, SUBJ_IRI, "?") in actionTriples:
            triples.append((self.id, SUBJ_IRI, subjIds))

        if actionIds is not None:
            triples.append((self.id, ACT_IRI, actionIds))

        triples.extend(subjTriples)
        triples.extend(filter(lambda t: t != (self.id, SUBJ_IRI, "?"), actionTriples))
        return self.id, triples

    @staticmethod
    def fromTree(tree):
        if tree[0] != Fact.treeName or len(tree) != 3:
            raise Exception("Cannot create " + Fact.treeName + " from given tree " + str(tree))

        return Fact(Nodes.InstanceNodes.instanceFromTree(tree[1]), Nodes.ActionNodes.actionFromTree(tree[2]))


class FactAllowedTo:
    treeName = "FactAllowedTo"

    def __init__(self, action):
        self.action = action
        self.id = useId()
        self.actionRefs = []

    def process(self):
        triples = [(self.id, "a", FACT_IRI)]

        (actionIds, actionTriples) = self.action.process([self])
        self.actionRefs = getDNFList(actionIds)

        if actionIds is not None:
            triples.append((self.id, ACT_IRI, actionIds))

        for refs in self.actionRefs:
            for ref in refs:
                triples.append((ref, MODAL_IRI, MOD_PERMISSION_IRI))

        triples.extend(filter(lambda t: t != (self.id, SUBJ_IRI, "?"), actionTriples))
        return self.id, triples

    @staticmethod
    def fromTree(tree):
        if tree[0] != FactAllowedTo.treeName or len(tree) != 2:
            raise Exception("Cannot create " + FactAllowedTo.treeName + " from given tree " + str(tree))

        return FactAllowedTo(Nodes.ActionNodes.actionFromTree(tree[1]))


class FactImpers:
    treeName = "FactImpers"

    def __init__(self, action):
        self.action = action
        self.id = useId()
        self.actionRefs = []
        self.subjectRefs = []

    def process(self):
        generalSubjId = useId()
        triples = [(self.id, "a", FACT_IRI),
                   (self.id, SUBJ_IRI, generalSubjId),
                   (generalSubjId, "a", CONCEPT_IRI),
                   (generalSubjId, CONCEPT_IRI, GENERAL_SUBJ_IRI)]

        (actionIds, actionTriples) = self.action.process([self])
        self.actionRefs = getDNFList(actionIds)

        if actionIds is not None:
            triples.append((self.id, ACT_IRI, actionIds))

        triples.extend(filter(lambda t: t != (self.id, SUBJ_IRI, "?") and not (t[1] == OBJ_IRI and t[2] == ""), actionTriples))
        return self.id, triples

    @staticmethod
    def fromTree(tree):
        if tree[0] != FactImpers.treeName or len(tree) != 2:
            raise Exception("Cannot create " + FactImpers.treeName + " from given tree " + str(tree))

        return FactImpers(Nodes.ActionNodes.actionFromTree(tree[1]))


class NoSubjFact:
    treeName = "NoSubjFact"

    def __init__(self, action):
        self.action = action
        self.id = useId()
        self.actionRefs = []
        self.subjectRefs = []

    def process(self):
        triples = [(self.id, "a", FACT_IRI)]

        (actionIds, actionTriples) = self.action.process([self])
        self.actionRefs = getDNFList(actionIds)

        if actionIds is not None:
            triples.append((self.id, ACT_IRI, actionIds))

        triples.extend(filter(lambda t: t != (self.id, SUBJ_IRI, "?") and not (t[1] == OBJ_IRI and t[2] == ""), actionTriples))
        return self.id, triples

    @staticmethod
    def fromTree(tree):
        if tree[0] != NoSubjFact.treeName or len(tree) != 2:
            raise Exception("Cannot create " + NoSubjFact.treeName + " from given tree " + str(tree))

        return NoSubjFact(Nodes.ActionNodes.actionFromTree(tree[1]))


class FactImpersV2Colon:
    treeName = "FactImpersV2Colon"

    def __init__(self, action, obj):
        self.action = action
        self.obj = obj
        self.id = useId()
        self.actionRefs = []
        self.subjectRefs = []
        self.objRefs = []

    def process(self):
        (objIds, objTriples) = self.obj.process([self])
        self.objRefs = getDNFList(objIds)

        (actionIds, actionTriples) = self.action.process([self])
        self.actionRefs = getDNFList(actionIds)

        if self.action.isPassive:
            generalSubjId = useId()
            triples = [(self.id, "a", FACT_IRI),
                       (self.id, SUBJ_IRI, generalSubjId),
                       (generalSubjId, "a", CONCEPT_IRI),
                       (generalSubjId, CONCEPT_IRI, GENERAL_SUBJ_IRI)]

            for refs in self.actionRefs:
                for ref in refs:
                    triples.append((ref, OBJ_IRI, objIds))

            triples.extend(filter(lambda t: t != (self.id, SUBJ_IRI, "?") and not t[1] == OBJ_IRI, actionTriples))
        else:
            triples = [(self.id, "a", FACT_IRI),
                       (self.id, SUBJ_IRI, objIds)]

            triples.extend(filter(lambda t: t != (self.id, SUBJ_IRI, "?") and not (t[1] == OBJ_IRI and t[2] == ""), actionTriples))

        if actionIds is not None:
            triples.append((self.id, ACT_IRI, actionIds))

        triples.extend(objTriples)
        return self.id, triples

    @staticmethod
    def fromTree(tree):
        if tree[0] != FactImpersV2Colon.treeName or len(tree) != 3:
            raise Exception("Cannot create " + FactImpersV2Colon.treeName + " from given tree " + str(tree))

        return FactImpersV2Colon(Nodes.ActionNodes.actionFromTree(tree[1]), Nodes.InstanceNodes.instanceFromTree(tree[2]))


class Exist:
    treeName = "Exist"

    def __init__(self, subject):
        self.subject = subject
        self.id = useId()
        self.actionId = useId()
        self.subjectRefs = []

    def process(self):
        triples = [(self.id, "a", FACT_IRI), (self.id, ACT_IRI, [self.actionId]), (self.actionId, PRED_IRI, EXIST_IRI)]

        (subjIds, subjTriples) = self.subject.process([self])
        self.subjectRefs = getDNFList(subjIds)

        if subjIds is not None:
            triples.append((self.id, SUBJ_IRI, subjIds))

        triples.extend(subjTriples)
        return self.id, triples

    @staticmethod
    def fromTree(tree):
        if tree[0] != Fact.treeName or len(tree) != 2:
            raise Exception("Cannot create " + Exist.treeName + " from given tree " + str(tree))

        return Exist(Nodes.InstanceNodes.instanceFromTree(tree[1]))


class Annotation:
    treeName = "Annotation"

    def __init__(self, f, i):
        self.f = f
        self.i = i
        self.id = useId()
        self.fRefs = []
        self.iRefs = []

    def process(self):
        triples = []

        (fIds, fTriples) = self.f.process()
        self.fRefs = getDNFList(fIds)

        (iIds, iTriples) = self.i.process([self])
        self.iRefs = getDNFList(iIds)

        for refs in self.fRefs:
            for ref in refs:
                triples.append((ref, CONCEPT_ANNOTATION_IRI, iIds))

        triples.extend(fTriples)
        triples.extend(iTriples)

        return fIds, triples

    @staticmethod
    def fromTree(tree):
        if tree[0] != Annotation.treeName or len(tree) != 3:
            raise Exception("Cannot create " + Annotation.treeName + " from given tree " + str(tree))

        return Annotation(factFromTree(tree[1]), Nodes.InstanceNodes.instanceFromTree(tree[2]))
