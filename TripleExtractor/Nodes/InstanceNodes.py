from Utility import useId, getDNFList, numeralToInt
from constants import *

import Nodes.ConceptNodes
import Nodes.MiscNodes

nodes = []


def init(nodeList):
    global nodes
    nodes = nodeList


def instanceFromTree(tree):
    if tree[0] in ["Instance2", "Instance3", "Instance4", "Instance5", "Instance6", "Instance7", "Instance8",
                   "Instance9", "Instance10", "Instance11", "Instance12", "Instance13", "Instance14", "Instance15",
                   "Instance16", "Instance17", "Instance18", "InstanceOwnerPronSg", "InstanceOwnerPronPl"]:
        tree[0] = "Instance"
    elif tree[0] in ["InstanceEvery2", "InstanceEvery3"]:
        tree[0] = "InstanceEvery"
    elif tree[0] in ["InstanceMany2"]:
        tree[0] = "InstanceMany"
    elif tree[0] in ["And2", "And3", "And4", "And5", "And6"]:
        tree[0] = "And"
    elif tree[0] in ["CommaDescription2"]:
        tree[0] = "CommaDescription"
    elif tree[0] in ["CommaDescriptionHometownMasc2"]:
        tree[0] = "CommaDescriptionHometownMasc"
    elif tree[0] in ["CommaDescriptionHometownFem2"]:
        tree[0] = "CommaDescriptionHometownFem"
    elif tree[0] in ["CommaDescriptionHometownNeut2"]:
        tree[0] = "CommaDescriptionHometownNeut"
    elif tree[0] in ["CommaDescriptionHometown3", "CommaDescriptionHometown4"]:
        tree[0] = "CommaDescriptionHometown2"
    elif tree[0] in ["Location2", "Location3", "Location4", "Location5", "Location6", "Location7"]:
        tree[0] = "Location"
    elif tree[0] in ["Target2", "Target3"]:
        tree[0] = "Target"
    elif tree[0] in ["Referring2", "Referring3", "Referring4", "Referring5", "Referring6"]:
        tree[0] = "Referring"
    elif tree[0] in ["AccordingTo2", "AccordingTo3", "AccordingTo4", "AccordingTo5", "AccordingTo6"]:
        tree[0] = "AccordingTo"
    elif tree[0] in ["CommaDescriptionBirthday2", "CommaDescriptionBirthday3", "CommaDescriptionBirthday4"]:
        tree[0] = "CommaDescriptionBirthday"

    instanceNodes = list(filter(lambda x: x.treeName == tree[0], nodes))

    if instanceNodes is None or len(instanceNodes) == 0:
        raise Exception("Cannot construct instance from given tree " + str(tree))

    return instanceNodes[0].fromTree(tree)


class Instance:
    treeName = "Instance"

    def __init__(self, concept):
        self.concept = concept
        self.id = concept.id

    def process(self, facts=None):
        return self.concept.process(facts)

    @staticmethod
    def fromTree(tree):
        if tree[0] != Instance.treeName or len(tree) != 2:
            raise Exception("Cannot create " + Instance.treeName + " from given tree " + str(tree))

        return Instance(Nodes.ConceptNodes.conceptFromTree(tree[1]))


class InstanceEvery:
    treeName = "InstanceEvery"

    def __init__(self, concept):
        self.concept = concept
        self.id = useId()
        self.conceptRefs = []

    def process(self, facts=None):
        triples = [(self.id, "a", QUANTIFIER_IRI), (self.id, NUM_IRI, QUANT_ALL_VALUE)]

        (conceptIds, conceptTriples) = self.concept.process(facts)
        self.conceptRefs = getDNFList(conceptIds)

        for ref in self.conceptRefs:
            for c in ref:
                triples.append((c, QUANT_IRI, self.id))

        triples.extend(conceptTriples)
        return conceptIds, triples

    @staticmethod
    def fromTree(tree):
        if tree[0] != InstanceEvery.treeName or len(tree) != 2:
            raise Exception("Cannot create " + InstanceEvery.treeName + " from given tree " + str(tree))

        return InstanceEvery(Nodes.ConceptNodes.conceptFromTree(tree[1]))


class InstanceFew:
    treeName = "InstanceFew"

    def __init__(self, concept):
        self.concept = concept
        self.id = useId()
        self.conceptRefs = []

    def process(self, facts=None):
        triples = [(self.id, "a", QUANTIFIER_IRI), (self.id, NUM_IRI, QUANT_FEW_VALUE)]

        (conceptIds, conceptTriples) = self.concept.process(facts)
        self.conceptRefs = getDNFList(conceptIds)

        for ref in self.conceptRefs:
            for c in ref:
                triples.append((c, QUANT_IRI, self.id))

        triples.extend(conceptTriples)
        return conceptIds, triples

    @staticmethod
    def fromTree(tree):
        if tree[0] != InstanceFew.treeName or len(tree) != 2:
            raise Exception("Cannot create " + InstanceFew.treeName + " from given tree " + str(tree))

        return InstanceFew(Nodes.ConceptNodes.conceptFromTree(tree[1]))


class InstanceMany:
    treeName = "InstanceMany"

    def __init__(self, concept):
        self.concept = concept
        self.id = useId()
        self.conceptRefs = []

    def process(self, facts=None):
        triples = [(self.id, "a", QUANTIFIER_IRI), (self.id, NUM_IRI, QUANT_MANY_VALUE)]

        (conceptIds, conceptTriples) = self.concept.process(facts)
        self.conceptRefs = getDNFList(conceptIds)

        for ref in self.conceptRefs:
            for c in ref:
                triples.append((c, QUANT_IRI, self.id))

        triples.extend(conceptTriples)
        return conceptIds, triples

    @staticmethod
    def fromTree(tree):
        if tree[0] != InstanceMany.treeName or len(tree) != 2:
            raise Exception("Cannot create " + InstanceMany.treeName + " from given tree " + str(tree))

        return InstanceMany(Nodes.ConceptNodes.conceptFromTree(tree[1]))


class InstanceNone:
    treeName = "InstanceNone"

    def __init__(self, concept):
        self.concept = concept
        self.id = useId()
        self.conceptRefs = []

    def process(self, facts=None):
        triples = [(self.id, "a", QUANTIFIER_IRI), (self.id, NUM_IRI, QUANT_NONE_VALUE)]

        (conceptIds, conceptTriples) = self.concept.process(facts)
        self.conceptRefs = getDNFList(conceptIds)

        for ref in self.conceptRefs:
            for c in ref:
                triples.append((c, QUANT_IRI, self.id))

        triples.extend(conceptTriples)
        return conceptIds, triples

    @staticmethod
    def fromTree(tree):
        if tree[0] != InstanceNone.treeName or len(tree) != 2:
            raise Exception("Cannot create " + InstanceNone.treeName + " from given tree " + str(tree))

        return InstanceNone(Nodes.ConceptNodes.conceptFromTree(tree[1]))


class InstancePN:
    treeName = "InstancePN"

    def __init__(self, pn):
        self.pn = pn
        self.id = useId()

    def process(self, facts=None):
        triples = [(self.id, "a", CONCEPT_IRI), (self.id, CONC_IRI, self.pn)]

        return self.id, triples

    @staticmethod
    def fromTree(tree):
        if tree[0] != InstancePN.treeName or len(tree) != 2:
            raise Exception("Cannot create " + InstancePN.treeName + " from given tree " + str(tree))

        return InstancePN(tree[1])


class InstancePron:
    treeName = "InstancePron"

    def __init__(self, pron):
        self.pron = pron
        self.id = useId()

    def process(self, facts=None):
        triples = [(self.id, "a", CONCEPT_IRI), (self.id, CONC_IRI, self.pron)]

        return self.id, triples

    @staticmethod
    def fromTree(tree):
        if tree[0] != InstancePron.treeName or len(tree) != 2:
            raise Exception("Cannot create " + InstancePron.treeName + " from given tree " + str(tree))

        return InstancePron(tree[1])


class InstanceGeneral:
    treeName = "InstanceGeneral"

    def __init__(self, concept):
        self.concept = concept
        self.id = useId()
        self.conceptRefs = []

    def process(self, facts=None):
        triples = [(self.id, "a", QUANTIFIER_IRI), (self.id, NUM_IRI, QUANT_ALL_VALUE)]

        (conceptIds, conceptTriples) = self.concept.process(facts)
        self.conceptRefs = getDNFList(conceptIds)

        for ref in self.conceptRefs:
            for c in ref:
                triples.append((c, QUANT_IRI, self.id))

        triples.extend(conceptTriples)
        return conceptIds, triples

    @staticmethod
    def fromTree(tree):
        if tree[0] != InstanceGeneral.treeName or len(tree) != 2:
            raise Exception("Cannot create " + InstanceGeneral.treeName + " from given tree " + str(tree))

        return InstanceGeneral(Nodes.ConceptNodes.conceptFromTree(tree[1]))


class InstanceDigits:
    treeName = "InstanceDigits"

    def __init__(self, concept, num):
        self.concept = concept
        self.num = num
        self.id = useId()
        self.conceptRefs = []

    def process(self, facts=None):
        triples = [(self.id, "a", QUANTIFIER_IRI), (self.id, NUM_IRI, self.num)]

        (conceptIds, conceptTriples) = self.concept.process(facts)
        self.conceptRefs = getDNFList(conceptIds)

        for ref in self.conceptRefs:
            for c in ref:
                triples.append((c, QUANT_IRI, self.id))

        triples.extend(conceptTriples)
        return conceptIds, triples

    @staticmethod
    def fromTree(tree):
        if tree[0] != InstanceDigits.treeName or len(tree) != 3:
            raise Exception("Cannot create " + InstanceDigits.treeName + " from given tree " + str(tree))

        return InstanceDigits(Nodes.ConceptNodes.conceptFromTree(tree[1]), tree[2])


class InstanceNumeral:
    treeName = "InstanceNumeral"

    def __init__(self, concept, num):
        self.concept = concept
        self.num = num
        self.id = useId()
        self.conceptRefs = []

    def process(self, facts=None):
        triples = [(self.id, "a", QUANTIFIER_IRI), (self.id, NUM_IRI, self.num)]

        (conceptIds, conceptTriples) = self.concept.process(facts)
        self.conceptRefs = getDNFList(conceptIds)

        for ref in self.conceptRefs:
            for c in ref:
                triples.append((c, QUANT_IRI, self.id))

        triples.extend(conceptTriples)
        return conceptIds, triples

    @staticmethod
    def fromTree(tree):
        if tree[0] != InstanceNumeral.treeName or len(tree) != 3:
            raise Exception("Cannot create " + InstanceNumeral.treeName + " from given tree " + str(tree))

        return InstanceNumeral(Nodes.ConceptNodes.conceptFromTree(tree[1]), numeralToInt(tree[2]))


class InstanceCard:
    treeName = "InstanceCard"

    def __init__(self, concept, num):
        self.concept = concept
        self.num = num
        self.id = useId()
        self.conceptRefs = []

    def process(self, facts=None):
        triples = [(self.id, "a", QUANTIFIER_IRI), (self.id, NUM_IRI, self.num)]

        (conceptIds, conceptTriples) = self.concept.process(facts)
        self.conceptRefs = getDNFList(conceptIds)

        for ref in self.conceptRefs:
            for c in ref:
                triples.append((c, QUANT_IRI, self.id))

        triples.extend(conceptTriples)
        return conceptIds, triples

    @staticmethod
    def fromTree(tree):
        if tree[0] != InstanceCard.treeName or len(tree) != 3:
            raise Exception("Cannot create " + InstanceCard.treeName + " from given tree " + str(tree))

        return InstanceCard(Nodes.ConceptNodes.conceptFromTree(tree[1]), Nodes.MiscNodes.intFromCardTree(tree[2]))


class InstanceAnd:
    treeName = "And"

    def __init__(self, c1, c2):
        self.c1 = c1
        self.c2 = c2
        self.id = useId()

    def process(self, facts=None):
        (c1Ids, c1Triples) = self.c1.process(facts)
        (c2Ids, c2Triples) = self.c2.process(facts)

        triples = []
        triples.extend(c1Triples)
        triples.extend(c2Triples)

        return "(" + c1Ids + ") & (" + c2Ids + ")", triples

    @staticmethod
    def fromTree(tree):
        if tree[0] != InstanceAnd.treeName or len(tree) != 3:
            raise Exception("Cannot create " + InstanceAnd.treeName + " from given tree " + str(tree))

        return InstanceAnd(Nodes.InstanceNodes.instanceFromTree(tree[1]), Nodes.InstanceNodes.instanceFromTree(tree[2]))


class InstanceOr:
    treeName = "Or"

    def __init__(self, c1, c2):
        self.c1 = c1
        self.c2 = c2
        self.id = useId()

    def process(self, facts=None):
        (c1Ids, c1Triples) = self.c1.process(facts)
        (c2Ids, c2Triples) = self.c2.process(facts)

        triples = []
        triples.extend(c1Triples)
        triples.extend(c2Triples)

        return "(" + c1Ids + ") | (" + c2Ids + ")", triples

    @staticmethod
    def fromTree(tree):
        if tree[0] != InstanceOr.treeName or len(tree) != 3:
            raise Exception("Cannot create " + InstanceOr.treeName + " from given tree " + str(tree))

        return InstanceOr(Nodes.InstanceNodes.instanceFromTree(tree[1]), Nodes.InstanceNodes.instanceFromTree(tree[2]))


class Location:
    treeName = "Location"

    def __init__(self, c, loc):
        self.c = c
        self.loc = loc
        self.id = useId()
        self.cRefs = []
        self.locRefs = []

    def process(self, facts=None):
        triples = []

        (cIds, cTriples) = self.c.process(facts)
        self.cRefs = getDNFList(cIds)

        (locIds, locTriples) = self.loc.process(facts)
        self.locRefs = getDNFList(locIds)

        if locIds is not None:
            for refs in self.cRefs:
                for ref in refs:
                    triples.append((ref, LOC_IRI, locIds))

        triples.extend(cTriples)
        triples.extend(locTriples)
        return cIds, triples

    @staticmethod
    def fromTree(tree):
        if tree[0] != Location.treeName or len(tree) != 3:
            raise Exception("Cannot create " + Location.treeName + " from given tree " + str(tree))

        return Location(Nodes.InstanceNodes.instanceFromTree(tree[1]), Nodes.InstanceNodes.instanceFromTree(tree[2]))


class Target:
    treeName = "Target"

    def __init__(self, i, t):
        self.i = i
        self.t = t
        self.id = useId()
        self.iRefs = []
        self.tRefs = []

    def process(self, facts=None):
        triples = []

        (iIds, iTriples) = self.i.process(facts)
        self.iRefs = getDNFList(iIds)

        (tIds, tTriples) = self.t.process(facts)
        self.tRefs = getDNFList(tIds)

        if tIds is not None:
            for refs in self.iRefs:
                for ref in refs:
                    triples.append((ref, TARGET_IRI, tIds))

        triples.extend(iTriples)
        triples.extend(tTriples)
        return iIds, triples

    @staticmethod
    def fromTree(tree):
        if tree[0] != Target.treeName or len(tree) != 3:
            raise Exception("Cannot create " + Target.treeName + " from given tree " + str(tree))

        return Target(Nodes.InstanceNodes.instanceFromTree(tree[1]), Nodes.InstanceNodes.instanceFromTree(tree[2]))


class InstancePercentage:
    treeName = "InstancePercentage"

    def __init__(self, c, p):
        self.c = c
        self.p = p
        self.id = useId()
        self.cRefs = []
        self.pRefs = []

    def process(self, facts=None):
        triples = []

        (cIds, cTriples) = self.c.process(facts)
        self.cRefs = getDNFList(cIds)

        (pIds, pTriples) = self.p.process()
        self.pRefs = getDNFList(pIds)

        for refs in self.cRefs:
            for ref in refs:
                for pRefs in self.pRefs:
                    for pRef in pRefs:
                        triples.append((ref, QUANT_IRI, pRef))

        triples.extend(cTriples)
        triples.extend(pTriples)
        return cIds, triples

    @staticmethod
    def fromTree(tree):
        if tree[0] != InstancePercentage.treeName or len(tree) != 3:
            raise Exception("Cannot create " + InstancePercentage.treeName + " from given tree " + str(tree))

        return InstancePercentage(Nodes.InstanceNodes.instanceFromTree(tree[1]), Nodes.MiscNodes.percentageFromTree(tree[2]))


class CommaDescription:
    treeName = "CommaDescription"

    def __init__(self, i, adj):
        self.i = i
        self.adj = adj
        self.id = useId()
        self.iRefs = []

    def process(self, facts=None):
        (iIds, iTriples) = self.i.process(facts)
        self.iRefs = getDNFList(iIds)

        propertyId = useId()

        (adjS, adjTriples) = self.adj.process(propertyId)

        triples = [(propertyId, "a", PROPERTY_IRI), (propertyId, ADJ_IRI, adjS)]

        for refs in self.iRefs:
            for ref in refs:
                triples.append((ref, PROP_IRI, propertyId))

        triples.extend(iTriples)
        triples.extend(adjTriples)
        return iIds, triples

    @staticmethod
    def fromTree(tree):
        if tree[0] != CommaDescription.treeName or len(tree) != 3:
            raise Exception("Cannot create " + CommaDescription.treeName + " from given tree " + str(tree))

        return CommaDescription(Nodes.InstanceNodes.instanceFromTree(tree[1]), Nodes.MiscNodes.adjFromTree(tree[2]))


class CommaDescriptionHometownMasc:
    treeName = "CommaDescriptionHometownMasc"

    def __init__(self, i, c):
        self.i = i
        self.c = c
        self.id = useId()
        self.iRefs = []

    def process(self, facts=None):
        (iIds, iTriples) = self.i.process(facts)
        self.iRefs = getDNFList(iIds)

        propertyId = useId()
        neId = useId()
        triples = [(neId, "a", NE_CONCEPT_IRI), (neId, CONC_IRI, self.c), (propertyId, "a", PROPERTY_IRI), (propertyId, ADJ_IRI, "wohnhaft_A"), (propertyId, LOC_IRI, neId)]

        for refs in self.iRefs:
            for ref in refs:
                triples.append((ref, PROP_IRI, propertyId))

        triples.extend(iTriples)
        return iIds, triples

    @staticmethod
    def fromTree(tree):
        if tree[0] != CommaDescriptionHometownMasc.treeName or len(tree) != 3:
            raise Exception("Cannot create " + CommaDescriptionHometownMasc.treeName + " from given tree " + str(tree))

        return CommaDescriptionHometownMasc(Nodes.InstanceNodes.instanceFromTree(tree[1]), tree[2][1:len(tree[2])-1].replace("&+", " "))


class CommaDescriptionHometownFem:
    treeName = "CommaDescriptionHometownFem"

    @staticmethod
    def fromTree(tree):
        if tree[0] != CommaDescriptionHometownFem.treeName or len(tree) != 3:
            raise Exception("Cannot create " + CommaDescriptionHometownFem.treeName + " from given tree " + str(tree))

        return CommaDescriptionHometownMasc(Nodes.InstanceNodes.instanceFromTree(tree[1]), tree[2][1:len(tree[2])-1].replace("&+", " "))


class CommaDescriptionHometownNeut:
    treeName = "CommaDescriptionHometownNeut"

    @staticmethod
    def fromTree(tree):
        if tree[0] != CommaDescriptionHometownNeut.treeName or len(tree) != 3:
            raise Exception("Cannot create " + CommaDescriptionHometownNeut.treeName + " from given tree " + str(tree))

        return CommaDescriptionHometownMasc(Nodes.InstanceNodes.instanceFromTree(tree[1]), tree[2][1:len(tree[2])-1].replace("&+", " "))


class CommaDescriptionHometown2:
    treeName = "CommaDescriptionHometown2"

    def __init__(self, i, t):
        self.i = i
        self.t = t
        self.id = useId()
        self.iRefs = []
        self.tRefs = []

    def process(self, facts=None):
        (iIds, iTriples) = self.i.process(facts)
        self.iRefs = getDNFList(iIds)

        (tIds, tTriples) = self.t.process(facts)
        self.tRefs = getDNFList(tIds)

        propertyId = useId()
        triples = [(propertyId, "a", PROPERTY_IRI), (propertyId, ADJ_IRI, "wohnhaft_A"), (propertyId, LOC_IRI, tIds)]

        for refs in self.iRefs:
            for ref in refs:
                triples.append((ref, PROP_IRI, propertyId))

        triples.extend(iTriples)
        triples.extend(tTriples)
        return iIds, triples

    @staticmethod
    def fromTree(tree):
        if tree[0] != CommaDescriptionHometown2.treeName or len(tree) != 3:
            raise Exception("Cannot create " + CommaDescriptionHometown2.treeName + " from given tree " + str(tree))

        return CommaDescriptionHometown2(Nodes.InstanceNodes.instanceFromTree(tree[1]), Nodes.InstanceNodes.instanceFromTree(tree[2]))


class CommaDescriptionBirthday:
    treeName = "CommaDescriptionBirthday"

    def __init__(self, i, d):
        self.i = i
        self.d = d
        self.id = useId()
        self.iRefs = []
        self.dRefs = []

    def process(self, facts=None):
        (iIds, iTriples) = self.i.process(facts)
        self.iRefs = getDNFList(iIds)

        propertyId = useId()

        (dIds, dTriples) = self.d.process(facts)
        self.dRefs = getDNFList(dIds)

        triples = [(propertyId, "a", PROPERTY_IRI), (propertyId, ADJ_IRI, "geboren_A"), (propertyId, DATE_PROP_IRI, dIds)]

        for refs in self.iRefs:
            for ref in refs:
                triples.append((ref, PROP_IRI, propertyId))

        triples.extend(iTriples)
        triples.extend(dTriples)
        return iIds, triples

    @staticmethod
    def fromTree(tree):
        if tree[0] != CommaDescriptionBirthday.treeName or len(tree) != 3:
            raise Exception("Cannot create " + CommaDescriptionBirthday.treeName + " from given tree " + str(tree))

        return CommaDescriptionBirthday(Nodes.InstanceNodes.instanceFromTree(tree[1]), Nodes.ConceptNodes.dateFromTree(tree[2]))


class Origin:
    treeName = "Origin"

    def __init__(self, i, o):
        self.i = i
        self.o = o
        self.id = useId()
        self.iRefs = []
        self.oRefs = []

    def process(self, facts=None):
        triples = []

        (iIds, iTriples) = self.i.process(facts)
        self.iRefs = getDNFList(iIds)

        (oIds, oTriples) = self.o.process(facts)
        self.oRefs = getDNFList(oIds)

        if oIds is not None:
            for refs in self.iRefs:
                for ref in refs:
                    triples.append((ref, ORIGIN_IRI, oIds))

        triples.extend(iTriples)
        triples.extend(oTriples)
        return iIds, triples

    @staticmethod
    def fromTree(tree):
        if tree[0] != Origin.treeName or len(tree) != 3:
            raise Exception("Cannot create " + Origin.treeName + " from given tree " + str(tree))

        return Origin(Nodes.InstanceNodes.instanceFromTree(tree[1]), Nodes.InstanceNodes.instanceFromTree(tree[2]))


class AccordingTo:
    treeName = "AccordingTo"

    def __init__(self, i, o):
        self.i = i
        self.o = o
        self.id = useId()
        self.iRefs = []
        self.oRefs = []

    def process(self, facts=None):
        triples = []

        (iIds, iTriples) = self.i.process(facts)
        self.iRefs = getDNFList(iIds)

        (oIds, oTriples) = self.o.process(facts)
        self.oRefs = getDNFList(oIds)

        if oIds is not None:
            for refs in self.iRefs:
                for ref in refs:
                    triples.append((ref, ACCORDING_TO_IRI, oIds))

        triples.extend(iTriples)
        triples.extend(oTriples)
        return iIds, triples

    @staticmethod
    def fromTree(tree):
        if tree[0] != AccordingTo.treeName or len(tree) != 3:
            raise Exception("Cannot create " + AccordingTo.treeName + " from given tree " + str(tree))

        return AccordingTo(Nodes.InstanceNodes.instanceFromTree(tree[1]), Nodes.InstanceNodes.instanceFromTree(tree[2]))


class Besides:
    treeName = "Besides"

    def __init__(self, i, o):
        self.i = i
        self.o = o
        self.id = useId()
        self.iRefs = []
        self.oRefs = []

    def process(self, facts=None):
        triples = []

        (iIds, iTriples) = self.i.process(facts)
        self.iRefs = getDNFList(iIds)

        (oIds, oTriples) = self.o.process(facts)
        self.oRefs = getDNFList(oIds)

        if oIds is not None:
            for refs in self.iRefs:
                for ref in refs:
                    triples.append((ref, BESIDES_IRI, oIds))

        triples.extend(iTriples)
        triples.extend(oTriples)
        return iIds, triples

    @staticmethod
    def fromTree(tree):
        if tree[0] != Besides.treeName or len(tree) != 3:
            raise Exception("Cannot create " + Besides.treeName + " from given tree " + str(tree))

        return Besides(Nodes.InstanceNodes.instanceFromTree(tree[1]), Nodes.InstanceNodes.instanceFromTree(tree[2]))


class AccordingToLawSection:
    treeName = "AccordingToLawSection"

    def __init__(self, i, ls):
        self.i = i
        self.ls = ls
        self.id = useId()
        self.iRefs = []
        self.lsRefs = []

    def process(self, facts=None):
        triples = []

        (iIds, iTriples) = self.i.process(facts)
        self.iRefs = getDNFList(iIds)

        (lsIds, lsTriples) = self.ls.process(facts)
        self.lsRefs = getDNFList(lsIds)

        if lsIds is not None:
            for refs in self.iRefs:
                for ref in refs:
                    triples.append((ref, ACCORDING_TO_IRI, lsIds))

        triples.extend(iTriples)
        triples.extend(lsTriples)
        return iIds, triples

    @staticmethod
    def fromTree(tree):
        if tree[0] != AccordingToLawSection.treeName or len(tree) != 3:
            raise Exception("Cannot create " + AccordingToLawSection.treeName + " from given tree " + str(tree))

        return AccordingToLawSection(Nodes.InstanceNodes.instanceFromTree(tree[1]), Nodes.ConceptNodes.lawSectionFromTree(tree[2]))
