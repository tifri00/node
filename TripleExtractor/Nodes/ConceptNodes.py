import Nodes
import Nodes.MiscNodes
import Nodes.ActionNodes
import Nodes.InstanceNodes
from Utility import useId, getDNFList
from constants import *

nodes = []


def init(nodeList):
    global nodes
    nodes = nodeList


def conceptFromTree(tree):
    if tree[0] in ["ConceptAnd2"]:
        tree[0] = "ConceptAnd"
    elif tree[0] in ["Owner2", "Owner3"]:
        tree[0] = "Owner"
    elif tree[0] in ["HRBNr2"]:
        tree[0] = "HRBNr"

    conceptNodes = list(filter(lambda x: x.treeName == tree[0], nodes))

    if conceptNodes is None or len(conceptNodes) == 0:
        raise Exception("Cannot construct concept from given tree " + str(tree))

    return conceptNodes[0].fromTree(tree)


def lawSectionFromTree(tree):
    if tree[0] in ["LawSectionWithParagraphAndSentence2"]:
        tree[0] = "LawSectionWithParagraphAndSentence"
    elif tree[0] in ["LawSectionNumOnlyWithParagraphAndSentence2"]:
        tree[0] = "LawSectionNumOnlyWithParagraphAndSentence"

    lawSectionNodes = list(filter(lambda x: x.treeName == tree[0] and x.treeName in ["LawSection", "LawSectionWithParagraph", "LawSectionWithParagraphAndSentence", "LawSectionNumOnly", "LawSectionNumOnlyWithParagraph", "LawSectionNumOnlyWithParagraphAndSentence"], nodes))

    if lawSectionNodes is None or len(lawSectionNodes) == 0:
        raise Exception("Cannot construct law section from given tree " + str(tree))

    return lawSectionNodes[0].fromTree(tree)


def namedEntityFromTree(tree):
    if tree[0] in ["NEConceptWithSaluteMasc", "NEConceptWithSaluteFem", "NEConceptWithSaluteNeut"]:
        tree[0] = "NEConceptWithSalute"

    namedEntityNodes = list(filter(lambda x: x.treeName == tree[0] and x.treeName in ["NEConcept", "NEConceptMasc", "NEConceptFem", "NEConceptNeut", "NEConceptWithSalute"], nodes))

    if namedEntityNodes is None or len(namedEntityNodes) == 0:
        raise Exception("Cannot construct named entity from given tree " + str(tree))

    return namedEntityNodes[0].fromTree(tree)


def dateFromTree(tree):
    dateNodes = list(filter(lambda x: x.treeName == tree[0] and x.treeName in ["Date", "Date2"], nodes))

    if dateNodes is None or len(dateNodes) == 0:
        raise Exception("Cannot construct date from given tree " + str(tree))

    return dateNodes[0].fromTree(tree)


def monthIntFromName(m):
    months = ["januar_CMonth", "februar_CMonth", "maerz_CMonth", "april_CMonth", "mai_CMonth",
              "juni_CMonth", "juli_CMonth", "august_CMonth", "september_CMonth", "oktober_CMonth",
              "november_CMonth", "dezember_CMonth"]

    return months.index(m) + 1


class Concept:
    treeName = "Concept"

    def __init__(self, noun):
        self.noun = noun
        self.id = useId()

    def process(self, facts=None):
        triples = [(self.id, "a", CONCEPT_IRI), (self.id, CONC_IRI, self.noun)]

        return self.id, triples

    @staticmethod
    def fromTree(tree):
        if tree[0] != Concept.treeName or len(tree) != 2:
            raise Exception("Cannot create " + Concept.treeName + " from given tree " + str(tree))

        return Concept(tree[1])


class ConceptAnd:
    treeName = "ConceptAnd"

    def __init__(self, c1, c2):
        self.c1 = c1
        self.c2 = c2
        self.id = useId()
        self.c1Refs = []
        self.c2Refs = []

    def process(self, facts=None):
        triples = []

        (c1Ids, c1Triples) = self.c1.process(facts)
        self.c1Refs = getDNFList(c1Ids)

        (c2Ids, c2Triples) = self.c2.process(facts)
        self.c2Refs = getDNFList(c2Ids)

        triples.extend(c1Triples)
        triples.extend(c2Triples)
        return f"({c1Ids}) & ({c2Ids})", triples

    @staticmethod
    def fromTree(tree):
        if tree[0] != ConceptAnd.treeName or len(tree) != 3:
            raise Exception("Cannot create " + ConceptAnd.treeName + " from given tree " + str(tree))

        return ConceptAnd(conceptFromTree(tree[1]), conceptFromTree(tree[2]))


class ConceptOr:
    treeName = "ConceptOr"

    def __init__(self, c1, c2):
        self.c1 = c1
        self.c2 = c2
        self.id = useId()
        self.c1Refs = []
        self.c2Refs = []

    def process(self, facts=None):
        triples = []

        (c1Ids, c1Triples) = self.c1.process(facts)
        self.c1Refs = getDNFList(c1Ids)

        (c2Ids, c2Triples) = self.c2.process(facts)
        self.c2Refs = getDNFList(c2Ids)

        triples.extend(c1Triples)
        triples.extend(c2Triples)
        return f"({c1Ids}) | ({c2Ids})", triples

    @staticmethod
    def fromTree(tree):
        if tree[0] != ConceptOr.treeName or len(tree) != 3:
            raise Exception("Cannot create " + ConceptOr.treeName + " from given tree " + str(tree))

        return ConceptOr(conceptFromTree(tree[1]), conceptFromTree(tree[2]))


class NEConcept:
    treeName = "NEConcept"

    def __init__(self, name):
        self.name = name
        self.id = useId()

    def process(self, facts=None):
        triples = [(self.id, "a", NE_CONCEPT_IRI), (self.id, CONC_IRI, self.name)]

        return self.id, triples

    @staticmethod
    def fromTree(tree):
        if tree[0] != NEConcept.treeName or len(tree) != 2:
            raise Exception("Cannot create " + NEConcept.treeName + " from given tree " + str(tree))

        return NEConcept(tree[1][1:len(tree[1])-1].replace("&+", " "))


class NEConceptMasc:
    treeName = "NEConceptMasc"

    @staticmethod
    def fromTree(tree):
        if tree[0] != NEConceptMasc.treeName or len(tree) != 2:
            raise Exception("Cannot create " + NEConceptMasc.treeName + " from given tree " + str(tree))

        return NEConcept(tree[1][1:len(tree[1])-1].replace("&+", " "))


class NEConceptFem:
    treeName = "NEConceptFem"

    @staticmethod
    def fromTree(tree):
        if tree[0] != NEConceptFem.treeName or len(tree) != 2:
            raise Exception("Cannot create " + NEConceptFem.treeName + " from given tree " + str(tree))

        return NEConcept(tree[1][1:len(tree[1])-1].replace("&+", " "))


class NEConceptNeut:
    treeName = "NEConceptNeut"

    @staticmethod
    def fromTree(tree):
        if tree[0] != NEConceptNeut.treeName or len(tree) != 2:
            raise Exception("Cannot create " + NEConceptNeut.treeName + " from given tree " + str(tree))

        return NEConcept(tree[1][1:len(tree[1])-1].replace("&+", " "))


class NEConceptWithSalute:
    treeName = "NEConceptWithSalute"

    def __init__(self, salute, name):
        self.salute = salute
        self.name = name
        self.id = useId()

    def process(self, facts=None):
        triples = [(self.id, "a", NE_CONCEPT_IRI),
                   (self.id, CONC_IRI, self.name),
                   (self.id, CONC_IRI, self.salute)]

        return self.id, triples

    @staticmethod
    def fromTree(tree):
        if tree[0] != NEConceptWithSalute.treeName or len(tree) != 3:
            raise Exception("Cannot create " + NEConceptWithSalute.treeName + " from given tree " + str(tree))

        return NEConceptWithSalute(tree[1], tree[2][1:len(tree[2])-1].replace("&+", " "))


class Prop:
    treeName = "Prop"

    def __init__(self, c, a):
        self.c = c
        self.a = a
        self.id = useId()
        self.cRefs = []

    def process(self, facts=None):
        (cIds, cTriples) = self.c.process(facts)
        self.cRefs = getDNFList(cIds)

        (adjS, adjTriples) = self.a.process(self.id, None)

        triples = [(self.id, "a", PROPERTY_IRI), (self.id, ADJ_IRI, adjS)]

        for refs in self.cRefs:
            for ref in refs:
                triples.append((ref, PROP_IRI, self.id))

        triples.extend(cTriples)
        triples.extend(adjTriples)
        return cIds, triples

    @staticmethod
    def fromTree(tree):
        if tree[0] != Prop.treeName or len(tree) != 3:
            raise Exception("Cannot create " + Prop.treeName + " from given tree " + str(tree))

        return Prop(conceptFromTree(tree[1]), Nodes.MiscNodes.adjFromTree(tree[2]))


class ActionProp:
    treeName = "ActionProp"

    @staticmethod
    def fromTree(tree):
        if tree[0] != ActionProp.treeName or len(tree) != 3:
            raise Exception("Cannot create " + ActionProp.treeName + " from given tree " + str(tree))

        return ConceptRel(conceptFromTree(tree[1]), Nodes.ActionNodes.actionFromTree(tree[2]))


class PassActionProp2:
    treeName = "PassActionProp2"

    def __init__(self, c, v):
        self.c = c
        self.v = v
        self.id = useId()
        self.cRefs = []

    def process(self, facts=None):
        (cIds, cTriples) = self.c.process(facts)
        self.cRefs = getDNFList(cIds)

        factId = useId()
        actId = useId()
        triples = [(factId, "a", FACT_IRI), (factId, ACT_IRI, actId), (actId, "a", ACTION_IRI), (actId, PRED_IRI, self.v), (actId, OBJ_IRI, cIds)]

        for refs in self.cRefs:
            for ref in refs:
                triples.append((ref, RELCL_IRI, factId))

        triples.extend(cTriples)
        return cIds, triples

    @staticmethod
    def fromTree(tree):
        if tree[0] != PassActionProp2.treeName or len(tree) != 3:
            raise Exception("Cannot create " + PassActionProp2.treeName + " from given tree " + str(tree))

        return PassActionProp2(conceptFromTree(tree[1]), tree[2])


class PassActionProp3:
    treeName = "PassActionProp3"

    def __init__(self, c, v, ind):
        self.c = c
        self.v = v
        self.ind = ind
        self.id = useId()
        self.cRefs = []
        self.indRefs = []

    def process(self, facts=None):
        (cIds, cTriples) = self.c.process(facts)
        self.cRefs = getDNFList(cIds)

        (indIds, indTriples) = self.ind.process(facts)
        self.indRefs = getDNFList(indIds)

        factId = useId()
        actId = useId()
        triples = [(factId, "a", FACT_IRI), (factId, ACT_IRI, actId), (actId, "a", ACTION_IRI), (actId, PRED_IRI, self.v), (actId, OBJ_IRI, cIds), (actId, INDIR_OBJ_IRI, indIds)]

        for refs in self.cRefs:
            for ref in refs:
                triples.append((ref, RELCL_IRI, factId))

        triples.extend(cTriples)
        triples.extend(indTriples)
        return cIds, triples

    @staticmethod
    def fromTree(tree):
        if tree[0] != PassActionProp3.treeName or len(tree) != 4:
            raise Exception("Cannot create " + PassActionProp3.treeName + " from given tree " + str(tree))

        return PassActionProp3(conceptFromTree(tree[1]), tree[2], Nodes.InstanceNodes.instanceFromTree(tree[3]))


class LawSection:
    treeName = "LawSection"

    def __init__(self, n, l):
        self.n = n
        self.l = l
        self.id = useId()

    def process(self, facts=None):
        triples = [(self.id, "a", LAW_SECTION_IRI), (self.id, NUM_IRI, self.n), (self.id, LAW_IRI, self.l)]

        return self.id, triples

    @staticmethod
    def fromTree(tree):
        if tree[0] != LawSection.treeName or len(tree) != 3:
            raise Exception("Cannot create " + LawSection.treeName + " from given tree " + str(tree))

        return LawSection(tree[1], tree[2])


class LawSectionNumOnly:
    treeName = "LawSectionNumOnly"

    def __init__(self, n):
        self.n = n
        self.id = useId()

    def process(self, facts=None):
        triples = [(self.id, "a", LAW_SECTION_IRI), (self.id, NUM_IRI, self.n)]

        return self.id, triples

    @staticmethod
    def fromTree(tree):
        if tree[0] != LawSectionNumOnly.treeName or len(tree) != 3:
            raise Exception("Cannot create " + LawSectionNumOnly.treeName + " from given tree " + str(tree))

        return LawSectionNumOnly(tree[1])


class LawSectionWithParagraph:
    treeName = "LawSectionWithParagraph"

    def __init__(self, s, p, l):
        self.s = s
        self.p = p
        self.l = l
        self.id = useId()

    def process(self, facts=None):
        triples = [(self.id, "a", LAW_SECTION_IRI),
                   (self.id, NUM_IRI, self.s),
                   (self.id, PARAGRAPH_IRI, self.p),
                   (self.id, LAW_IRI, self.l)]

        return self.id, triples

    @staticmethod
    def fromTree(tree):
        if tree[0] != LawSectionWithParagraph.treeName or len(tree) != 4:
            raise Exception("Cannot create " + LawSectionWithParagraph.treeName + " from given tree " + str(tree))

        return LawSectionWithParagraph(tree[1], tree[2], tree[3])


class LawSectionWithParagraphAndSentence:
    treeName = "LawSectionWithParagraphAndSentence"

    def __init__(self, s, p, sen, l):
        self.s = s
        self.p = p
        self.sen = sen
        self.l = l
        self.id = useId()

    def process(self, facts=None):
        triples = [(self.id, "a", LAW_SECTION_IRI),
                   (self.id, NUM_IRI, self.s),
                   (self.id, PARAGRAPH_IRI, self.p),
                   (self.id, SENTENCE_IRI, self.sen),
                   (self.id, LAW_IRI, self.l)]

        return self.id, triples

    @staticmethod
    def fromTree(tree):
        if tree[0] != LawSectionWithParagraphAndSentence.treeName or len(tree) != 5:
            raise Exception("Cannot create " + LawSectionWithParagraphAndSentence.treeName + " from given tree " + str(tree))

        return LawSectionWithParagraphAndSentence(tree[1], tree[2], tree[3], tree[4])


class LawSectionNumOnlyWithParagraph:
    treeName = "LawSectionNumOnlyWithParagraph"

    def __init__(self, s, p):
        self.s = s
        self.p = p
        self.id = useId()

    def process(self, facts=None):
        triples = [(self.id, "a", LAW_SECTION_IRI),
                   (self.id, NUM_IRI, self.s),
                   (self.id, PARAGRAPH_IRI, self.p)]

        return self.id, triples

    @staticmethod
    def fromTree(tree):
        if tree[0] != LawSectionNumOnlyWithParagraph.treeName or len(tree) != 3:
            raise Exception("Cannot create " + LawSectionNumOnlyWithParagraph.treeName + " from given tree " + str(tree))

        return LawSectionNumOnlyWithParagraph(tree[1], tree[2])


class LawSectionNumOnlyWithParagraphAndSentence:
    treeName = "LawSectionNumOnlyWithParagraphAndSentence"

    def __init__(self, s, p, sen):
        self.s = s
        self.p = p
        self.sen = sen
        self.id = useId()

    def process(self, facts=None):
        triples = [(self.id, "a", LAW_SECTION_IRI),
                   (self.id, NUM_IRI, self.s),
                   (self.id, PARAGRAPH_IRI, self.p),
                   (self.id, SENTENCE_IRI, self.sen)]

        return self.id, triples

    @staticmethod
    def fromTree(tree):
        if tree[0] != LawSectionNumOnlyWithParagraphAndSentence.treeName or len(tree) != 4:
            raise Exception("Cannot create " + LawSectionNumOnlyWithParagraphAndSentence.treeName + " from given tree " + str(tree))

        return LawSectionNumOnlyWithParagraphAndSentence(tree[1], tree[2], tree[3])

class Owner:
    treeName = "Owner"

    def __init__(self, c, owner):
        self.c = c
        self.owner = owner
        self.id = useId()
        self.cRefs = []
        self.ownerRefs = []

    def process(self, facts=None):
        triples = []

        (cIds, cTriples) = self.c.process(facts)
        self.cRefs = getDNFList(cIds)

        (ownerIds, ownerTriples) = self.owner.process(facts)
        self.ownerRefs = getDNFList(ownerIds)

        if ownerIds is not None:
            for refs in self.cRefs:
                for ref in refs:
                    triples.append((ref, OWNER_IRI, ownerIds))

        triples.extend(cTriples)
        triples.extend(ownerTriples)
        return cIds, triples

    @staticmethod
    def fromTree(tree):
        if tree[0] != Owner.treeName or len(tree) != 3:
            raise Exception("Cannot create " + Owner.treeName + " from given tree " + str(tree))

        return Owner(Nodes.ConceptNodes.conceptFromTree(tree[1]), Nodes.InstanceNodes.instanceFromTree(tree[2]))


class Date:
    treeName = "Date"

    def __init__(self, d, m, y):
        self.d = d
        self.m = m
        self.y = y
        self.id = useId()

    def process(self, facts=None):
        triples = [(self.id, "a", DATE_IRI),
                   (self.id, DATE_DAY_IRI, self.d),
                   (self.id, DATE_MONTH_IRI, self.m),
                   (self.id, DATE_YEAR_IRI, self.y)]

        return self.id, triples

    @staticmethod
    def fromTree(tree):
        if tree[0] != Date.treeName or len(tree) != 4:
            raise Exception("Cannot create " + Date.treeName + " from given tree " + str(tree))

        return Date(tree[1], tree[2], tree[3])


class Date2:
    treeName = "Date2"

    @staticmethod
    def fromTree(tree):
        if tree[0] != Date2.treeName or len(tree) != 4:
            raise Exception("Cannot create " + Date2.treeName + " from given tree " + str(tree))

        return Date(tree[1], monthIntFromName(tree[2]), tree[3])


class HRBNr:
    treeName = "HRBNr"

    def __init__(self, n):
        self.n = n
        self.id = useId()

    def process(self, facts=None):
        triples = [(self.id, "a", HRB_ENTRY_IRI),
                   (self.id, NUM_IRI, self.n)]

        return self.id, triples

    @staticmethod
    def fromTree(tree):
        if tree[0] != HRBNr.treeName or len(tree) != 2:
            raise Exception("Cannot create " + HRBNr.treeName + " from given tree " + str(tree))

        return HRBNr(tree[1])


class Price:
    treeName = "Price"

    def __init__(self, n):
        self.n = n
        self.id = useId()

    def process(self, facts=None):
        triples = [(self.id, "a", PRICE_IRI),
                   (self.id, EURO_IRI, self.n),
                   (self.id, CENT_IRI, "0")]

        return self.id, triples

    @staticmethod
    def fromTree(tree):
        if tree[0] != Price.treeName or len(tree) != 2:
            raise Exception("Cannot create " + Price.treeName + " from given tree " + str(tree))

        return Price(tree[1])


class Price2:
    treeName = "Price2"

    @staticmethod
    def fromTree(tree, facts=None):
        if tree[0] != Price2.treeName or len(tree) != 2:
            raise Exception("Cannot create " + Price2.treeName + " from given tree " + str(tree))

        return Price(tree[1])


class Price3:
    treeName = "Price3"

    @staticmethod
    def fromTree(tree):
        if tree[0] != Price3.treeName or len(tree) != 2:
            raise Exception("Cannot create " + Price3.treeName + " from given tree " + str(tree))

        return Price(tree[1])


class Price4:
    treeName = "Price4"

    @staticmethod
    def fromTree(tree):
        if tree[0] != Price4.treeName or len(tree) != 2:
            raise Exception("Cannot create " + Price4.treeName + " from given tree " + str(tree))

        return Price(tree[1])


class PriceComma:
    treeName = "PriceComma"

    def __init__(self, e, c):
        self.e = e
        self.c = c
        self.id = useId()

    def process(self, facts=None):
        triples = [(self.id, "a", PRICE_IRI),
                   (self.id, EURO_IRI, self.e),
                   (self.id, CENT_IRI, self.c)]

        return self.id, triples

    @staticmethod
    def fromTree(tree):
        if tree[0] != PriceComma.treeName or len(tree) != 3:
            raise Exception("Cannot create " + PriceComma.treeName + " from given tree " + str(tree))

        return PriceComma(tree[1], tree[2])


class PriceComma2:
    treeName = "PriceComma2"

    @staticmethod
    def fromTree(tree):
        if tree[0] != PriceComma2.treeName or len(tree) != 3:
            raise Exception("Cannot create " + PriceComma2.treeName + " from given tree " + str(tree))

        return PriceComma(tree[1], tree[2])


class ConceptRel:
    treeName = "ConceptRel"

    def __init__(self, c, action):
        self.c = c
        self.action = action
        self.id = useId()
        self.cRefs = []
        self.actionRefs = []

    def process(self, facts=None):
        (cIds, cTriples) = self.c.process(facts)
        self.cRefs = getDNFList(cIds)

        (actionIds, actionTriples) = self.action.process(facts)
        self.actionRefs = getDNFList(actionIds)

        factId = useId()
        triples = [(factId, "a", FACT_IRI), (factId, SUBJ_IRI, cIds), (factId, ACT_IRI, actionIds)]

        for refs in self.cRefs:
            for ref in refs:
                triples.append((ref, RELCL_IRI, factId))

        triples.extend(cTriples)
        triples.extend(actionTriples)
        return cIds, triples

    @staticmethod
    def fromTree(tree):
        if tree[0] != ConceptRel.treeName or len(tree) != 3:
            raise Exception("Cannot create " + ConceptRel.treeName + " from given tree " + str(tree))

        return ConceptRel(conceptFromTree(tree[1]), Nodes.ActionNodes.actionFromTree(tree[2]))


class ConceptLawSection:
    treeName = "ConceptLawSection"

    def __init__(self, ls):
        self.ls = ls
        self.id = useId()
        self.lsRefs = []

    def process(self, facts=None):
        return self.ls.process(facts)

    @staticmethod
    def fromTree(tree):
        if tree[0] != ConceptLawSection.treeName or len(tree) != 2:
            raise Exception("Cannot create " + ConceptLawSection.treeName + " from given tree " + str(tree))

        return ConceptLawSection(lawSectionFromTree(tree[1]))


class ConceptNamedEntity:
    treeName = "ConceptNamedEntity"

    def __init__(self, ne):
        self.ne = ne
        self.id = useId()
        self.neRefs = []

    def process(self, facts=None):
        return self.ne.process(facts)

    @staticmethod
    def fromTree(tree):
        if tree[0] != ConceptNamedEntity.treeName or len(tree) != 2:
            raise Exception("Cannot create " + ConceptNamedEntity.treeName + " from given tree " + str(tree))

        return ConceptNamedEntity(namedEntityFromTree(tree[1]))


class ZipAndCity:
    treeName = "ZipAndCity"

    def __init__(self, z, c):
        self.z = z
        self.c = c
        self.id = useId()
        self.cRefs = []

    def process(self, facts=None):
        (cIds, cTriples) = self.c.process(facts)
        self.cRefs = getDNFList(cIds)

        triples = [(self.id, "a", ADDRESS_IRI), (self.id, ZIP_IRI, self.z), (self.id, CITY_IRI, cIds)]

        triples.extend(cTriples)
        return self.id, triples

    @staticmethod
    def fromTree(tree):
        if tree[0] != ZipAndCity.treeName or len(tree) != 3:
            raise Exception("Cannot create " + ZipAndCity.treeName + " from given tree " + str(tree))

        return ZipAndCity(tree[1], namedEntityFromTree(tree[2]))


class StreetAndNr:
    treeName = "StreetAndNr"

    def __init__(self, s, n):
        self.s = s
        self.n = n
        self.id = useId()
        self.sRefs = []

    def process(self, facts=None):
        (sIds, sTriples) = self.s.process(facts)
        self.sRefs = getDNFList(sIds)

        triples = [(self.id, "a", ADDRESS_IRI), (self.id, STREET_IRI, sIds), (self.id, HOUSE_NR_IRI, self.n)]

        triples.extend(sTriples)
        return self.id, triples

    @staticmethod
    def fromTree(tree):
        if tree[0] != StreetAndNr.treeName or len(tree) != 3:
            raise Exception("Cannot create " + StreetAndNr.treeName + " from given tree " + str(tree))

        return StreetAndNr(namedEntityFromTree(tree[1]), tree[2])


class Address:
    treeName = "Address"

    def __init__(self, z, c, s, n):
        self.z = z
        self.c = c
        self.s = s
        self.n = n
        self.id = useId()
        self.cRefs = []
        self.sRefs = []

    def process(self, facts=None):
        (cIds, cTriples) = self.c.process(facts)
        self.cRefs = getDNFList(cIds)

        (sIds, sTriples) = self.s.process(facts)
        self.sRefs = getDNFList(sIds)

        triples = [(self.id, "a", ADDRESS_IRI), (self.id, ZIP_IRI, self.z), (self.id, CITY_IRI, cIds), (self.id, STREET_IRI, sIds), (self.id, HOUSE_NR_IRI, self.n)]

        triples.extend(cTriples)
        triples.extend(sTriples)
        return self.id, triples

    @staticmethod
    def fromTree(tree):
        if tree[0] != Address.treeName or len(tree) != 5:
            raise Exception("Cannot create " + Address.treeName + " from given tree " + str(tree))

        return Address(tree[1], namedEntityFromTree(tree[2]), namedEntityFromTree(tree[3]), tree[4])


class Address2:
    treeName = "Address2"

    @staticmethod
    def fromTree(tree):
        if tree[0] != Address2.treeName or len(tree) != 5:
            raise Exception("Cannot create " + Address2.treeName + " from given tree " + str(tree))

        return Address(tree[1], namedEntityFromTree(tree[2]), namedEntityFromTree(tree[3]), tree[4])


class ConceptDate:
    treeName = "ConceptDate"

    def __init__(self, d):
        self.d = d
        self.id = useId()
        self.dRefs = []

    def process(self, facts=None):
        return self.d.process(facts)

    @staticmethod
    def fromTree(tree):
        if tree[0] != ConceptDate.treeName or len(tree) != 2:
            raise Exception("Cannot create " + ConceptDate.treeName + " from given tree " + str(tree))

        return ConceptDate(dateFromTree(tree[1]))
