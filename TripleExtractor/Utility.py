from sympy import Symbol
from sympy.logic.boolalg import to_dnf, And, Or

idCounter = 0


def useId():
    global idCounter
    idCounter += 1

    return "x" + str(idCounter)


def getDNFList(exp):
    if exp is None or exp == "":
        return []

    result = []
    dnf = to_dnf(exp)

    if isinstance(dnf, Symbol):
        return [[dnf.name]]
    elif isinstance(dnf, Or):
        for arg in dnf.args:
            if isinstance(arg, Symbol):
                result.append([arg.name])
            elif isinstance(arg, And):
                result.append(list(map(lambda x: x.name, arg.args)))
    elif isinstance(dnf, And):
        result.append(list(map(lambda x: x.name, dnf.args)))
    else:
        raise Exception("Malformed expression during resolution of expression")

    return result


def DNFListToString(dnfList):
    return " | ".join(map(lambda x: "(" + (" & ".join(x)) + ")", dnfList))


def treeToString(tree):
    result = ""

    for i, elem in enumerate(tree):
        if i == 0:
            result += "("

        if isinstance(elem, list):
            result += treeToString(elem)
        else:
            result += elem

        if i == len(tree) - 1:
            result += ")"
        else:
            result += " " if i < len(tree) - 1 else ""

    return result


def numeralToInt(numeral):
    op = numeral[0] if isinstance(numeral, list) else numeral

    if op in ["num", "pot2as3", "pot1as2", "pot0as1", "pot0"]:
        return numeralToInt(numeral[1])
    elif op == "pot01":
        return "1"
    elif op == "n2":
        return "2"
    elif op == "n3":
        return "3"
    elif op == "n4":
        return "4"
    elif op == "n5":
        return "5"
    elif op == "n6":
        return "6"
    elif op == "n7":
        return "7"
    elif op == "n8":
        return "8"
    elif op == "n9":
        return "9"
    elif op == "pot110":
        return "10"
    elif op == "pot111":
        return "11"
    elif op == "pot1to19":
        return str(10 + int(numeralToInt(numeral[1])))
    elif op == "pot1":
        return str(10 * int(numeralToInt(numeral[1])))

    return "0"
