from subprocess import TimeoutExpired

from constants import PGF_PATH, GF_PATH
from gf import GFShellRaw

PROMPT_STR = "LangWrapper>"

linMap = {}

gfShell = None

class GFException(Exception):
    def __str__(self):
        return "Please run startGF() first before trying to access GF"


class GFParseException(Exception):
    def __init__(self, errorMsg):
        self.errorMsg = errorMsg

    def __str__(self):
        return self.errorMsg


class GFLinearizeException(Exception):
    def __init__(self, errorMsg):
        self.errorMsg = errorMsg

    def __str__(self):
        return self.errorMsg


def startGF():
    global gfShell

    gfShell = GFShellRaw(GF_PATH)
    gfShell.handle_command("i %s" % PGF_PATH)


def stopGF():
    try:
        gfShell.do_shutdown()
    except TimeoutExpired:
        pass


def parse(sentence):
    if gfShell is None:
        raise GFException()

    result = gfShell.handle_command("p \"" + sentence + "\"")

    if result is None or result.startswith("The parser failed") or result.startswith("command not parsed"):
        raise GFParseException("")

    return list(map(lambda x: x.strip(), result.split("\n")))


def linearize(exp):
    if exp in linMap:
        return linMap[exp]

    if gfShell is None:
        raise GFException()

    result = gfShell.handle_command("l %s" % exp)

    if result is None or result.startswith("command not parsed") or result.endswith("not in scope"):
        raise GFLinearizeException("")

    linResults = list(map(lambda x: x.strip(), result.split("\n")))

    linMap[exp] = linResults[0]

    return linMap[exp]
