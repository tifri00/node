# absolute path to PGF file
PGF_PATH = "/absolute/path/to/grammar.pgf"

GF_PATH = "/usr/local/bin/gf"

NODE_PREFIX = "http://localhost:9999/NoDE#"

GRAPH_IRI = "http://localhost:9999/NoDE"

ACTION_IRI = NODE_PREFIX + "Action"
ADDRESS_IRI = NODE_PREFIX + "Address"
ADVERB_IRI = NODE_PREFIX + "Adverb"
CONCEPT_IRI = NODE_PREFIX + "Concept"
DATE_IRI = NODE_PREFIX + "Date"
FACT_IRI = NODE_PREFIX + "Fact"
FACT_LIST_IRI = NODE_PREFIX + "FactList"
HRB_ENTRY_IRI = NODE_PREFIX + "HRBEntry"
KNOWLEDGE_CHUNK_IRI = NODE_PREFIX + "KnowledgeChunk"
LAW_SECTION_IRI = NODE_PREFIX + "LawSection"
NE_CONCEPT_IRI = NODE_PREFIX + "NEConcept"
PRICE_IRI = NODE_PREFIX + "Price"
PROPERTY_IRI = NODE_PREFIX + "Property"
QUANTIFIER_IRI = NODE_PREFIX + "Quantifier"

ACCORDING_TO_IRI = NODE_PREFIX + "accordingTo"
ACT_IRI = NODE_PREFIX + "act"
ADJ_IRI = NODE_PREFIX + "adj"
ADV_IRI = NODE_PREFIX + "adv"
ADV_S_IRI = NODE_PREFIX + "advS"
BESIDES_IRI = NODE_PREFIX + "besides"
CENT_IRI = NODE_PREFIX + "numCent"
CITY_IRI = NODE_PREFIX + "city"
COMP_IRI = NODE_PREFIX + "comp"
CONC_IRI = NODE_PREFIX + "conc"
CONCEPT_ANNOTATION_IRI = NODE_PREFIX + "concAnnotation"
COND_IRI = NODE_PREFIX + "cond"
CONTRADICTION_IRI = NODE_PREFIX + "contradiction"
DATE_DAY_IRI = NODE_PREFIX + "dateDay"
DATE_MONTH_IRI = NODE_PREFIX + "dateMonth"
DATE_PROP_IRI = NODE_PREFIX + "dateProp"
DATE_YEAR_IRI = NODE_PREFIX + "dateYear"
DISREGARDING_IRI = NODE_PREFIX + "disregarding"
DURCH_IRI = NODE_PREFIX + "durch"
EURO_IRI = NODE_PREFIX + "numEuro"
FACT_REL_IRI = NODE_PREFIX + "fact"
FACTS_IRI = NODE_PREFIX + "facts"
FOR_IRI = NODE_PREFIX + "for"
HOUSE_NR_IRI = NODE_PREFIX + "houseNr"
INDIR_OBJ_IRI = NODE_PREFIX + "indirObj"
LAW_IRI = NODE_PREFIX + "law"
LINKED_FACT_IRI = NODE_PREFIX + "linkedFact"
LOC_IRI = NODE_PREFIX + "loc"
MIT_IRI = NODE_PREFIX + "mit"
MOD_IRI = NODE_PREFIX + "mod"
MOD_A_IRI = NODE_PREFIX + "modA"
MODAL_IRI = NODE_PREFIX + "modal"
NEG_IRI = NODE_PREFIX + "neg"
NUM_IRI = NODE_PREFIX + "num"
OBJ_IRI = NODE_PREFIX + "obj"
OHNE_IRI = NODE_PREFIX + "without"
OP_IRI = NODE_PREFIX + "op"
ORIGIN_IRI = NODE_PREFIX + "origin"
OWNER_IRI = NODE_PREFIX + "owner"
PARAGRAPH_IRI = NODE_PREFIX + "paragraph"
PART_IRI = NODE_PREFIX + "part"
PROP_IRI = NODE_PREFIX + "prop"
PRED_IRI = NODE_PREFIX + "pred"
QUANT_IRI = NODE_PREFIX + "quant"
REASON_IRI = NODE_PREFIX + "reason"
REGARDING_IRI = NODE_PREFIX + "regarding"
RELCL_IRI = NODE_PREFIX + "relcl"
SENTENCE_IRI = NODE_PREFIX + "sentence"
STREET_IRI = NODE_PREFIX + "street"
SUBJ_IRI = NODE_PREFIX + "subj"
SYNTREE_IRI = NODE_PREFIX + "syntree"
TAKES_EFFECT_DATE_IRI = NODE_PREFIX + "takesEffectDate"
TARGET_IRI = NODE_PREFIX + "target"
THAT_IRI = NODE_PREFIX + "that"
ZIP_IRI = NODE_PREFIX + "zip"
ZU_IRI = NODE_PREFIX + "zu"


MOD_POSSIBILITY_IRI = NODE_PREFIX + "modPossibility"
MOD_OBLIGATION_IRI = NODE_PREFIX + "modObligation"
MOD_PERMISSION_IRI = NODE_PREFIX + "modPermission"
MOD_DESIRE_IRI = NODE_PREFIX + "modDesire"


QUANT_ALL_VALUE = NODE_PREFIX + "all"
QUANT_FEW_VALUE = NODE_PREFIX + "few"
QUANT_MANY_VALUE = NODE_PREFIX + "many"
QUANT_NONE_VALUE = NODE_PREFIX + "none"


BE_IRI = NODE_PREFIX + "be"
EXIST_IRI = NODE_PREFIX + "exist"
GENERAL_SUBJ_IRI = NODE_PREFIX + "generalSubj"


DYN_PROPERTIES = [
    FACTS_IRI,
    OWNER_IRI,
    LOC_IRI,
    OBJ_IRI,
    INDIR_OBJ_IRI,
    COMP_IRI,
    MOD_IRI,
    ACT_IRI,
    SUBJ_IRI,
    COND_IRI,
    REASON_IRI,
    CONTRADICTION_IRI,
    PROP_IRI,
    ADV_IRI,
    QUANT_IRI,
    PART_IRI,
    ZU_IRI,
    TARGET_IRI,
    REGARDING_IRI,
    DATE_PROP_IRI,
    DURCH_IRI,
    MIT_IRI,
    OHNE_IRI,
    FOR_IRI,
    DISREGARDING_IRI,
    RELCL_IRI,
    ORIGIN_IRI,
    ACCORDING_TO_IRI,
    LINKED_FACT_IRI,
    TAKES_EFFECT_DATE_IRI,
    BESIDES_IRI,
    CITY_IRI,
    STREET_IRI,
    CONCEPT_ANNOTATION_IRI,
    THAT_IRI,
]


SATZUNGSSITZ_LOCATION_KEY = "satzungssitzLocation"
EINZELVERTRETUNGSBERECHTIGUNG_POSSIBLE_KEY = "einzelvertretungsberechtigungPossible"
WAIVER_181_BGB_POSSIBLE_KEY = "waiver181BGBPossible"
GESELLSCHAFTER_LIST_KEY = "gesellschafterList"
NEW_GF_NAME_KEY = "newGFName"
NEW_GF_WAIVER_181_BGB_KEY = "newGFWaiver181BGB"
NEW_GF_REPRESENTATION_TYPE_KEY = "newGFRepresentationType"

INFO_KEYS = {
    SATZUNGSSITZ_LOCATION_KEY: {
        "name": "Satzungssitz der Gesellschaft",
        "multiple": False,
        "valueMap": {},
        "newInfoInputLabel": "Satzungssitz",
        "editInfoInputLabel": "Korrekten Satzungssitz angeben (falls nicht korrekt)",
        "inputType": "text",
        "shortInfoText": lambda value, hoverable, keyword, maxAlt, alt, source:
            f"Der Satzungssitz der Gesellschaft ist in {value}",
        "longInfoText": lambda value, hoverable, keyword, maxAlt, alt, source:
            hoverable(f"Der Satzungssitz der Gesellschaft ist in {keyword(value)}", source) + ('' if len(maxAlt) == 0 else ' (Möglicherweise auch in {}{})'.format('' if len(maxAlt) <= 1 else ', '.join(list(map(lambda a: keyword(hoverable(a[0], a[1])), maxAlt[:-1]))) + ' oder ', keyword(hoverable(maxAlt[len(maxAlt) - 1][0], maxAlt[len(maxAlt) - 1][1])))),
    },
    EINZELVERTRETUNGSBERECHTIGUNG_POSSIBLE_KEY: {
        "name": "Erteilbarkeit von Einzelvertretungsberechtigungen",
        "multiple": False,
        "valueMap": {
            "True": "können gewährt werden",
            "False": "können nicht gewährt werden",
        },
        "newInfoInputLabel": "Einzelvertretungsberechtigung erlaubt?",
        "editInfoInputLabel": "Einzelvertretungsberechtigung erlaubt? (korrigieren, falls nicht korrekt)",
        "inputType": "checkbox",
        "shortInfoText": lambda value, hoverable, keyword, maxAlt, alt, source:
            f"Einzelvertretungsberechtigungen können {'' if value == 'True' else 'nicht '}gewährt werden",
        "longInfoText": lambda value, hoverable, keyword, maxAlt, alt, source:
            f"{hoverable(INFO_KEYS[EINZELVERTRETUNGSBERECHTIGUNG_POSSIBLE_KEY]['shortInfoText'](value, hoverable, keyword, maxAlt, alt, source), source)}{'(Möglicherweise {})'.format(hoverable(keyword('doch {}möglich'.format('nicht ' if value == 'True' else '')), maxAlt[0][1] if len(maxAlt) > 0 else alt[0][1])) if len(alt) > 0 or len(maxAlt) > 0 else ''}",
    },
    WAIVER_181_BGB_POSSIBLE_KEY: {
        "name": "Möglichkeit der Befreiung von § 181 BGB",
        "multiple": False,
        "valueMap": {
            "True": "können gewährt werden",
            "False": "können nicht gewährt werden",
        },
        "newInfoInputLabel": "Befreiung von § 181 BGB möglich?",
        "editInfoInputLabel": "Befreiung von § 181 BGB möglich? (korrigieren, falls nicht korrekt)",
        "inputType": "checkbox",
        "shortInfoText": lambda value, hoverable, keyword, maxAlt, alt, source:
            f"Befreiung von den Beschränkungen des § 181 BGB kann {'' if value == 'True' else 'nicht '}gewährt werden",
        "longInfoText": lambda value, hoverable, keyword, maxAlt, alt, source:
            f"{hoverable(INFO_KEYS[WAIVER_181_BGB_POSSIBLE_KEY]['shortInfoText'](value, hoverable, keyword, maxAlt, alt, source), source)}{'(Möglicherweise {})'.format(hoverable(keyword('doch {}möglich'.format('nicht ' if value == 'True' else '')), maxAlt[0][1] if len(maxAlt) > 0 else alt[0][1])) if len(alt) > 0 or len(maxAlt) > 0 else ''}",
    },
    GESELLSCHAFTER_LIST_KEY: {
        "name": "Gesellschafter(in)",
        "multiple": True,
        "valueMap": {},
        "newInfoInputLabel": "Name des Gesellschafters/der Gesellschafterin",
        "editInfoInputLabel": "Name des Gesellschafters/der Gesellschafterin (korrigieren, falls nicht korrekt)",
        "inputType": "text",
        "shortInfoText": lambda value, hoverable, keyword, maxAlt, alt, source:
            f"{value} ist Gesellschafter(in)",
        "longInfoText": lambda value, hoverable, keyword, maxAlt, alt, source:
            hoverable(f"{keyword(value)} ist Gesellschafter(in)", source) + ('' if len(maxAlt) == 0 else ' (Möglicherweise auch {}{})'.format('' if len(maxAlt) <= 1 else ', '.join(list(map(lambda a: keyword(hoverable(a[0], a[1])), maxAlt[:-1]))) + ' oder ', keyword(hoverable(maxAlt[len(maxAlt) - 1][0], maxAlt[len(maxAlt) - 1][1])))),
    },
    NEW_GF_NAME_KEY: {
        "name": "Name des neuen Geschäftsführers",
        "multiple": False,
        "valueMap": {},
        "newInfoInputLabel": "Name des neuen Geschäftsführers",
        "editInfoInputLabel": "Name des neuen Geschäftsführers (korrigieren, falls nicht korrekt)",
        "inputType": "text",
        "shortInfoText": lambda value, hoverable, keyword, maxAlt, alt, source:
            f"{value} ist der neu angemeldete Geschäftsführer",
        "longInfoText": lambda value, hoverable, keyword, maxAlt, alt, source:
            hoverable(f"{keyword(value)} ist der neu angemeldete Geschäftsführer", source) + ('' if len(maxAlt) == 0 else ' (Möglicherweise auch {}{})'.format('' if len(maxAlt) <= 1 else ', '.join(list(map(lambda a: keyword(hoverable(a[0], a[1])), maxAlt[:-1]))) + ' oder ', keyword(hoverable(maxAlt[len(maxAlt) - 1][0], maxAlt[len(maxAlt) - 1][1])))),
    },
    NEW_GF_WAIVER_181_BGB_KEY: {
        "name": "Neuer Geschäftsführer Befreiung von § 181 BGB",
        "multiple": False,
        "valueMap": {
            "True": "befreit",
            "False": "nicht befreit",
        },
        "newInfoInputLabel": "Neuer Geschäftsführer von § 181 BGB befreit?",
        "editInfoInputLabel": "Neuer Geschäftsführer von § 181 BGB befreit? (korrigieren, falls nicht korrekt)",
        "inputType": "checkbox",
        "shortInfoText": lambda value, hoverable, keyword, maxAlt, alt, source:
            f"Der neue Geschäftsführer ist {'' if value == 'True' else 'nicht '}von den Beschränkungen des § 181 BGB befreit",
        "longInfoText": lambda value, hoverable, keyword, maxAlt, alt, source:
            f"{hoverable(INFO_KEYS[NEW_GF_WAIVER_181_BGB_KEY]['shortInfoText'](value, hoverable, keyword, maxAlt, alt, source), source)}{'(Möglicherweise {})'.format(hoverable(keyword('doch {}'.format('nicht ' if value == 'True' else '')), maxAlt[0][1] if len(maxAlt) > 0 else alt[0][1])) if len(alt) > 0 or len(maxAlt) > 0 else ''}",
    },
    NEW_GF_REPRESENTATION_TYPE_KEY: {
        "name": "Vertretungsregelung des neuen Geschäftsführers",
        "multiple": False,
        "valueMap": {
            "alleinvertretung": "Alleinvertretung",
            "gesamtvertretung": "Gesamtvertretung",
            "echtegesamtvertretung": "Echte Gesamtvertretung",
        },
        "newInfoInputLabel": "Vertretungsregelung",
        "editInfoInputLabel": "Vertretungsregelung (korrigieren, falls nicht korrekt)",
        "inputType": "select",
        "shortInfoText": lambda value, hoverable, keyword, maxAlt, alt, source:
            f"Vertretungsregelung des neuen Geschäftsführers: {INFO_KEYS[NEW_GF_REPRESENTATION_TYPE_KEY]['valueMap'][value]}",
        "longInfoText": lambda value, hoverable, keyword, maxAlt, alt, source:
            f"{hoverable(INFO_KEYS[NEW_GF_REPRESENTATION_TYPE_KEY]['shortInfoText'](value, hoverable, keyword, maxAlt, alt, source), source)}{' (Möglicherweise auch {}{})'.format('' if len(maxAlt) <= 1 else ', '.join(list(map(lambda a: keyword(hoverable(a[0], a[1])), maxAlt[:-1]))) + ' oder ', keyword(hoverable(maxAlt[len(maxAlt) - 1][0], maxAlt[len(maxAlt) - 1][1]))) if len(maxAlt) > 0 else ''}",
    },
}
